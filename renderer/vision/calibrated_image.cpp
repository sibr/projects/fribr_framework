#include "vision/calibrated_image.h"
#include "tools.h"
#include "3dmath.h"

#include <opencv2/opencv.hpp>
#include <sstream>
#include <fstream>
#include <iostream>
#include <cmath>

namespace
{

GLuint create_index_buffer()
{
    unsigned int indices[] = { 0,  1,  // X
                               2,  3,  // Y
                               4,  5,  // Z
                               6,  7,
                               6,  8,
                               6,  9,
                               6, 10,  // Frustum
                               7,  8,
                               8,  9,
                               9, 10,
                              10,  7}; // Frame

    GLuint index_buffer;
    glGenBuffers(1, &index_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 2 * 11, indices, GL_STATIC_DRAW);

    return index_buffer;
}

GLuint create_color_buffer(float r, float g, float b)
{
    float colors[] = {1.0f, 0.0f, 0.0f,
                      1.0f, 0.0f, 0.0f,  // Red
                      0.0f, 1.0f, 0.0f,
                      0.0f, 1.0f, 0.0f,  // Green
                      0.0f, 0.0f, 1.0f,
                      0.0f, 0.0f, 1.0f,  // Blue
                      0.0f, 0.0f, 0.0f,
                      0.0f, 0.0f, 0.0f,
                      0.0f, 0.0f, 0.0f,
                      0.0f, 0.0f, 0.0f,
                      0.0f, 0.0f, 0.0f};

    for (int i = 3 * 6; i < 3 * 11; i += 3)
    {
        colors[i + 0] = r;
        colors[i + 1] = g;
        colors[i + 2] = b;
    }

    GLuint color_buffer;
    glGenBuffers(1, &color_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * 11, colors, GL_STATIC_DRAW);

    return color_buffer;
}

fribr::Shader* compile_calibrated_image_shader()
{
    fribr::Shader* shader = new fribr::Shader();
    shader->vertex_shader(
    GLSL(330,
        layout(location = 0) in vec3 vertex_position;
        layout(location = 1) in vec3 vertex_color;

        uniform mat4 camera_to_clip;
        uniform mat4 world_to_camera;

        out vec3 color;

        void main()
        {
            color       = vertex_color;
            gl_Position = camera_to_clip * world_to_camera * vec4(vertex_position, 1.0);
        }
    ));

    shader->fragment_shader(
    GLSL(330,
        in vec3  color;
        out vec4 frag_color;	     
        void main() { frag_color = vec4(color, 1.0); }
    ));
    shader->link();

    return shader;
}

Eigen::Matrix<float, 3, 11> create_frustum_vertices(float focal_x, float focal_y,
                                                    float dx, float dy, 
                                                    Eigen::Matrix3f orientation,
                                                    Eigen::Vector3f position,
                                                    float render_scale)
{

    Eigen::Vector3f up   (0.0f, dy / focal_y, 0.0f);
    Eigen::Vector3f right(dx / focal_x, 0.0f, 0.0f);

    Eigen::Matrix<float, 3, 11> points;
    points << Eigen::Vector3f(0.0f, 0.0f, 0.0f),
              Eigen::Vector3f(1.0f, 0.0f, 0.0f), // Red
              Eigen::Vector3f(0.0f, 0.0f, 0.0f),
              Eigen::Vector3f(0.0f, 1.0f, 0.0f), // Green
              Eigen::Vector3f(0.0f, 0.0f, 0.0f),
              Eigen::Vector3f(0.0f, 0.0f, 1.0f), // Blue
              Eigen::Vector3f(0.0f, 0.0f, 0.0f),
               up - right - Eigen::Vector3f(0.0f, 0.0f, 1.0f),
               up + right - Eigen::Vector3f(0.0f, 0.0f, 1.0f),
              -up + right - Eigen::Vector3f(0.0f, 0.0f, 1.0f),
              -up - right - Eigen::Vector3f(0.0f, 0.0f, 1.0f); // Gray

    Eigen::Affine3f transformation = Eigen::Translation3f(position) *
                                     orientation *
                                     Eigen::Scaling(render_scale);

    return transformation * points;
}

}

namespace fribr
{

int     CalibratedImage::s_num_instances = 0;
GLuint  CalibratedImage::s_index_buffer  = 0;
GLuint  CalibratedImage::s_gray_buffer   = 0;
GLuint  CalibratedImage::s_red_buffer    = 0;
GLuint  CalibratedImage::s_green_buffer  = 0;
GLuint  CalibratedImage::s_blue_buffer   = 0;
GLuint  CalibratedImage::s_yellow_buffer = 0;
Shader* CalibratedImage::s_shader        = 0;



CalibratedImage::CalibratedImage(float focal_x, float focal_y,
                                 float dx, float dy, Eigen::Vector3f k,
                                 Eigen::Matrix3f orientation,
                                 Eigen::Vector3f position,
                                 Eigen::Vector2i resolution,
                                 Texture::Ptr texture,
                                 const std::string &image_path)
    : m_world_to_camera(compute_world_to_cam(orientation, position)),
      m_near(-1.0f),
      m_far(-1.0f),
      m_focal_x(focal_x),
      m_focal_y(focal_y),
      m_dx(dx),
      m_dy(dy),
      m_k(k),
      m_orientation(orientation),
      m_position(position),
      m_render_scale(0.2f),
      m_resolution(resolution),
      m_max_image_width(8192),
      m_texture(texture),
      m_image_path(image_path),
      m_image_name(strip_extension(get_filename(image_path)))
{
    set_max_width(m_max_image_width);

    // The first instance of CalibratedImage is responsible for compiling the
    // shader and creating the index/color buffers.
    if (s_num_instances++ == 0)
    {
        s_index_buffer  = create_index_buffer();
        s_gray_buffer   = create_color_buffer(0.5f, 0.5f, 0.5f);
        s_red_buffer    = create_color_buffer(1.0f, 0.0f, 0.0f);
        s_green_buffer  = create_color_buffer(0.0f, 1.0f, 0.0f);
        s_blue_buffer   = create_color_buffer(0.0f, 0.0f, 1.0f);
        s_yellow_buffer = create_color_buffer(1.0f, 1.0f, 0.0f);
        s_shader        = compile_calibrated_image_shader();
    }

    m_gl_data.reset(new GLData(focal_x, focal_y, dx, dy, orientation, position, m_render_scale));
    glBindVertexArray(m_gl_data->m_vao);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_index_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, s_gray_buffer);

    glEnableVertexAttribArray(COLOR_LOCATION);
    glVertexAttribPointer(COLOR_LOCATION, 3, GL_FLOAT, 0, 0, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
}

CalibratedImage::CalibratedImage(const CalibratedImage& rhs)
    : m_camera_to_clip(rhs.m_camera_to_clip),
      m_world_to_camera(rhs.m_world_to_camera),
      m_intrinsics(rhs.m_intrinsics),
      m_near(rhs.m_near),
      m_far(rhs.m_far),
      m_focal_x(rhs.m_focal_x),
      m_focal_y(rhs.m_focal_y), 
      m_dx(rhs.m_dx),
      m_dy(rhs.m_dy),
      m_k(rhs.m_k),
      m_orientation(rhs.m_orientation),
      m_position(rhs.m_position),
      m_render_scale(rhs.m_render_scale),
      m_resolution(rhs.m_resolution),
      m_scale_factor(rhs.m_scale_factor),
      m_max_image_width(rhs.m_max_image_width),
      m_gl_data(rhs.m_gl_data),
      m_texture(rhs.m_texture),
      m_image(rhs.m_image),
      m_image_path(rhs.m_image_path),
      m_image_name(rhs.m_image_name)
{
    s_num_instances++;
}

CalibratedImage::~CalibratedImage() noexcept {
    // The last instance of CalibratedImage is responsible for deleting the
    // shader and the index/color buffers.
    if (--s_num_instances == 0)
    {
        glDeleteBuffers(1, &s_index_buffer);
        glDeleteBuffers(1, &s_gray_buffer);
        glDeleteBuffers(1, &s_red_buffer);
        glDeleteBuffers(1, &s_green_buffer);
        glDeleteBuffers(1, &s_blue_buffer);
        glDeleteBuffers(1, &s_yellow_buffer);
        delete s_shader;
    }
}

CalibratedImage::GLData::GLData(float focal_x, float focal_y, float dx, float dy,
                                Eigen::Matrix3f orientation, Eigen::Vector3f position,
                                float render_scale)
{
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    Eigen::Matrix<float, 3, 11> points;
    points = create_frustum_vertices(focal_x, focal_y, dx, dy,
                                     orientation, position,
                                     render_scale);

    glGenBuffers(1, &m_vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * 11, points.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(VERTEX_LOCATION);
    glVertexAttribPointer(VERTEX_LOCATION, 3, GL_FLOAT, 0, 0, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
}

CalibratedImage::GLData::~GLData()
{
    if (m_vao)
        glDeleteVertexArrays(1, &m_vao);
    if (m_vertex_buffer)
        glDeleteBuffers(1, &m_vertex_buffer);
}


Eigen::Matrix4f CalibratedImage::get_camera_to_clip(float near, float far) noexcept {
    if (m_near != near || m_far != far)
    {
        float width      = float(m_resolution.x());
        float height     = float(m_resolution.y());
        float half_fov   = 180.0f * atan2f(height * 0.5f, m_focal_y) / float(M_PI);
        m_camera_to_clip = compute_projection_matrix(near, far, 2.0f * half_fov, width / height);

        m_near = near;
        m_far  = far;
    }

    return m_camera_to_clip;
}

void CalibratedImage::set_max_width(size_t max_image_width) noexcept {
    m_max_image_width = max_image_width;
    m_scale_factor    = std::min(1.0f, static_cast<float>(max_image_width) / m_resolution.x());
    m_intrinsics      = compute_intrinsics(m_focal_x, m_focal_y, m_dx, m_dy, float(m_resolution.y()), m_scale_factor);
}

void CalibratedImage::load_image(size_t max_image_width,
				 RadialDistortion distortion,
				 ResourceMode resource_mode) noexcept {
    const bool undistort = distortion == IgnoreRadialDistortion;
    m_image = undistort ? fribr::load_image(m_image_path, int(max_image_width), int(cv::IMREAD_COLOR))
                        : load_undistorted_image(m_image_path,
                                                 Eigen::Vector2f(m_focal_x, m_focal_y),
                                                 Eigen::Vector2f(m_dx, m_dy),
                                                 m_k, int(max_image_width),
                                                 int(cv::IMREAD_COLOR));                                        

    if (resource_mode == CPUAndOpenGL)
    {
	fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR,
						      fribr::Texture::GENERATE_MIPMAPS |
						      fribr::Texture::ANISOTROPIC_FILTERING);
	m_texture = fribr::Texture::Ptr(new fribr::Texture(m_image, texture_descriptor));
    }

    set_max_width(max_image_width);
}

void CalibratedImage::set_render_scale(float scale) noexcept {
    if (scale == m_render_scale)
        return;
    
    m_render_scale = scale;
    m_gl_data.reset(new GLData(m_focal_x, m_focal_y, m_dx, m_dy, m_orientation, m_position, m_render_scale));
    glBindVertexArray(m_gl_data->m_vao);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_index_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, s_gray_buffer);

    glEnableVertexAttribArray(COLOR_LOCATION);
    glVertexAttribPointer(COLOR_LOCATION, 3, GL_FLOAT, 0, 0, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
}

void CalibratedImage::draw(const Eigen::Affine3f &world_to_camera,
                           const Eigen::Matrix4f &camera_to_clip,
                           Color color) const noexcept {
    s_shader->use();
    s_shader->set_uniform("world_to_camera", world_to_camera);
    s_shader->set_uniform("camera_to_clip",  camera_to_clip);

    glBindVertexArray(m_gl_data->m_vao);
    
    GLuint color_buffer = s_gray_buffer;
    switch(color)
    {
    case Gray:   color_buffer = s_gray_buffer;   break;
    case Red:    color_buffer = s_red_buffer;    break;
    case Green:  color_buffer = s_green_buffer;  break;
    case Blue:   color_buffer = s_blue_buffer;   break;
    case Yellow: color_buffer = s_yellow_buffer; break;
    }

    glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
    glEnableVertexAttribArray(COLOR_LOCATION);
    glVertexAttribPointer(COLOR_LOCATION, 3, GL_FLOAT, 0, 0, 0);

    glDrawElements(GL_LINES, 11 * 2, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    glUseProgram(0);
}

}
