#ifndef CALIBRATED_IMAGE_H
#define CALIBRATED_IMAGE_H

#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/3d/types.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/StdVector>

#include <memory>

namespace fribr
{

class CalibratedImage
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef std::shared_ptr<CalibratedImage> Ptr;
    typedef std::vector<CalibratedImage, Eigen::aligned_allocator<CalibratedImage> > Vector;

    // Vertex shader attribute locations
    static const int VERTEX_LOCATION  = 0;
    static const int COLOR_LOCATION   = 1;

    CalibratedImage(float focal_x, float focal_y, float dx, float dy, Eigen::Vector3f k,
                    Eigen::Matrix3f orientation, Eigen::Vector3f position, Eigen::Vector2i resolution,
                    Texture::Ptr texture, const std::string &image_path);
    CalibratedImage(const CalibratedImage& rhs);
    // Default assignment operator does what we want.
    ~CalibratedImage() throw();

    enum Color { Red, Green, Blue, Gray, Yellow };
    void draw(const Eigen::Affine3f &world_to_camera,
              const Eigen::Matrix4f &camera_to_clip, 
              Color color = Gray) const throw();

    enum RadialDistortion { CorrectRadialDistortion, IgnoreRadialDistortion };
    enum ResourceMode     { CPUOnly, CPUAndOpenGL };

          size_t          get_max_width()                           const throw() { return m_max_image_width; }
          void            set_max_width(size_t max_image_width)           throw();

          void            clear_image()                                   throw() { m_texture.reset();  m_image.release(); }

          void            load_image(size_t max_image_width, 
                                     RadialDistortion distortion
                                     = CorrectRadialDistortion,
	                             ResourceMode resource_mode
	                             = CPUAndOpenGL)                      throw();
          void            load_image(RadialDistortion distortion
                                     = CorrectRadialDistortion,
	                             ResourceMode resource_mode
	                             = CPUAndOpenGL)                      throw() { load_image(m_max_image_width, distortion, resource_mode); }

          float           get_render_scale()                        const throw() { return m_render_scale; }
          void            set_render_scale(float scale)                   throw();
		  void			  set_image_path(std::string &path) { m_image_path = path; }

    const Texture::Ptr    get_texture()                             const throw() { return m_texture;          }
    const cv::Mat&        get_image()                               const throw() { return m_image;            }
          cv::Mat&        get_image()                                     throw() { return m_image;            }
          std::string     get_image_path()                          const throw() { return m_image_path;       }
          std::string     get_image_name()                          const throw() { return m_image_name;       }
          Eigen::Vector3f get_position()                            const throw() { return m_position;         }
          Eigen::Matrix3f get_orientation()                         const throw() { return m_orientation;      }
          Eigen::Vector3f get_forward()                             const throw() { return m_orientation * Eigen::Vector3f(0.0f, 0.0f, -1.0f); }
                                                                           
          Eigen::Matrix3f get_intrinsics()                          const throw() { return m_intrinsics;       }

          float           get_focal_x()                             const throw() { return m_focal_x;          }
          float           get_focal_y()                             const throw() { return m_focal_y;          }
          float           get_dx()                                  const throw() { return m_dx;               }
          float           get_dy()                                  const throw() { return m_dy;               }
          Eigen::Vector3f get_k()                                   const throw() { return m_k;                }
          Eigen::Vector2i get_resolution()                          const throw() { return m_resolution;       }
          Eigen::Vector2i get_scaled_resolution()                   const throw() { return (m_resolution.cast<float>() * m_scale_factor).cast<int>(); }
          float           get_scale_factor()                        const throw() { return m_scale_factor;     }
                                                                           
          Eigen::Matrix4f get_camera_to_clip(float near, float far)       throw();
          Eigen::Affine3f get_world_to_camera()                     const throw() { return m_world_to_camera;  }

private:
    // Cached matrices
    Eigen::Matrix4f m_camera_to_clip;
    Eigen::Affine3f m_world_to_camera;
    Eigen::Matrix3f m_intrinsics;
    float           m_near;
    float           m_far;

    float           m_focal_x, m_focal_y; // Focal length (in pixels).
    float           m_dx, m_dy;           // Optical center (in pixels).
    Eigen::Vector3f m_k;                  // Undistortion coefficient.
    Eigen::Matrix3f m_orientation;        // CAM TO WORLD
    Eigen::Vector3f m_position;
    float           m_render_scale;
    Eigen::Vector2i m_resolution;
    float           m_scale_factor;
    size_t          m_max_image_width;

    struct GLData {
        typedef std::shared_ptr<GLData> Ptr;
        GLuint      m_vao;
        GLuint      m_vertex_buffer;

        GLData(float focal_x, float focal_y, float dx, float dy,
                Eigen::Matrix3f orientation, Eigen::Vector3f position,
                float render_scale);
        ~GLData();
    };
    GLData::Ptr     m_gl_data;
    Texture::Ptr    m_texture;
    cv::Mat         m_image;
    std::string     m_image_path;
    std::string     m_image_name;


    // Shared resources
    static int      s_num_instances;
    static GLuint   s_index_buffer;
    static GLuint   s_gray_buffer;
    static GLuint   s_red_buffer;
    static GLuint   s_green_buffer;
    static GLuint   s_blue_buffer;
    static GLuint   s_yellow_buffer;
    static Shader*  s_shader;

};

}

#endif
