#ifndef MATH_TOOLS_H
#define MATH_TOOLS_H

#include <Eigen/Core>

namespace fribr
{

enum CoordinateSystem { RH_Y_UP, RH_Y_DOWN };

Eigen::Matrix3f to_right_handed_y_up(CoordinateSystem system);
Eigen::Matrix4f compute_projection_matrix(float near, float far, float fov, float aspect);
Eigen::Matrix3f look_at(const Eigen::Vector3f &from,
			const Eigen::Vector3f &to,
			const Eigen::Vector3f &up);

Eigen::Matrix3f compute_intrinsics(float focal_x, float focal_y,
                                   float dx, float dy,
                                   float height, float scale);

Eigen::Matrix4f compute_world_to_cam(const Eigen::Matrix3f &orientation,
				     const Eigen::Vector3f &position);

}

#endif
