#include "3dmath/math_tools.h"
#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#include <cassert>
#include <Eigen/Geometry>

namespace fribr
{

Eigen::Matrix3f to_right_handed_y_up(CoordinateSystem system)
{
    Eigen::Matrix3f converter;

    switch(system)
    {
    case RH_Y_UP:
	converter << 
	    1, 0, 0,
	    0, 1, 0,
	    0, 0, 1;
	break;

    case RH_Y_DOWN:
	converter <<
	    1,  0,  0,
	    0, -1,  0,
	    0,  0, -1;
	break;

    default:
	assert(!"Unknown coordinate system");
    }

    return converter;
}

Eigen::Matrix4f compute_projection_matrix(float near, float far, float fov, float aspect)
{

    float range = tanf(fov * 0.5f * float(M_PI) / 180.0f) * near;
    float Sx = (2.0f * near) / (range * aspect + range * aspect);
    float Sy = near / range;
    float Sz = -(far + near) / (far - near);
    float Pz = -(2.0f * far * near) / (far - near);

    Eigen::Matrix4f projection;
    projection << Sx,   0.0f, 0.0f,  0.0f,
                  0.0f, Sy,   0.0f,  0.0f,
                  0.0f, 0.0f, Sz,    Pz,
                  0.0f, 0.0f, -1.0f, 0.0f;

    return projection;
}

Eigen::Matrix3f look_at(const Eigen::Vector3f &from,
			const Eigen::Vector3f &to,
			const Eigen::Vector3f &up)
{
    Eigen::Vector3f direction   = (from - to).normalized();
    Eigen::Vector3f local_right = -direction.cross(up).normalized();
    Eigen::Vector3f local_up    = -local_right.cross(direction).normalized();

    Eigen::Matrix3f orientation;
    orientation << local_right, local_up, direction;

    return orientation;
}

Eigen::Matrix3f compute_intrinsics(float focal_x, float focal_y,
                                   float dx, float dy,
                                   float height, float scale)
{
    Eigen::Matrix3f intrinsics;
    intrinsics <<
    focal_x * scale,            0.0f,           -dx * scale,
               0.0f, focal_y * scale, (dy - height) * scale,
               0.0f,            0.0f,                 -1.0f;
    return intrinsics;
}

Eigen::Matrix4f compute_world_to_cam(const Eigen::Matrix3f &orientation,
				     const Eigen::Vector3f &position)
{
    Eigen::Affine3f translation;
    translation = Eigen::Translation3f(-position);

    Eigen::AngleAxisf aa;
    aa = orientation.transpose();

    return (aa * translation).matrix();
}

}
