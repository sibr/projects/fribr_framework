#ifndef SPLINES_H
#define SPLINES_H

#include <Eigen/Core>
#include <vector>

namespace fribr
{

Eigen::Vector3f catmull_rom_spline(float t, Eigen::Vector3f a, Eigen::Vector3f b,
				   Eigen::Vector3f c, Eigen::Vector3f d);

std::vector<Eigen::Vector3f> make_control_points(const std::vector<Eigen::Vector3f> &samples);

void smooth_interpolate(const std::vector<Eigen::Vector3f> &samples,
			const int sampling_rate,
			std::vector<Eigen::Vector3f> *interpolated);

}

#endif
