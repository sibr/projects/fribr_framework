#ifdef INRIA_WIN
#include <Eigen/StdVector>
#endif
#include "3dmath/splines.h"

Eigen::Vector3f fribr::catmull_rom_spline(float t, Eigen::Vector3f a, Eigen::Vector3f b,
					  Eigen::Vector3f c, Eigen::Vector3f d)
{
    Eigen::Matrix4f M;
    M <<  0.0f,  1.0f,  0.0f,  0.0f,
         -0.5f,  0.0f,  0.5f,  0.0f,
          1.0f, -2.5f,  2.0f, -0.5f,
         -0.5f,  1.5f, -1.5f,  0.5f;

    Eigen::Matrix<float, 4, 3> X;
    X << a.transpose(),
         b.transpose(),
         c.transpose(),
         d.transpose();

    return (Eigen::Matrix<float, 1, 4>(1.0f, t, t * t, t * t * t) * M * X).transpose();
}

std::vector<Eigen::Vector3f>
fribr::make_control_points(const std::vector<Eigen::Vector3f> &samples)
{
    std::vector<Eigen::Vector3f> control_points;
    if (samples.empty())
        return control_points;

    control_points.reserve(samples.size() + 2);
    control_points.push_back(samples.front());
    for (const Eigen::Vector3f &sample : samples)
        control_points.push_back(sample);
    control_points.push_back(samples.back());

    return control_points;
}

void fribr::smooth_interpolate(const std::vector<Eigen::Vector3f> &samples,
			       const int sampling_rate,
			       std::vector<Eigen::Vector3f> *interpolated)
{
    if (samples.empty())
        return;
    
    interpolated->reserve((samples.size() - 1) * sampling_rate + samples.size());
    std::vector<Eigen::Vector3f> control_points = make_control_points(samples);
    
    for (int i = 0; i < static_cast<int>(control_points.size()) - 3; ++i)
    {
        Eigen::Vector3f a(control_points[i + 0]);
        Eigen::Vector3f b(control_points[i + 1]);
        Eigen::Vector3f c(control_points[i + 2]);
        Eigen::Vector3f d(control_points[i + 3]);

	interpolated->emplace_back(samples[i]);
        for (int j = 0; j < sampling_rate; ++j)
        {
            float t = (j + 1.0f) / sampling_rate;
	    interpolated->emplace_back(catmull_rom_spline(t, a, b, c, d));
        }
    }

    interpolated->emplace_back(samples.back());
}
