#ifndef BVH_SEARCH_H
#define BVH_SEARCH_H

#include "ray_tracing/aabb_node_point.h"
#include "ray_tracing/aabb_node.h"
#include "3d/types.h"

#include <Eigen/Core>
#include <vector>

namespace fribr
{

class BVHSearch
{
public:
    void construct_hierarchy(std::vector<float3>& points, BVHHeuristic heuristic = BVHHeuristic_SAH);
    void destroy_hierarchy(); 

    void save_hierarchy(const char *filename);
    void load_hierarchy(const char *filename);

    bool find_nearest(Eigen::Vector3f p, Eigen::Vector3f* nearest_out,
		      float guess_sqr_distance = 1e21f, int guess_index = -1) const;
    std::vector<Eigen::Vector3f> find_knn(Eigen::Vector3f p, int k) const;
    std::vector<Eigen::Vector3f> find_radius(Eigen::Vector3f p, float r, int max_neighbors = -1) const;
    std::vector<Eigen::Vector3f> find_radius_mahalanobis(Eigen::Vector3f p, Eigen::Vector3f n,
                                                         float r, int max_neighbors = -1) const;

    static std::string compute_md5(const std::vector<float3> &points);

private:
    std::vector<Eigen::Vector3f> m_points;
    std::vector<AABBNodePoint>   m_hierarchy;
};

}

#endif
