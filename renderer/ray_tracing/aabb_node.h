#ifndef AABB_NODE_H
#define AABB_NODE_H

#include "ray_tracing/ray_tracer_structs.h"

#include <Eigen/Core>

#include <vector>
#include <algorithm>
#include <cassert>
#include <cstdint>

namespace fribr
{

enum BVHHeuristic
{
    BVHHeuristic_SAH = 0,
    BVHHeuristic_SM,
    BVHHeuristic_OM,
};

template<typename Tools>
class AABBNode
{
    typedef typename Tools::Type T;
public:
    AABBNode(std::vector<T> *scene, size_t _begin, size_t _end);

    // Returns the index where the subrange should be split, or negative if the node shouldn't be split anymore.
    int             initialize(std::vector<T> *scene, size_t max_per_leaf, BVHHeuristic heuristic);

    Eigen::Vector3f get_max_point() const               { return m_max_point; }
    Eigen::Vector3f get_min_point() const               { return m_min_point; }

    size_t          get_begin()     const               { return m_begin; }
    size_t          get_end()       const               { return m_end; }

    void            set_left_index(size_t left_index)   { m_left_index  = left_index; }
    void            set_right_index(size_t right_index) { m_right_index = right_index; }

    size_t          get_left_index() const              { return m_left_index; }
    size_t          get_right_index() const             { return m_right_index; }

    // For the file loader
    AABBNode(std::istream &in_stream);
    void            serialize(std::ostream &out_stream) const;

    // Conversions to/from a C-style struct
    AABBNode(const BVHNode &node);
    operator BVHNode() const;

private:
    size_t m_left_index;
    size_t m_right_index;

    size_t m_begin;
    size_t m_end;

    Eigen::Vector3f m_min_point;
    Eigen::Vector3f m_max_point;
};

inline bool bounding_box_intersect(const float box_min[3], const float box_max[3],
                                   const float orig[3],    const float inv_dir[3], 
                                   float *t_out)
{
    static const float EPSILON = 0.000001f;
    // Check if the ray is parallel to any of our sides and has it's
    // origin outside our boundaries.
    if (inv_dir[0] == 0.0f && (orig[0] < box_min[0] || orig[0] > box_max[0]))
        return false;
    if (inv_dir[1] == 0.0f && (orig[1] < box_min[1] || orig[1] > box_max[1]))
        return false;
    if (inv_dir[2] == 0.0f && (orig[2] < box_min[2] || orig[2] > box_max[2]))
        return false;

    float delta_min[3] = {
	    (box_min[0] - orig[0]) * inv_dir[0],
	    (box_min[1] - orig[1]) * inv_dir[1],
	    (box_min[2] - orig[2]) * inv_dir[2],
    };

    float delta_max[3] = {
	    (box_max[0] - orig[0]) * inv_dir[0],
	    (box_max[1] - orig[1]) * inv_dir[1],
	    (box_max[2] - orig[2]) * inv_dir[2],
    };

    if (delta_min[0] > delta_max[0])
        std::swap(delta_min[0], delta_max[0]);
    if (delta_min[1] > delta_max[1])
        std::swap(delta_min[1], delta_max[1]);
    if (delta_min[2] > delta_max[2])
        std::swap(delta_min[2], delta_max[2]);

    float end = std::min(delta_max[0], std::min(delta_max[1], delta_max[2]));
    if (end < EPSILON)
        return false;

    float start = std::max(delta_min[0], std::max(delta_min[1], delta_min[2]));
    if (start > end + EPSILON)
        return false;

    // return negative value if the camera is inside the box.
    *t_out = start;

    return true;
}

inline float surface_area(const Eigen::Vector3f &min_point, const Eigen::Vector3f &max_point)
{
    const Eigen::Vector3f d = max_point - min_point;
    return 2.0f * (d[0] * d[1] + d[0] * d[2] + d[1] * d[2]);
}

template<typename Tools>
struct CentroidMinMax
{
    void operator()(const typename Tools::Type& t, Eigen::Vector3f *out_min, Eigen::Vector3f *out_max)
    {
        const Eigen::Array3f c = Tools::centroid(t).array();
        *out_min = out_min->array().min(c).matrix();
        *out_max = out_max->array().max(c).matrix();
    }
};

template<typename Tools, typename Func>
void accumulate_values(typename std::vector<typename Tools::Type>::const_iterator begin,
		       typename std::vector<typename Tools::Type>::const_iterator end,
		       Eigen::Vector3f *out_min, Eigen::Vector3f *out_max, Func func)
{
    if (begin == end)
        return;

    *out_min = *out_max = Tools::centroid(*begin);
    for (typename std::vector<typename Tools::Type>::const_iterator i = begin; i < end; ++i)
        func(*i, out_min, out_max);
}

template<typename Tools>
struct BinaryCompareCentroid
{
    int index;

    BinaryCompareCentroid(int _index)
	:	index(_index)
    {
    }

    bool operator()(const typename Tools::Type& lhs, const typename Tools::Type& rhs)
    {
        return Tools::centroid(lhs)[index] < Tools::centroid(rhs)[index];
    }
};

template<typename Tools>
struct UnaryCompareCentroid
{
    int   index;
    float split;

    UnaryCompareCentroid(int _index, float _split)
	:	index(_index),
		split(_split)
    {
    }

    bool operator()(const typename Tools::Type& t)
    {
        return Tools::centroid(t)[index] < split;
    }
};

template<typename Tools>
AABBNode<Tools>::AABBNode(std::vector<T> *scene, size_t _begin, size_t _end)
    :	m_left_index(0),
	m_right_index(0),
	m_begin(_begin),
	m_end(_end)
{
    assert(scene);
    assert(m_begin <= m_end);
    assert(m_end <= scene->size());

    typedef typename Tools::MinMax MinMaxFunctor;
    accumulate_values<Tools, MinMaxFunctor>(scene->begin() + m_begin,
                                            scene->begin() + m_end,
                                            &m_min_point, &m_max_point,
                                            MinMaxFunctor());
}

template<typename Tools>
size_t object_mean(std::vector<typename Tools::Type> *scene, const Eigen::Vector3f& centroid_min, const Eigen::Vector3f& centroid_max, 
		   size_t begin, size_t end)
{
    int axis = 0;
    Eigen::Vector3f dimensions = centroid_max - centroid_min;
    for (int i = 0; i < 3; ++i)
        if (dimensions[axis] < dimensions[i])
            axis = i;
    std::sort(scene->begin() + begin, scene->begin() + end, BinaryCompareCentroid<Tools>(axis));
    return begin + (end - begin) / 2;
}

template<typename Tools>
size_t spatial_mean(std::vector<typename Tools::Type> *scene, const Eigen::Vector3f &centroid_min, const Eigen::Vector3f &centroid_max, 
		    size_t begin, size_t end)
{
    int axis = 0;
    size_t best_split = 0;
    const size_t num_triangles = end - begin;
    for (int i = 0; i < 3; i++)
    {
        float  center = 0.5f * (centroid_max + centroid_min)[i];
        size_t split  = static_cast<size_t>(std::count_if(scene->begin() + begin,
                                                          scene->begin() + end,
        UnaryCompareCentroid<Tools>(i, center)));

        if (fabs(split - num_triangles / 2.0f) < fabs(best_split - num_triangles / 2.0f))
        {
            best_split = split;
            axis = i;
        }
    }
    float  center = 0.5f * (centroid_max + centroid_min)[axis];
    size_t middle = static_cast<size_t>(std::partition(scene->begin() + begin,
                                                       scene->begin() + end,
                                                       UnaryCompareCentroid<Tools>(axis, center)) - scene->begin());
    return middle;
}

template<typename Tools>
size_t surface_area_heuristic(std::vector<typename Tools::Type> *scene, const Eigen::Vector3f &centroid_min, const Eigen::Vector3f &centroid_max, 
                              const Eigen::Vector3f &min_point, const Eigen::Vector3f &max_point, size_t begin, size_t end)
{
    int    axis       = 0;
    size_t best_split = 0;

    const float SAH_this = surface_area(min_point, max_point);
    const float max_cost = (end - begin) * 2.0f;
    float best_cost = max_cost;

    std::vector<float> costs(end - begin);
    for (int j = 0; j < 3; j++)
    {
        std::sort(scene->begin() + begin, scene->begin() + end, BinaryCompareCentroid<Tools>(j));

        Eigen::Vector3f t_min, t_max;
        t_min = t_max = Tools::centroid(*(scene->begin() + begin));
        typename Tools::MinMax min_max;
        for (int i = static_cast<int>(begin); i < static_cast<int>(end); ++i)
        {
            size_t count_left = i - begin;
            min_max(*(scene->begin() + i), &t_min, &t_max);

            costs[count_left] = (count_left * surface_area(t_min, t_max)) / SAH_this;
        }

        t_min = t_max = Tools::centroid(*(scene->begin() + end - 1));
        for (int i = static_cast<int>(end - 1); i >= static_cast<int>(begin); --i)
        {
            size_t count_left = i - begin;
            size_t count_right = end - i;
            min_max(*(scene->begin() + i), &t_min, &t_max);

            const float SAH_cost = (count_right * surface_area(t_min, t_max)) / SAH_this + costs[count_left];
            if (SAH_cost < best_cost)
            {
                best_cost  = SAH_cost;
                best_split = i;
                axis       = j;
            }
        }
    }

    // If SAH didn't find a feasible split point, assume it's stupid and continue
    // subdivision using the object mean.
    if (best_cost >= max_cost)
    {
        Eigen::Vector3f dimensions = centroid_max - centroid_min;
        for (int i = 0; i < 3; ++i)
            if (dimensions[axis] < dimensions[i])
                axis = i;

        best_split = begin + (end - begin) / 2;
    }

    std::sort(scene->begin() + begin, scene->begin() + end, BinaryCompareCentroid<Tools>(axis));
    return best_split;
}

template<typename Tools>
int AABBNode<Tools>::initialize(std::vector<typename Tools::Type> *scene, size_t max_per_leaf, BVHHeuristic heuristic)
{
    if (m_end - m_begin <= max_per_leaf)
        return -1;

    Eigen::Vector3f centroid_min, centroid_max;
    accumulate_values<Tools, CentroidMinMax<Tools> >(scene->begin() + m_begin,
                                                     scene->begin() + m_end,
                                                     &centroid_min, &centroid_max,
                                                     CentroidMinMax<Tools>());
    size_t middle = 0;
    switch(heuristic)
    {
    case BVHHeuristic_SAH:
	middle = surface_area_heuristic<Tools>(scene, centroid_min, centroid_max, m_min_point, m_max_point, m_begin, m_end);
	break;
    case BVHHeuristic_OM:
	middle = object_mean<Tools>(scene, centroid_min, centroid_max, m_begin, m_end);
	break;
    case BVHHeuristic_SM:
	middle = spatial_mean<Tools>(scene, centroid_min, centroid_max, m_begin, m_end);
	break;
    default:
	assert(!"Unsupported heuristic value");
    }
    assert(middle > m_begin && middle < m_end);

    return static_cast<int>(middle);
}

template<typename Tools>
AABBNode<Tools>::AABBNode(std::istream &in_stream)
    :	m_left_index(0),
        m_right_index(0),
        m_begin(0),
        m_end(0)
{
    uint32_t temp;
    float    temp_vec[3];

    in_stream.read((char*)&temp, sizeof(uint32_t));
    m_left_index = temp;

    in_stream.read((char*)&temp, sizeof(uint32_t));
    m_right_index = temp;

    in_stream.read((char*)&temp, sizeof(uint32_t));
    m_begin = temp;

    in_stream.read((char*)&temp, sizeof(uint32_t));
    m_end = temp;

    in_stream.read((char*)temp_vec, 3 * sizeof(float));
    m_min_point = Eigen::Vector3f(temp_vec[0], temp_vec[1], temp_vec[2]);

    in_stream.read((char*)temp_vec, 3 * sizeof(float));
    m_max_point = Eigen::Vector3f(temp_vec[0], temp_vec[1], temp_vec[2]);

    assert(m_begin <= m_end);
}

template<typename Tools>
void AABBNode<Tools>::serialize(std::ostream &out_stream) const
{
    uint32_t left_index = static_cast<uint32_t>(m_left_index);
    out_stream.write((const char*)&left_index, sizeof(uint32_t));

    uint32_t right_index = static_cast<uint32_t>(m_right_index);
    out_stream.write((const char*)&right_index, sizeof(uint32_t));

    uint32_t begin = static_cast<uint32_t>(m_begin);
    out_stream.write((const char*)&begin, sizeof(uint32_t));

    uint32_t end = static_cast<uint32_t>(m_end);
    out_stream.write((const char*)&end, sizeof(uint32_t));

    out_stream.write((const char*)m_min_point.data(), 3 * sizeof(float));
    out_stream.write((const char*)m_max_point.data(), 3 * sizeof(float));
}

template<typename Tools>
AABBNode<Tools>::AABBNode(const BVHNode& node)
    :	m_left_index(node.left_index),
        m_right_index(node.right_index),
        m_begin(node.begin),
        m_end(node.end),
        m_min_point(node.min_point[0], node.min_point[1], node.min_point[2]),
        m_max_point(node.max_point[0], node.max_point[1], node.max_point[2])
{
    assert(m_begin <= m_end);
}

template<typename Tools>
AABBNode<Tools>::operator BVHNode() const
{
    BVHNode node;
    node.left_index  = static_cast<uint32_t>(m_left_index);
    node.right_index = static_cast<uint32_t>(m_right_index);
    node.begin       = static_cast<uint32_t>(m_begin);
    node.end         = static_cast<uint32_t>(m_end);
	
    for (int i = 0; i < 3; ++i)
    {
        node.min_point[i] = m_min_point[i];
        node.max_point[i] = m_max_point[i];
    }

    return node;
}

}

#endif
