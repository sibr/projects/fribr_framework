#ifndef AABB_NODE_POINT_H
#define AABB_NODE_POINT_H

#include "ray_tracing/aabb_node.h"
#include <Eigen/Core>

namespace fribr
{

struct PointTools
{
	typedef Eigen::Vector3f Type;

	static Eigen::Vector3f centroid(const Eigen::Vector3f &v)
	{
		return v;
	}

	struct MinMax
	{
		void operator()(const Eigen::Vector3f &v, Eigen::Vector3f* out_min, Eigen::Vector3f* out_max)
		{
            Eigen::Array3f array_min = out_min->array();
            Eigen::Array3f array_max = out_max->array();

            array_min = array_min.min(v.array());
            array_max = array_max.max(v.array());

            *out_min = array_min.matrix();
            *out_max = array_max.matrix();
		}
	};
};

typedef AABBNode<PointTools> AABBNodePoint;

}

#endif
