#ifndef RAY_TRACER_STRUCTS_H
#define RAY_TRACER_STRUCTS_H

#include <cstdint>

namespace fribr
{

struct RTTriangle
{
    float    *m_vertices[3];
    void     *m_user_pointer;
    uint32_t  m_original_index;
};

struct RTResult
{
    float p[3];
    float t;
    float u;
    float v;
    const RTTriangle *tri;
};

struct RTOperation
{
    float    orig[3];
    float    dir[3];
    RTResult result;
};

struct BVHNode
{
    float    max_point[3];
    float    min_point[3];

    uint32_t begin;
    uint32_t end;
    uint32_t left_index;
    uint32_t right_index;
};

}

#endif
