#include "ray_tracing/ray_tracer.h"
#include "ray_tracing/aabb_node_triangle.h"
#include "ray_tracing/rt_intersect.h"

#include <cstdio>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cstdint>

void MD5Buffer(void* buffer, size_t bufLen, unsigned int* pDigest);

namespace
{

void permute_triangles(std::vector<fribr::RTTriangle> &triangles, const std::vector<uint32_t> &permutation)
{
    std::vector<fribr::RTTriangle> sorted_triangles(triangles.size());

    for (size_t i = 0; i < permutation.size(); ++i)
	sorted_triangles[i] = triangles[permutation[i]];

    triangles.swap(sorted_triangles);
}

void construct_hierarchy_recursive(std::vector<fribr::RTTriangle> *triangles, 
                                   std::vector<fribr::BVHNode> &hierarchy,
                                   size_t current_node, size_t begin, 
                                   size_t split, fribr::BVHHeuristic heuristic)
{
    size_t end = hierarchy[current_node].end;

    fribr::AABBNodeTriangle left(triangles, begin, split);
    int left_split = left.initialize(triangles, 16, heuristic);
    hierarchy.push_back(left);

    uint32_t left_index = static_cast<uint32_t>(hierarchy.size() - 1);
    hierarchy[current_node].left_index = left_index;
    if (left_split >= 0)
	construct_hierarchy_recursive(triangles, hierarchy, left_index, begin, left_split, heuristic);

    fribr::AABBNodeTriangle right(triangles, split, end);
    int right_split = right.initialize(triangles, 16, heuristic);
    hierarchy.push_back(right);

    uint32_t right_index = static_cast<uint32_t>(hierarchy.size() - 1);
    hierarchy[current_node].right_index = right_index;
    if (right_split >= 0)
	construct_hierarchy_recursive(triangles, hierarchy, right_index, split, right_split, heuristic);
}

}

namespace fribr
{

std::string RayTracer::compute_md5(const std::vector<float>& vertices)
{
    unsigned char digest[16];
    MD5Buffer((void*)&vertices[0], sizeof(float) * vertices.size(), (unsigned int*)digest);

    // turn into string
    char ad[33];
    for (int i = 0; i < 16; ++i)
	::sprintf(ad+i*2, "%02x", digest[i]);
    ad[32] = 0;

    return std::string(ad);
}

void RayTracer::load_hierarchy(const char* filename, std::vector<RTTriangle> &triangles)
{
    std::ifstream file(filename, std::ios::binary);

    uint32_t num_nodes = 0;
    file.read((char*)&num_nodes, sizeof(uint32_t));
    assert(num_nodes > 0);
	
    uint32_t num_triangles = 0;
    file.read((char*)&num_triangles, sizeof(uint32_t));
    assert(num_triangles == triangles.size());

    std::vector<uint32_t> permuted_triangles(num_triangles);
    file.read((char*)&permuted_triangles[0], num_triangles * sizeof(uint32_t));

    for (size_t i = 0; i < num_nodes; ++i)
	m_hierarchy.push_back(AABBNodeTriangle(file));
    assert(file.good());

    permute_triangles(triangles, permuted_triangles);
    m_triangles = &triangles;
}

void RayTracer::save_hierarchy(const char* filename)
{
    std::ofstream file(filename, std::ios::binary);

    uint32_t num_nodes = static_cast<uint32_t>(m_hierarchy.size());
    file.write((const char*)&num_nodes, sizeof(uint32_t));

    uint32_t num_triangles = static_cast<uint32_t>(m_triangles->size());
    file.write((const char*)&num_triangles, sizeof(uint32_t));

    for (size_t i = 0; i < m_triangles->size(); ++i)
    {
	uint32_t original_index = static_cast<uint32_t>((*m_triangles)[i].m_original_index);
	file.write((const char*)&original_index, sizeof(uint32_t));
    }

    for (size_t i = 0; i < m_hierarchy.size(); ++i)
	AABBNodeTriangle(m_hierarchy[i]).serialize(file);
}

void RayTracer::construct_hierarchy(std::vector<RTTriangle>& triangles, BVHHeuristic heuristic)
{
    m_triangles = &triangles;

    AABBNodeTriangle root(m_triangles, 0, m_triangles->size());
    int split = root.initialize(m_triangles, 16, heuristic);
    m_hierarchy.push_back(root);

    if (split >= 0)
	construct_hierarchy_recursive(m_triangles, m_hierarchy, root.get_left_index(), 0, static_cast<size_t>(split), heuristic);
}

void RayTracer::destroy_hierarchy()
{
    m_triangles = 0;
    m_hierarchy.clear();
}

bool RayTracer::ray_cast_any(const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const
{
    return ray_cast(orig, dir).tri != 0;
}

void RayTracer::intersect_with_triangles(size_t begin, size_t end, const Eigen::Vector3f &orig, const Eigen::Vector3f &dir, RTResult *out) const
{
    for (size_t i = begin; i < end; ++i)
    {
        float t, u, v;
        if (MollerTrumbore::intersect_triangle1(orig.data(),
                                                dir.data(),
                                                (*m_triangles)[i].m_vertices[0],
                                                (*m_triangles)[i].m_vertices[1],
                                                (*m_triangles)[i].m_vertices[2],
                                                t, u, v))
        {
            if (t > 0.0f && t < out->t)
            {
                out->tri = &(*m_triangles)[i];
                out->t = t;
                out->u = u;
                out->v = v;
            }
        }
    }
}

RTResult RayTracer::ray_cast_hierarchy(const Eigen::Vector3f &orig, const Eigen::Vector3f &dir, const Eigen::Vector3f &inv_dir) const
{
    static const float EPSILON = 0.000001f;
	
    RTResult result;
    result.tri = 0;
    result.t = 1.0f;
    result.u = 0.0f;
    result.v = 0.0f;

    typedef std::pair<size_t, float> StackNode;
    std::vector<StackNode> node_stack;
    node_stack.reserve(128);

    StackNode current = std::make_pair(0, 1.0f);
    if (!bounding_box_intersect(m_hierarchy[current.first].min_point, m_hierarchy[current.first].max_point,
                                orig.data(), inv_dir.data(), &current.second))
        return result;

    for (;;)
    {
        if (current.second < result.t + EPSILON)
        {
            const BVHNode& node = m_hierarchy[current.first];
            size_t i_left  = node.left_index;
            size_t i_right = node.right_index;

            size_t own_triangles_begin = (i_left || i_right) ? node.end : node.begin;

            // First we check whether we intersect with anything stored in the current node.
            intersect_with_triangles(own_triangles_begin, node.end, orig, dir, &result);

            float t_left  = 2.0f;
            if (i_left)
                bounding_box_intersect(m_hierarchy[i_left].min_point, m_hierarchy[i_left].max_point,
            orig.data(), inv_dir.data(), &t_left);
            float t_right = 2.0f;
            if (i_right)
                bounding_box_intersect(m_hierarchy[i_right].min_point, m_hierarchy[i_right].max_point,
            orig.data(), inv_dir.data(), &t_right);

            if (std::min(t_left, t_right) < result.t + EPSILON)
            {
                StackNode first_node  = std::make_pair(i_left,  t_left);
                StackNode second_node = std::make_pair(i_right, t_right);
                if (first_node.second > second_node.second)
                    std::swap(first_node, second_node);

                current = first_node;
                if (second_node.first)
                    node_stack.push_back(second_node);

                continue;
            }
        }

        if (node_stack.empty())
        {
            Eigen::Vector3f p = orig + result.t * dir;
            result.p[0] = p[0];
            result.p[1] = p[1];
            result.p[2] = p[2];
            return result;
        }

        current = node_stack.back();
        node_stack.pop_back();
    }
}

RTResult RayTracer::ray_cast(const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const
{
    static const float EPSILON = 0.000001f;
    Eigen::Vector3f inv_dir;
    inv_dir[0] = (fabs(dir[0]) < EPSILON) ? 0.0f : 1.0f / dir[0];
    inv_dir[1] = (fabs(dir[1]) < EPSILON) ? 0.0f : 1.0f / dir[1];
    inv_dir[2] = (fabs(dir[2]) < EPSILON) ? 0.0f : 1.0f / dir[2];

    return ray_cast_hierarchy(orig, dir, inv_dir);
}

}
