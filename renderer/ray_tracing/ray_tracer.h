#ifndef RAY_TRACER_H
#define RAY_TRACER_H

#include "ray_tracing/ray_tracer_structs.h"
#include "ray_tracing/aabb_node.h"

#include <Eigen/Core>
#include <vector>

namespace fribr
{

class RayTracer
{
public:
    void construct_hierarchy(std::vector<RTTriangle>& triangles, BVHHeuristic heuristic = BVHHeuristic_SAH);
    void destroy_hierarchy(); 

    void save_hierarchy(const char *filename);
    void load_hierarchy(const char *filename, std::vector<RTTriangle> &triangles);

    RTResult ray_cast(const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const;
    bool     ray_cast_any(const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const;

    static std::string compute_md5(const std::vector<float> &vertices);

private:
    void     intersect_with_triangles(size_t begin, size_t end, const Eigen::Vector3f &orig, const Eigen::Vector3f &dir, RTResult *out) const;
    RTResult ray_cast_hierarchy(const Eigen::Vector3f &orig, const Eigen::Vector3f &dir, const Eigen::Vector3f &inv_dir) const;

    std::vector<RTTriangle> *m_triangles;
    std::vector<BVHNode>     m_hierarchy;
};

}

#endif
