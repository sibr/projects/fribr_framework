#ifndef MESHRAYTRACER_H
#define MESHRAYTRACER_H

#include "3d.h"
#include "ray_tracing/ray_tracer.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace patcher
{

class MeshRayTracer
{
public:
    fribr::RTResult ray_cast     (const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const;
    bool            ray_cast_any (const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const;
    void            set_scene    (fribr::Scene::Ptr scene);

private:
    std::vector<float>             m_rt_vertices;
    std::vector<fribr::RTTriangle> m_triangles;
    fribr::RayTracer               m_ray_tracer;
};

}

#endif
