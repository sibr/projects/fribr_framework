#include "ray_tracing/bvh_search.h"
#include "ray_tracing/aabb_node_point.h"
#include "tools/sized_priority_queue.h"

#include <cstdio>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cstdint>

void MD5Buffer(void* buffer, size_t bufLen, unsigned int* pDigest);

namespace
{

void construct_hierarchy_recursive(std::vector<Eigen::Vector3f> *points, 
                                   std::vector<fribr::AABBNodePoint> &hierarchy,
                                   size_t current_node, size_t begin, 
                                   size_t split, fribr::BVHHeuristic heuristic)
{
    size_t end = hierarchy[current_node].get_end();

    fribr::AABBNodePoint left(points, begin, split);
    int left_split = left.initialize(points, 16, heuristic);
    hierarchy.push_back(left);

    uint32_t left_index = static_cast<uint32_t>(hierarchy.size() - 1);
    hierarchy[current_node].set_left_index(left_index);
    if (left_split >= 0)
        construct_hierarchy_recursive(points, hierarchy, left_index, begin, left_split, heuristic);

    fribr::AABBNodePoint right(points, split, end);
    int right_split = right.initialize(points, 16, heuristic);
    hierarchy.push_back(right);

    uint32_t right_index = static_cast<uint32_t>(hierarchy.size() - 1);
    hierarchy[current_node].set_right_index(right_index);
    if (right_split >= 0)
        construct_hierarchy_recursive(points, hierarchy, right_index, split, right_split, heuristic);
}

float min_aabb_distance_squared(const Eigen::Vector3f &p, const fribr::AABBNodePoint &node)
{
    Eigen::Array3f min_point = node.get_min_point().array();
    Eigen::Array3f max_point = node.get_max_point().array();
    
    Eigen::Array3f delta = (min_point - p.array()).max(Eigen::Array3f(0.0f, 0.0f, 0.0f)) +
                           (p.array() - max_point).max(Eigen::Array3f(0.0f, 0.0f, 0.0f));

    return delta.matrix().squaredNorm();
}

static float mahalanobis_distance_squared(const Eigen::Vector3f &pa,
                                          const Eigen::Vector3f &na,
                                          const Eigen::Vector3f &pb)
{
    static const float SQUASH  = 8.0f;
    Eigen::Vector3f delta      = pb - pa;
    Eigen::Vector3f proj_delta = delta.dot(na) * na;
    Eigen::Vector3f orth_delta = delta - proj_delta;

    return (delta + orth_delta * SQUASH).squaredNorm();
}

}

namespace fribr
{

std::string BVHSearch::compute_md5(const std::vector<float3>& vertices)
{
    unsigned char digest[16];
    MD5Buffer((void*)&vertices[0], sizeof(float3) * vertices.size(), (unsigned int*)digest);

    // turn into string
    char ad[33];
    for (int i = 0; i < 16; ++i)
	::sprintf(ad+i*2, "%02x", digest[i]);
    ad[32] = 0;

    return std::string(ad);
}

void BVHSearch::load_hierarchy(const char* filename)
{
    std::ifstream file(filename, std::ios::binary);

    uint32_t num_nodes = 0;
    file.read((char*)&num_nodes, sizeof(uint32_t));
    assert(num_nodes > 0);
	
    uint32_t num_points = 0;
    file.read((char*)&num_points, sizeof(uint32_t));

    std::vector<float3> points(num_points);
    file.read((char*)&points[0], num_points * sizeof(float3));

    for (size_t i = 0; i < num_nodes; ++i)
        m_hierarchy.push_back(AABBNodePoint(file));
    assert(file.good());

    m_points.clear();
    m_points.reserve(points.size());
    for (float3 p : points)
        m_points.push_back(make_vec3f(p));
}

void BVHSearch::save_hierarchy(const char* filename)
{
    std::ofstream file(filename, std::ios::binary);

    uint32_t num_nodes = static_cast<uint32_t>(m_hierarchy.size());
    file.write((const char*)&num_nodes, sizeof(uint32_t));

    uint32_t num_points = static_cast<uint32_t>(m_points.size());
    file.write((const char*)&num_points, sizeof(uint32_t));

    for (size_t i = 0; i < m_points.size(); ++i)
    {
        float3 p = make_float3(m_points[i]);
        file.write((const char*)&p, sizeof(float3));
    }

    for (size_t i = 0; i < m_hierarchy.size(); ++i)
        AABBNodePoint(m_hierarchy[i]).serialize(file);
}

void BVHSearch::construct_hierarchy(std::vector<float3>& points, BVHHeuristic heuristic)
{
    m_points.clear();
    m_points.reserve(points.size());
    for (float3 p : points)
        m_points.push_back(make_vec3f(p));

    AABBNodePoint root(&m_points, 0, m_points.size());
    int split = root.initialize(&m_points, 16, heuristic);
    m_hierarchy.push_back(root);

    if (split >= 0)
        construct_hierarchy_recursive(&m_points, m_hierarchy, root.get_left_index(), 0, static_cast<size_t>(split), heuristic);
}

void BVHSearch::destroy_hierarchy()
{
    m_points.clear();
    m_hierarchy.clear();
}

bool BVHSearch::find_nearest(Eigen::Vector3f p, Eigen::Vector3f* nearest_out,
			     float guess_sqr_distance, int guess_index) const
{
    if (m_hierarchy.empty() || !nearest_out)
        return false;

    typedef std::pair<float, int> DistanceIndex;
    std::vector<DistanceIndex> node_stack;
    node_stack.reserve(128);

    DistanceIndex current = std::make_pair(0.0f, 0);
    DistanceIndex best    = std::make_pair(guess_sqr_distance, guess_index);
    for (;;)
    {
	const AABBNodePoint& node = m_hierarchy[current.second];
	size_t i_first  = node.get_left_index();
	size_t i_second = node.get_right_index();

	size_t nodes_begin = (i_first || i_second) ? node.get_end() : node.get_begin();
	for (size_t i = nodes_begin; i < node.get_end(); ++i)
	{
            float distanceSquared = (m_points[i] - p).squaredNorm();
	    if (distanceSquared < best.first)
		best = std::make_pair(distanceSquared, static_cast<int>(i));
	}

        float d_first  = i_first  ? min_aabb_distance_squared(p, m_hierarchy[i_first])  : 1e21f;
        float d_second = i_second ? min_aabb_distance_squared(p, m_hierarchy[i_second]) : 1e21f;
        if (d_second < d_first)
        {
            std::swap(d_second, d_first);
            std::swap(i_second, i_first);
        }

	if (i_second && d_second <= best.first)
	    node_stack.push_back(std::make_pair(d_second, i_second));
	
	if (i_first && d_first <= best.first)
	    node_stack.push_back(std::make_pair(d_first, i_first));

        do {
            if (node_stack.empty())
                goto knn_done;

            current = node_stack.back();
            node_stack.pop_back();
        } while(current.first > best.first);
    }

    knn_done:
    if (best.second < 0)
	return false;
    
    *nearest_out = m_points[best.second];
    return true;
}
    
std::vector<Eigen::Vector3f> BVHSearch::find_knn(Eigen::Vector3f p, int k) const
{
    if (m_hierarchy.empty())
        return std::vector<Eigen::Vector3f>();

    typedef std::pair<float, int> DistanceIndex;
    std::vector<DistanceIndex> node_stack;
    node_stack.reserve(128);

    typedef fribr::SizedPriorityQueue<DistanceIndex> KNNQueue;
    KNNQueue best_matches;
    best_matches.set_capacity(k);

    DistanceIndex current = std::make_pair(0.0f, 0);
    for (;;)
    {
	const AABBNodePoint& node = m_hierarchy[current.second];
	size_t i_first  = node.get_left_index();
	size_t i_second = node.get_right_index();

	size_t nodes_begin  = (i_first || i_second) ? node.get_end() : node.get_begin();
	for (size_t i = nodes_begin; i < node.get_end(); ++i)
	{
            float distanceSquared = (m_points[i] - p).squaredNorm();
            best_matches.push(std::make_pair(distanceSquared, static_cast<int>(i)));
	}

        float d_first  = i_first  ? min_aabb_distance_squared(p, m_hierarchy[i_first])  : 1e21f;
        float d_second = i_second ? min_aabb_distance_squared(p, m_hierarchy[i_second]) : 1e21f;
        if (d_second < d_first)
        {
            std::swap(d_second, d_first);
            std::swap(i_second, i_first);
        }

	if (i_second && (!best_matches.full() || d_second <= best_matches.top().first))
	    node_stack.push_back(std::make_pair(d_second, i_second));

	if (i_first && (!best_matches.full() || d_first <= best_matches.top().first))
		    node_stack.push_back(std::make_pair(d_first, i_first));

        do {
            if (node_stack.empty())
                goto knn_done;

            current = node_stack.back();
            node_stack.pop_back();
        } while(best_matches.full() && current.first > best_matches.top().first);
    }

    knn_done:
    std::vector<Eigen::Vector3f> neighbors;
    for (DistanceIndex di : best_matches.get_storage())
        neighbors.push_back(m_points[di.second]);
    return neighbors;
}

std::vector<Eigen::Vector3f> BVHSearch::find_radius(Eigen::Vector3f p, float r, int max_neighbors) const
{
    if (m_hierarchy.empty())
        return std::vector<Eigen::Vector3f>();

    typedef std::pair<float, int> DistanceIndex;
    std::vector<DistanceIndex> node_stack;
    node_stack.reserve(128);

    std::vector<Eigen::Vector3f> neighbors;

    DistanceIndex current = std::make_pair(0.0f, 0);
    for (;;)
    {
	const AABBNodePoint& node = m_hierarchy[current.second];
	size_t i_first  = node.get_left_index();
	size_t i_second = node.get_right_index();

	size_t nodes_begin  = (i_first || i_second) ? node.get_end() : node.get_begin();
	for (size_t i = nodes_begin; i < node.get_end(); ++i)
	{
            float distanceSquared = (m_points[i] - p).squaredNorm();
            if (distanceSquared <= r * r)
                neighbors.push_back(m_points[i]);

            if (max_neighbors > 0 && neighbors.size() >= static_cast<size_t>(max_neighbors))
                return neighbors;
	}

        float d_first  = i_first  ? min_aabb_distance_squared(p, m_hierarchy[i_first])  : 1e21f;
        float d_second = i_second ? min_aabb_distance_squared(p, m_hierarchy[i_second]) : 1e21f;

	if (i_second && d_second <= r * r)
	    node_stack.push_back(std::make_pair(d_second, i_second));

	if (i_first && d_first <= r * r)
	    node_stack.push_back(std::make_pair(d_first, i_first));

        if (node_stack.empty())
            return neighbors;

        current = node_stack.back();
        node_stack.pop_back();
    }
}

std::vector<Eigen::Vector3f> BVHSearch::find_radius_mahalanobis(Eigen::Vector3f p, Eigen::Vector3f n,
                                                                float r, int max_neighbors) const
{
    if (m_hierarchy.empty())
        return std::vector<Eigen::Vector3f>();

    typedef std::pair<float, int> DistanceIndex;
    std::vector<DistanceIndex> node_stack;
    node_stack.reserve(128);

    std::vector<Eigen::Vector3f> neighbors;

    DistanceIndex current = std::make_pair(0.0f, 0);
    for (;;)
    {
	const AABBNodePoint& node = m_hierarchy[current.second];
	size_t i_first  = node.get_left_index();
	size_t i_second = node.get_right_index();

	size_t nodes_begin  = (i_first || i_second) ? node.get_end() : node.get_begin();
	for (size_t i = nodes_begin; i < node.get_end(); ++i)
	{
            float distanceSquared = mahalanobis_distance_squared(p, n, m_points[i]);
            if (distanceSquared <= r * r)
                neighbors.push_back(m_points[i]);

            if (max_neighbors > 0 && neighbors.size() >= static_cast<size_t>(max_neighbors))
                return neighbors;
	}

        float d_first  = i_first  ? min_aabb_distance_squared(p, m_hierarchy[i_first])  : 1e21f;
        float d_second = i_second ? min_aabb_distance_squared(p, m_hierarchy[i_second]) : 1e21f;

	if (i_second && d_second <= r * r)
	    node_stack.push_back(std::make_pair(d_second, i_second));

	if (i_first && d_first <= r * r)
		    node_stack.push_back(std::make_pair(d_first, i_first));

        if (node_stack.empty())
            return neighbors;

        current = node_stack.back();
        node_stack.pop_back();
    }
}

}
