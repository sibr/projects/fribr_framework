#ifndef AABB_NODE_TRIANGLE_H
#define AABB_NODE_TRIANGLE_H

#include "ray_tracing/aabb_node.h"
#include "ray_tracing/ray_tracer.h"
#include "ray_tracing/ray_tracer_structs.h"

namespace fribr
{

struct TriangleTools
{
    typedef RTTriangle Type;

    static Eigen::Vector3f centroid(const RTTriangle& t)
    {
        return Eigen::Vector3f(t.m_vertices[0][0] + t.m_vertices[1][0] + t.m_vertices[2][0],
                               t.m_vertices[0][1] + t.m_vertices[1][1] + t.m_vertices[2][1],
                               t.m_vertices[0][2] + t.m_vertices[1][2] + t.m_vertices[2][2]) * (1.0f / 3.0f);
    }

    struct MinMax
    {
        void operator()(const RTTriangle &triangle, Eigen::Vector3f *out_min, Eigen::Vector3f *out_max)
        {
            Eigen::Array3f array_min = out_min->array();
            Eigen::Array3f array_max = out_max->array();
            for (size_t j = 0; j < 3; ++j)
            {
                Eigen::Array3f vertex(triangle.m_vertices[j][0],
                                      triangle.m_vertices[j][1],
                                      triangle.m_vertices[j][2]);
                array_min = array_min.min(vertex);
                array_max = array_max.max(vertex);
            }
            *out_min = array_min.matrix();
            *out_max = array_max.matrix();
        }
    };
};

typedef AABBNode<TriangleTools> AABBNodeTriangle;

}

#endif
