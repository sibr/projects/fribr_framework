#include "tools/image_tools.h"
#include "tools/string_tools.h"

#ifdef INRIA_WIN
#ifndef NOMINMAX
#define NOMINMAX
#endif
#endif

#if defined(_WIN32)
#include "Winsock2.h"
#elif defined(__linux__)
#include <netinet/in.h>
#else
#error "Could not find ntohl"
#endif

#include <3d/types.h>

#include <cstdint>
#include <fstream>
#include <iostream>

namespace
{

// Adopted from http://www.64lines.com/jpeg-width-height. Gets the JPEG size from the file stream passed
// to the function, file reference: http://www.obrador.com/essentialjpeg/headerinfo.htm
Eigen::Vector2i get_jpeg_size(std::ifstream& file)
{
    // Check for valid JPG
    if (file.get() != 0xFF || file.get() != 0xD8)
        return Eigen::Vector2i(-1, -1);
    file.get(); file.get(); // Skip the rest of JPG identifier.

    std::streampos block_length = static_cast<std::streampos>(file.get() * 256 + file.get() - 2);
    for (;;)
    {
        // Skip the first block since it doesn't contain the resolution
        file.seekg(file.tellg() + block_length);

        // Check if we are at the start of another block
        if (!file.good() || file.get() != 0xFF)
            break;

        // If the block is not the "Start of frame", skip to the next block
        if (file.get() != 0xC0)
        {
            block_length = static_cast<std::streampos>(file.get() * 256 + file.get() - 2);
            continue;
        }
   
        // Found the appropriate block. Extract the dimensions.
        for (int i = 0; i < 3; ++i) file.get();
       
        int height = file.get() * 256 + file.get();
        int width  = file.get() * 256 + file.get();
        return Eigen::Vector2i(width, height);
    }

   return Eigen::Vector2i(-1, -1);
}

}

namespace fribr
{

// Adopted from http://stackoverflow.com/questions/22638755/image-dimensions-without-loading
// kudos to Lukas (http://stackoverflow.com/users/643315/lukas).
Eigen::Vector2i image_resolution(const std::string& file_path)
{
    enum ValidFormats {
        PNG = 0,
        BMP,
        TGA,
        JPG,
        JPEG,
        VALID_COUNT 
    };

    std::string valid_extensions[] = {
        "png",
        "bmp",
        "tga",
        "jpg",
        "jpeg"
    };

    std::string extension = to_lower(get_extension(file_path));
    int extension_id = 0;
    while (extension_id < VALID_COUNT &&
           extension != valid_extensions[extension_id])
        extension_id++;

    if (extension_id == VALID_COUNT)
        return Eigen::Vector2i(-1, -1);

    std::ifstream file(file_path, std::ios::binary);
    if (!file.good())
        return Eigen::Vector2i(-1, -1);

    uint32_t temp   = 0;
    int32_t  width  = -1;
    int32_t  height = -1;
    switch (extension_id)
    {
    case PNG: 
       file.seekg(16);
        file.read(reinterpret_cast<char*>(&width), 4);
        file.read(reinterpret_cast<char*>(&height), 4);
#ifndef INRIA_WIN
        width  = ntohl(width);
        height = ntohl(height);
#endif
        break;
    case BMP:
        file.seekg(14);
        file.read(reinterpret_cast<char*>(&temp), 4);
        if (temp == 40) // Windows Format
        {
            file.read(reinterpret_cast<char*>(&width), 4);
            file.read(reinterpret_cast<char*>(&height), 4);
        }
        else if (temp == 20) // MAC Format
        {
            file.read(reinterpret_cast<char*>(&width), 2);
            file.read(reinterpret_cast<char*>(&height), 2);
        }
        break;
    case TGA:
        file.seekg(12);
        file.read(reinterpret_cast<char*>(&width), 2);
        file.read(reinterpret_cast<char*>(&height), 2);
        break;
    case JPG:
    case JPEG:
        return get_jpeg_size(file);
        break;
    }

    return Eigen::Vector2i(width, height);
}

cv::Mat load_image(const std::string &file_path, int max_image_width, int flags)
{
    cv::Mat input_image = cv::imread(file_path, flags);
    if (input_image.empty())
    {
        std::cerr << "Could not load calibrated image: " << file_path << std::endl;
        return cv::Mat();
    }
    if (max_image_width <= 0)
        return input_image;

    Eigen::Vector2i input_resolution(input_image.cols, input_image.rows);
    const float resize_factor = std::min(1.0f, static_cast<float>(max_image_width) / input_image.cols);
    
    size_t resized_x = size_t(resize_factor * input_image.cols);
    size_t resized_y = size_t(resize_factor * input_image.rows);

    cv::Mat distorted_image;
    cv::resize(input_image, distorted_image, cv::Size(int(resized_x), int(resized_y)), 0.0, 0.0, cv::INTER_AREA); 

    return distorted_image;
}

cv::Mat load_undistorted_image(const std::string &file_path,
                               Eigen::Vector2f f, 
                               Eigen::Vector2f d,
                               Eigen::Vector3f k,
                               int max_image_width,
                               int flags)
{
    cv::Mat input_image = cv::imread(file_path, flags);
    if (input_image.empty())
    {
        std::cerr << "Could not load calibrated image: " << file_path << std::endl;
        return cv::Mat();
    }
    Eigen::Vector2i input_resolution(input_image.cols, input_image.rows);
    
    float resize_factor = 1.0f;
    if (max_image_width > 0)
        resize_factor = std::min(1.0f, static_cast<float>(max_image_width) / input_image.cols);
    
    size_t resized_x = size_t(resize_factor * input_image.cols);
    size_t resized_y = size_t(resize_factor * input_image.rows);

    cv::Mat distorted_image;
    cv::resize(input_image, distorted_image, cv::Size(int(resized_x), int(resized_y)), 0.0, 0.0, cv::INTER_AREA); 

    Eigen::Vector2f resized_d     = d * resize_factor;
    Eigen::Vector2f resized_focal = f * resize_factor;
    k.x() *= resize_factor * resize_factor;
    k.y() *= resize_factor * resize_factor * resize_factor * resize_factor;
    k.z() *= resize_factor * resize_factor * resize_factor * resize_factor * resize_factor * resize_factor;

    cv::Mat undistorted_image;
    cv::Mat *output_image = &distorted_image;
    if (k.norm() > 0.000001f)
    {
        cv::Mat intrinsics = cv::Mat::zeros(3, 3, CV_32FC1);
        intrinsics.at<float>(0, 0)  = resized_focal.x();
        intrinsics.at<float>(1, 1)  = resized_focal.y();
        intrinsics.at<float>(0, 2)  = resized_d.x();
        intrinsics.at<float>(1, 2)  = resized_d.y();
        intrinsics.at<float>(2, 2)  = 1.0f;

        cv::Mat distortion_coefficients = cv::Mat::zeros(1, 5, CV_32FC1);
        distortion_coefficients.at<float>(0, 0) = k.x();
        distortion_coefficients.at<float>(0, 1) = k.y();
        distortion_coefficients.at<float>(0, 4) = k.z();

        cv::undistort(distorted_image, undistorted_image, intrinsics, distortion_coefficients);
        output_image = &undistorted_image;
    }

    return *output_image;
}

}
