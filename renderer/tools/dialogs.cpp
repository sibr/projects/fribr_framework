#include "tools/dialogs.h"

#include <nfd.h>

namespace fribr
{

std::string open_file_dialog(const std::string &filters)
{
    nfdchar_t   *cpath  = NULL;
    nfdresult_t  result = NFD_OpenDialog(filters.c_str(), NULL, &cpath);
    if (result != NFD_OKAY)
        return std::string();

    std::string path(cpath);
    free(cpath);
    return path;
}

std::string save_file_dialog(const std::string &filters)
{
    nfdchar_t   *cpath  = NULL;
    nfdresult_t  result = NFD_SaveDialog(filters.c_str(), NULL, &cpath);
    if (result != NFD_OKAY)
        return std::string();

    std::string path(cpath);
    free(cpath);
    return path;
}

}
