#ifndef SIZED_PRIORITY_QUEUE_H
#define SIZED_PRIORITY_QUEUE_H

#include <vector>
#include <algorithm>

namespace fribr
{

template<typename T>
class SizedPriorityQueue
{
public:
    SizedPriorityQueue(size_t capacity = 4) : m_capacity(capacity) { m_storage.reserve(capacity); }
    
          void            clear()                         { m_storage.clear(); }
          void            set_capacity(size_t capacity)   { m_capacity = capacity; m_storage.reserve(capacity); }

          std::vector<T>& get_storage()           throw() { return m_storage; }
    const std::vector<T>& get_storage()     const throw() { return m_storage; }
          void            push(const T &t);
    
    const T&              top()             const         { return m_storage.front(); }
          void            pop()                           { std::pop_heap(m_storage.begin(), m_storage.end()); m_storage.pop_back(); }
          bool            empty()           const throw() { return m_storage.empty(); }
          bool            full()            const throw() { return m_storage.size() == m_capacity; }
        

private:
    size_t         m_capacity;
    std::vector<T> m_storage;
};

template<typename T>
void SizedPriorityQueue<T>::push(const T &t)
{
    if (!full())
    {
        m_storage.push_back(t);
        std::push_heap(m_storage.begin(), m_storage.end());
        return;
    }

    if (t >= top())
        return;

    pop();
    m_storage.push_back(t);
    std::push_heap(m_storage.begin(), m_storage.end());
}

}

#endif
