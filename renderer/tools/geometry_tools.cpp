#include "geometry_tools.h"

namespace fribr
{

std::vector<float> create_plane(Eigen::Array3f bbox_min, Eigen::Array3f bbox_max,
                                Eigen::Matrix3f rotation, Eigen::Vector3f position)
{
    std::vector<float> vertices;
    vertices.reserve(3 * 12);
    Eigen::Array3f delta = bbox_max - bbox_min;
    Eigen::Vector3f v;

    // Front face
    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                                     bbox_min[1] + 0.0f * delta[1],
                                                     bbox_min[2] + 0.51f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                                     bbox_min[1] + 0.0f * delta[1],
                                                     bbox_min[2] + 0.51f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                                     bbox_min[1] + 1.0f * delta[1],
                                                     bbox_min[2] + 0.51f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                                     bbox_min[1] + 1.0f * delta[1],
                                                     bbox_min[2] + 0.51f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                                     bbox_min[1] + 0.0f * delta[1],
                                                     bbox_min[2] + 0.51f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                                     bbox_min[1] + 1.0f * delta[1],
                                                     bbox_min[2] + 0.51f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    // Back face
    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                                     bbox_min[1] + 0.0f * delta[1],
                                                     bbox_min[2] + 0.49f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                                     bbox_min[1] + 1.0f * delta[1],
                                                     bbox_min[2] + 0.49f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                                     bbox_min[1] + 0.0f * delta[1],
                                                     bbox_min[2] + 0.49f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                                     bbox_min[1] + 1.0f * delta[1],
                                                     bbox_min[2] + 0.49f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                                     bbox_min[1] + 1.0f * delta[1],
                                                     bbox_min[2] + 0.49f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);


    v = position + 2.0f * rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                                     bbox_min[1] + 0.0f * delta[1],
                                                     bbox_min[2] + 0.49f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);
    
    return vertices;
}

std::vector<float> create_box(Eigen::Array3f bbox_min, Eigen::Array3f bbox_max,
			      Eigen::Matrix3f rotation, Eigen::Vector3f position)
{
    std::vector<float> vertices;
    vertices.reserve(3 * 18);
    Eigen::Array3f delta = bbox_max - bbox_min;
    Eigen::Vector3f v;

    // z = 0
    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    // z = 1
    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    // x = 0
    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    // x = 1
    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    // y = 0
    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 0.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    // y = 1
    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 0.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 0.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);

    v = position + rotation * Eigen::Vector3f(bbox_min[0] + 1.0f * delta[0],
                                              bbox_min[1] + 1.0f * delta[1],
                                              bbox_min[2] + 1.0f * delta[2]);
    vertices.push_back(v[0]); vertices.push_back(v[1]); vertices.push_back(v[2]);
    
    return vertices;
}

} // fribr
