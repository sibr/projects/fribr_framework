#include "tools/file_tools.h"
#include <fstream>

#if defined(_WIN32)
#include "Windows.h"
#include <direct.h>
#else
#include <sys/stat.h>
#endif

namespace fribr
{

bool file_exists(const std::string &path) noexcept {
    std::ifstream file(path);
    return file.good();
}

bool directory_exists(const std::string &path) noexcept {
#if defined(_WIN32)
    DWORD attributes = GetFileAttributes(path.c_str());
    return (attributes != INVALID_FILE_ATTRIBUTES && 
            (attributes & FILE_ATTRIBUTE_DIRECTORY));
#else
    struct stat sb;
    return stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode);
#endif
}

void make_directory(const std::string &path) noexcept {
#if defined(_WIN32)
    _mkdir(path.c_str());
#else
    mkdir(path.c_str(), 0755);
#endif
}

}
