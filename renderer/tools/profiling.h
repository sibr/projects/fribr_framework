#ifndef PROFILING_H
#define PROFILING_H

#include <iostream>
#include <chrono>
#define TIC() auto profiling_start_time = std::chrono::high_resolution_clock::now()
#define RETIC() profiling_start_time = std::chrono::high_resolution_clock::now()
#define TOC(title,it) do { std::chrono::duration<double> elapsed_seconds = std::chrono::high_resolution_clock::now() - profiling_start_time; \
                           std::cout << title << ": " << elapsed_seconds.count() / it << " s" << std::endl; } while(0)

#endif
