#ifndef DIALOGS_H
#define DIALOGS_H

#include <string>

namespace fribr
{

std::string open_file_dialog(const std::string &filters = "");
std::string save_file_dialog(const std::string &filters = "");

}

#endif
