#ifndef FILE_TOOLS_H
#define FILE_TOOLS_H

#include <string>

namespace fribr
{

bool file_exists(const std::string &path) throw();
bool directory_exists(const std::string &path) throw();
void make_directory(const std::string &path) throw();

}

#endif
