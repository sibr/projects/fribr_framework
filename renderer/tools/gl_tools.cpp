#include "tools/gl_tools.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include <core/graphics/Config.hpp>

namespace fribr
{

void draw_full_screen_quad()
{
    const static float positions[] =
	{
	    -1.0f, -1.0f,
	     1.0f, -1.0f,
	    -1.0f,  1.0f,
	     1.0f,  1.0f
	};
	
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glEnableVertexAttribArray(0);
	CHECK_GL_ERROR;
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, positions);
	CHECK_GL_ERROR;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableVertexAttribArray(0);
}

}
