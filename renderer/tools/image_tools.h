#ifndef IMAGE_TOOLS_H
#define IMAGE_TOOLS_H

#include <opencv2/opencv.hpp>
#include <Eigen/Core>

namespace fribr
{

Eigen::Vector2i image_resolution(const std::string &file_path);
cv::Mat         load_image(const std::string &file_path, int max_image_width, int flags);
cv::Mat         load_undistorted_image(const std::string &file_path,
                                       Eigen::Vector2f f, 
                                       Eigen::Vector2f d,
                                       Eigen::Vector3f k,
                                       int max_image_width,
                                       int flags);

}

#endif
