#ifndef GEOMETRY_TOOLS_H
#define GEOMETRY_TOOLS_H

#include <vector>
#include <Eigen/Core>

namespace fribr
{

std::vector<float> create_plane(Eigen::Array3f bbox_min, Eigen::Array3f bbox_max,
                                Eigen::Matrix3f rotation, Eigen::Vector3f position);

std::vector<float> create_box(Eigen::Array3f bbox_min, Eigen::Array3f bbox_max,
                              Eigen::Matrix3f rotation, Eigen::Vector3f position);

}

#endif
