#ifndef TOOLS_H
#define TOOLS_H


#include "tools/dialogs.h"
#include "tools/sized_priority_queue.h"
#include "tools/string_tools.h"
#include "tools/file_tools.h"
#include "tools/gl_tools.h"
#include "tools/geometry_tools.h"
#include "tools/image_tools.h"
#include "tools/profiling.h"

#ifndef M_PI
#define M_PI 3.141592653589793238462643383279502884197169399375105820974944592308
#endif
#endif
