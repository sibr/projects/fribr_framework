#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include "gl_wrappers/enumerations.h"
#include "gl_wrappers/texture.h"

#include <opencv2/opencv.hpp>
#include <GL/glew.h>
#include <GL/gl.h>

#include <Eigen/Core>

#include <vector>
#include <memory>
#include <exception>

namespace fribr
{

class Framebuffer
{
public:
    typedef std::shared_ptr<Framebuffer> Ptr;
    
    Framebuffer(Eigen::Vector2i resolution, const std::vector<Texture::Descriptor> &descriptors) noexcept(false);
    ~Framebuffer() noexcept;
    
          cv::Mat                    read_texture(size_t index, ReadbackMode mode)           noexcept(false);
    const std::vector<Texture::Ptr>& get_textures()                                    const noexcept { return m_textures; }
          Eigen::Vector2i            get_resolution()                                  const noexcept { return m_resolution; }
	    
          void                       set_resolution(Eigen::Vector2i resolution) noexcept(false);

          void bind()         noexcept;
          void unbind() const noexcept;

private:
    Eigen::Vector2i                  m_resolution;
    GLint                            m_old_viewport[4];
    GLuint                           m_fbo;
    GLint                            m_old_fbo;
    GLuint                           m_depth_buffer;
    std::vector<Texture::Descriptor> m_descriptors;
    std::vector<Texture::Ptr>        m_textures;

    // Deliberately left unimplmented, Framebuffer is non-copyable.
    Framebuffer(const Framebuffer &rhs);
    Framebuffer& operator=(const Framebuffer &rhs);
};

cv::Mat read_framebuffer(Eigen::Vector2i resolution, size_t format, ReadbackMode mode) noexcept;

}

#endif
