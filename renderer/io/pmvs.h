#ifndef PMVS_H
#define PMVS_H

#include "3d/point_cloud.h"
#include "io/observations.h"

namespace fribr
{

namespace PMVS
{

PointCloud::Ptr load_point_cloud(const std::string &file_path);  
Observations    load_observations(const std::string &file_path, int num_cameras = 0);

}

}

#endif
