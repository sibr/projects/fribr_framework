#ifndef OBSERVATIONS_H
#define OBSERVATIONS_H

#include <Eigen/Core>
#include <vector>

namespace fribr
{

struct Observations
{
    std::vector<Eigen::Vector3f>   points;
    std::vector<std::vector<int> > observations;
};

}

#endif
