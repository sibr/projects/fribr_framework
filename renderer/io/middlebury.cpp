#include "io/middlebury.h"
#include "tools/string_tools.h"
#include "tools/image_tools.h"
#include "3d/point_cloud.h"
#include "gl_wrappers/texture.h"

#include <Eigen/Core>

#include <fstream>
#include <sstream>

namespace fribr
{

PointCloud::Ptr Middlebury::load_point_cloud(const std::string &file_path)
{

    std::string stripped_path = strip_extension(file_path);
    stripped_path += ".ply";
    return fribr::load_point_cloud(stripped_path.c_str());
}

CalibratedImage::Vector Middlebury::load_calibrated_images(const std::string &file_path)
{
    std::ifstream fcf_file(file_path, std::ios::in);

    if (!fcf_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
	    return CalibratedImage::Vector();
    }

    std::string folder_path = strip_filename(file_path);

    std::string line;

    getline(fcf_file, line);
    size_t nb_cameras = size_t(std::stof(line));

    CalibratedImage::Vector calib_cameras;
    calib_cameras.reserve(nb_cameras);

    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);

    for (size_t i = 0; i < nb_cameras; ++i)
    {
        std::getline(fcf_file, line);
        std::vector<std::string> tokens = split_string(line);

        std::string     image_path = append_path(folder_path, tokens[0]);
        Eigen::Vector2i resolution = image_resolution(image_path);
        if (resolution.x() < 0 || resolution.y() < 0)
        {
            std::cerr << "Could not get resolution for calibrated image: " << image_path << std::endl;
            return CalibratedImage::Vector();
        }

        static const size_t NB_CAM_PARAMS = 21;
        float values[NB_CAM_PARAMS];
        for (unsigned int j = 0; j < NB_CAM_PARAMS; ++j)
            values[j] = std::stof(tokens[j + 1]);

        float dx      = values[2];
        float dy      = values[5];
        float focal_x = values[0];
        float focal_y = values[4];

        Eigen::Matrix4f world_to_cam = Eigen::Matrix4f::Zero();
        for (int ii = 0; ii < 3; ++ii)
        for (int jj = 0; jj < 3; ++jj)
            world_to_cam(ii, jj) = values[9 + 3 * ii + jj];

        for (int ii = 0; ii < 3; ++ii)
            world_to_cam(ii, 3) = values[18 + ii];

        world_to_cam(3,3) = 1.0f;

        std::cout << world_to_cam << std::endl;

        Eigen::Matrix4f cam_to_world = world_to_cam.inverse();

        Eigen::Vector3f position = converter.transpose() * cam_to_world.block<3,1>(0,3);

        Eigen::Matrix3f orientation;
        orientation = converter.transpose() * cam_to_world.block<3,3>(0,0) * converter;

        Eigen::Vector3f k(0.0f, 0.0f, 0.0f);

        calib_cameras.push_back(CalibratedImage(focal_x, focal_y, dx, dy, k,
                                                orientation, position, resolution,
                                                Texture::Ptr(), image_path));
    }

    return calib_cameras;
}

}
