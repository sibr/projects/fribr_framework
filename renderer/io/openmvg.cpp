#include "io/openmvg.h"
#include "tools/string_tools.h"
#include "tools/image_tools.h"
#include "3d/point_cloud.h"
#include "gl_wrappers/texture.h"

#include <Eigen/Core>
#include <Eigen/StdVector>

#include <fstream>
#include <sstream>

// OpenMVG point clouds also include the camera locations as bright green points.
// This hack removes any points with the color (0, 255, 0) from the cloud.
#define IGNORE_OPENMVG_CAMERA_POINTS_HACK 1

namespace fribr
{

PointCloud::Ptr OpenMVG::load_point_cloud(const std::string &file_path)
{
    std::string stripped_path = strip_filename(file_path);
    stripped_path += "/colorized.ply";
    PointCloud::Ptr cloud = fribr::load_point_cloud(stripped_path);

    #if IGNORE_OPENMVG_CAMERA_POINTS_HACK
    std::vector<float3> vertices;
    std::vector<float3> normals;
    std::vector<float3> colors;

    vertices.reserve(cloud->get_vertices().size());
    normals.reserve(cloud->get_vertices().size());
    colors.reserve(cloud->get_vertices().size());

    for (size_t i = 0; i < cloud->get_vertices().size(); ++i)
    {
        // Avoid copying any points that have a pure green colors.
        // These are likely to be the camera locations from OpenMVG.
        if (cloud->has_colors())
        {
            float3 color = cloud->get_colors()[i];
            if (color.x < 0.0001f && color.y > 0.9999f && color.z < 0.0001f)
               continue;

            colors.push_back(color);
        }
        vertices.push_back(cloud->get_vertices()[i]);
        if (cloud->has_normals())
            normals.push_back(cloud->get_normals()[i]);
    }
    return PointCloud::Ptr(new PointCloud(vertices, normals, colors));
    #else
    return cloud;
    #endif
}

std::vector<OpenMVG::View> OpenMVG::load_views(const std::string &file_path)
{
    std::vector<View> view_vector;
    std::ifstream json_file(file_path, std::ios::in);

    if (!json_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
        return view_vector;
    }

    picojson::value v;
    json_file >> v;

    picojson::array& views       = v.get("views"     ).get<picojson::array>();
    picojson::array& extrinsics  = v.get("extrinsics").get<picojson::array>();

    size_t nb_views = views.size();
    view_vector.reserve(nb_views);

    for (size_t i = 0; i < nb_views; ++i)
    {
        picojson::value& view  = views[i].get("value").get("ptr_wrapper").get("data");
        view_vector.push_back(View(view.get("filename").get<std::string>(), -1));
    }

    size_t nb_extrinsics = extrinsics.size();
    for (size_t i = 0; i < nb_extrinsics; ++i)
    {
        int view_id = int(extrinsics[i].get("key").get<double>());
        view_vector[view_id].extrinsic_id = int(i);
    }

    return view_vector;
}

size_t OpenMVG::num_extrinsics(const std::string &file_path)
{
    std::ifstream json_file(file_path, std::ios::in);

    if (!json_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
        return 0;
    }

    picojson::value v;
    json_file >> v;

    picojson::array& extrinsics = v.get("extrinsics").get<picojson::array>();

    return extrinsics.size();
}

CalibratedImage::Vector OpenMVG::load_calibrated_images(const std::string &file_path)
{
    std::ifstream json_file(file_path, std::ios::in);

    if (!json_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
        return CalibratedImage::Vector();
    }

    picojson::value v;
    json_file >> v;

    picojson::array& extrinsics  = v.get("extrinsics").get<picojson::array>();
    picojson::array& views       = v.get("views"     ).get<picojson::array>();
    picojson::array& intrinsincs = v.get("intrinsics").get<picojson::array>();

    std::string images_path = strip_filename(file_path) + "/../images/";

    size_t nb_cameras = extrinsics.size();
    CalibratedImage::Vector calib_cameras;
    calib_cameras.reserve(nb_cameras);

    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);

    for (size_t i = 0; i < nb_cameras; ++i)
    {
        int view_id           = int(extrinsics[i].get("key").get<double>());
        picojson::value& view = views[view_id].get("value").get("ptr_wrapper").get("data");
        int intrinsics_id     = int(view.get("id_intrinsic").get<double>());

        picojson::value& intrinsic = intrinsincs[intrinsics_id].get("value").get("ptr_wrapper").get("data");
        float focal = float(intrinsic.get("focal_length").get<double>());
        float dx    = float(intrinsic.get("principal_point").get<picojson::array>()[0].get<double>());
        float dy    = float(intrinsic.get("principal_point").get<picojson::array>()[1].get<double>());

        Eigen::Vector3f k(0.0f, 0.0f, 0.0f);

        if (!intrinsic.get("disto_k3").is<picojson::null>())
            k = Eigen::Vector3f(float(intrinsic.get("disto_k3").get<picojson::array>()[0].get<double>()),
                                float(intrinsic.get("disto_k3").get<picojson::array>()[1].get<double>()),
                                float(intrinsic.get("disto_k3").get<picojson::array>()[2].get<double>()));

        std::string image_path  = images_path + view.get("filename").get<std::string>();
        Eigen::Vector2i resolution(int(view.get("width").get<double>()), int(view.get("height").get<double>()));

        picojson::array& center   = extrinsics[i].get("value").get("center"  ).get<picojson::array>();
        picojson::array& rotation = extrinsics[i].get("value").get("rotation").get<picojson::array>();

        Eigen::Vector3f position(float(center[0].get<double>()), float(center[1].get<double>()), float(center[2].get<double>()));
        Eigen::Matrix3f orientation;

        for (int ii = 0; ii < 3; ++ii)
        for (int jj = 0; jj < 3; ++jj)
            orientation(ii, jj) = float(rotation[jj].get<picojson::array>()[ii].get<double>());
        orientation = converter.transpose() * orientation * converter;

        position = converter.transpose() * position;

        calib_cameras.push_back(CalibratedImage(focal, focal, dx, dy, k,
                                                orientation, position, resolution,
                                                Texture::Ptr(), image_path));

    }

    return calib_cameras;
}

Observations OpenMVG::load_observations(const std::string &file_path)
{
    Observations observations;
    std::ifstream json_file(file_path, std::ios::in);

    if (!json_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
        return observations;
    }

    picojson::value v;
    json_file >> v;

    picojson::array& json_views      = v.get("views"     ).get<picojson::array>();
    picojson::array& json_extrinsics = v.get("extrinsics").get<picojson::array>();

    size_t nb_cameras = json_extrinsics.size();
    observations.observations.resize(nb_cameras);

    // The JSON files contains views for all cameras, not only the reconstructed
    // ones. We use an index remap to only consider SFM points which are visible
    // in the reconstructed images.
    size_t nb_views = json_views.size();
    std::vector<int> view_to_extrinsics(nb_views, -1);
    for (size_t i = 0; i < nb_cameras; ++i)
    {
        int view_id = int(json_extrinsics[i].get("key").get<double>());
        view_to_extrinsics[view_id] = int(i);
    }

    picojson::array& json_structures = v.get("structure").get<picojson::array>();
    size_t nb_points = json_structures.size();
    observations.points.reserve(nb_points);

    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);
    for (size_t i = 0; i < nb_points; ++i)
    {        
        picojson::value& json_structure = json_structures[i].get("value");

        float x = float(json_structure.get("X").get<picojson::array>()[0].get<double>());
        float y = float(json_structure.get("X").get<picojson::array>()[1].get<double>());
        float z = float(json_structure.get("X").get<picojson::array>()[2].get<double>());
        observations.points.push_back(converter.transpose() * Eigen::Vector3f(x, y, z));

        picojson::array& json_observations = json_structure.get("observations").get<picojson::array>();
        size_t nb_observations = json_observations.size();
        for (size_t j = 0; j < nb_observations; ++j)
        {
            size_t view_id   = size_t(json_observations[j].get("key").get<double>());
            int    camera_id = view_to_extrinsics[view_id];
            if (camera_id >= 0)
                observations.observations[camera_id].push_back(int(i));
        }
    }
    
    return observations;
}

}
