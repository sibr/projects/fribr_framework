#ifndef NVM_H
#define NVM_H

#include "3d/point_cloud.h"
#include "vision/calibrated_image.h"

#include <vector>
#include <string>

namespace fribr
{

namespace NVM
{

PointCloud::Ptr              load_point_cloud(const std::string &file_path);  
CalibratedImage::Vector load_calibrated_images(const std::string &file_path);

void                         save(const std::string &file_path,
                                  const CalibratedImage::Vector &images,
                                  const PointCloud::Ptr point_cloud);

}

}

#endif
