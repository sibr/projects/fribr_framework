#ifndef BUNDLER_H
#define BUNDLER_H

#include "projects/fribr_framework/renderer/3d/point_cloud.h"
#include "projects/fribr_framework/renderer/vision/calibrated_image.h"
#include "projects/fribr_framework/renderer/io/observations.h"

#include <Eigen/Core>
#include <vector>
#include <string>

namespace fribr
{

namespace Bundler
{

#ifdef INRIA_WIN
	PointCloud::Ptr         load_point_cloud(const std::string &file_path);
	PointCloud::Ptr			load_sparse_point_cloud(const std::string & file_path);
#endif

CalibratedImage::Vector   load_calibrated_images(const std::string &file_path, const bool ignoreImages = false);
void                      save(const std::string &file_path, const CalibratedImage::Vector &images);

}

}

#endif
