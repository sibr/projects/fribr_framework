#include "io/soft3d.h"
#include "3d/types.h"
#include "3dmath/math_tools.h"
#include "tools/string_tools.h"
#include "tools/image_tools.h"
#include "tools/file_tools.h"
#include "gl_wrappers/texture.h"

#include <Eigen/Core>

#include <fstream>
#include <sstream>
#include <unordered_map>

namespace fribr
{

CalibratedImage::Vector Soft3D::load_calibrated_images(const std::string &file_path)
{
    const std::string base_path = strip_filename(file_path);

    CalibratedImage::Vector calib_cameras;
    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);   
    for (int i = 0; ; ++i)
    {
	std::stringstream path_stream;
	path_stream << "cam" << std::setw(2) << std::setfill('0') << i << ".txt";
	std::string calibration_path = base_path + "/" + path_stream.str();

	path_stream.str("");
	path_stream << "color_calibrated_" << std::setw(6) << std::setfill('0') << i << ".jpg";
	std::string image_path = base_path + "/" + path_stream.str();

	if (!file_exists(calibration_path) && !file_exists(image_path))
	    break;

	if (!file_exists(calibration_path))
	{
	    std::cerr << "Could not find calibration file at: " << calibration_path << std::endl;
	    return CalibratedImage::Vector();
	}

	if (!file_exists(image_path))
	{
	    std::cerr << "Could not find image at: " << image_path << std::endl;
	    return CalibratedImage::Vector();
	}

	std::string line;
	std::ifstream calibration_stream(calibration_path);
	if (!calibration_stream.good())
	{
	    std::cerr << "Could not load calibration file at: " << calibration_path << std::endl;
	    return CalibratedImage::Vector();
	}

	// Position
	std::getline(calibration_stream, line);
	std::vector<std::string> tokens = split_string(line);
        Eigen::Vector3f position(std::stof(tokens[0]), std::stof(tokens[1]), std::stof(tokens[2]));

	// Orientation
	Eigen::Matrix3f orientation;
	for (int j = 0; j < 3; ++j)
	{
	    std::getline(calibration_stream, line);
	    tokens = split_string(line);
	    orientation.row(j) = Eigen::Vector3f(std::stof(tokens[0]), std::stof(tokens[1]), std::stof(tokens[2]));
	}

	// Convert to OpenGL coordinates.
	orientation = converter.transpose() * orientation * converter;
	position = (orientation * converter.transpose() * position);

	// Intrinsics
	std::getline(calibration_stream, line);
	tokens = split_string(line);
	float focal = std::stof(tokens[0]);
	float dx    = std::stof(tokens[1]);
	float dy    = std::stof(tokens[2]);

	// Get the image dimensions
	Eigen::Vector2i resolution = image_resolution(image_path);
        if (resolution.x() < 0 || resolution.y() < 0)
        {
            std::cerr << "Could not get resolution for calibrated image: " << image_path << std::endl;
            return CalibratedImage::Vector();
        }
	
	calib_cameras.push_back(CalibratedImage(focal, focal, dx, dy,
						Eigen::Vector3f(0.0f, 0.0f, 0.0f),
                                                orientation, position, resolution,
                                                Texture::Ptr(), image_path));
    }

    return calib_cameras;
}

}
