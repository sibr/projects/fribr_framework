#include "io/colmap.h"
#include "3d/types.h"
#include "tools/string_tools.h"
#include "tools/image_tools.h"
#include "tools/file_tools.h"
#include "gl_wrappers/texture.h"

#include <Eigen/Core>

#include <fstream>
#include <sstream>
#include <unordered_map>

namespace fribr
{

PointCloud::Ptr COLMAP::load_point_cloud(const std::string &file_path)
{
    const std::string base_path   = strip_filename(file_path);
    const std::string points_path = base_path + "/stereo/sparse/points3D.txt";
    
    std::ifstream points_file(points_path, std::ios::in);
    if (!points_file)
    {
        std::cerr << "Loading COLMAP points file: "
		  << points_path << " failed." << std::endl;
        return PointCloud::Ptr();
    }

    std::string line;
    while (std::getline(points_file, line) && line[0] == '#'); // Skip comments

    std::vector<float3> sfm_vertices, sfm_colors;
    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_UP);
    
    while (std::getline(points_file, line))
    {
	std::vector<std::string> tokens = split_string(line);
	if (tokens.size() < 7)
	{
	    std::cerr << "Unexpected line: " << line << std::endl;
	    return PointCloud::Ptr();
	}
	
	Eigen::Vector3f point = converter * Eigen::Vector3f(std::stof(tokens[1]),
                                                            std::stof(tokens[2]), 
                                                            std::stof(tokens[3]));
        float3 point3f = make_float3(point);
        float3 color3f = make_float3(std::stof(tokens[4]) / 255.0f,
                                     std::stof(tokens[5]) / 255.0f,
                                     std::stof(tokens[6]) / 255.0f);

	sfm_vertices.push_back(point3f);
	sfm_colors.push_back(color3f);
    }

    return PointCloud::Ptr(
	new PointCloud(sfm_vertices, std::vector<float3>(), sfm_colors)
    );
}

CalibratedImage::Vector COLMAP::load_calibrated_images(const std::string &file_path)
{
    const std::string base_path = strip_filename(file_path);
    const std::string cameras_path = base_path + "/stereo/sparse/cameras.txt";
    const std::string images_path = base_path + "/stereo/sparse/images.txt";
    
    std::ifstream cameras_file(cameras_path, std::ios::in);
    std::ifstream images_file(images_path, std::ios::in);

    if (!cameras_file)
    {
        std::cerr << "Loading COLMAP cameras file: "
		  << cameras_path << " failed." << std::endl;
        return CalibratedImage::Vector();
    }

    if (!images_file)
    {
        std::cerr << "Loading COLMAP images file: "
		  << images_path << " failed." << std::endl;
        return CalibratedImage::Vector();
    }

    // First load the camera models with precalibrated instrics
    struct PinholeIntrinsics
    {
	size_t width, height;
	float fx, fy, dx, dy;

	PinholeIntrinsics(size_t _width, size_t _height,
			  float _fx, float _fy,
			  float _dx, float _dy)
	    : width(_width), height(_height),
	      fx(_fx), fy(_fy), dx(_dx), dy(_dy)
	{
	}

	// Defaul constructor for std::unordered_map.
	PinholeIntrinsics()
	{
	}
    };
    std::unordered_map<size_t, PinholeIntrinsics> cameras;
    
    std::string line;
    while (std::getline(cameras_file, line) && line[0] == '#'); // Skip comments

    do
    {
	std::vector<std::string> tokens = split_string(line);
	if (tokens.size() < 8)
	{
	    std::cerr << "Unexpected line: " << line << std::endl;
	    return CalibratedImage::Vector();
	}

	if (tokens[1] != "PINHOLE" && tokens[1] != "OPENCV")
	{
	    std::cerr << "Unsupported camera type: " << tokens[1] << std::endl;
	    return CalibratedImage::Vector();
	}

	size_t id     = std::stol(tokens[0]);
	size_t width  = std::stol(tokens[2]);
	size_t height = std::stol(tokens[3]);
	float  fx     = std::stof(tokens[4]);
	float  fy     = std::stof(tokens[5]);
	float  dx     = std::stof(tokens[6]);
	float  dy     = std::stof(tokens[7]);

	cameras[id] = PinholeIntrinsics(width, height, fx, fy, dx, dy);
    } while (std::getline(cameras_file, line));

    // Now load the individual images and their extrinsic parameters
    CalibratedImage::Vector calib_cameras;
    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);
    
    while (std::getline(images_file, line) && line[0] == '#'); // Skip comments
    
    do
    {
	std::vector<std::string> tokens = split_string(line);
	if (tokens.size() < 10)
	{
	    std::cerr << "Unexpected line: " << line << std::endl;
	    return CalibratedImage::Vector();
	}

	size_t      id         = std::stol(tokens[8]);
	float       qw         = std::stof(tokens[1]);
	float       qx         = std::stof(tokens[2]);
	float       qy         = std::stof(tokens[3]);
	float       qz         = std::stof(tokens[4]);
	float       tx         = std::stof(tokens[5]);
	float       ty         = std::stof(tokens[6]);
	float       tz         = std::stof(tokens[7]);
	std::string image_name = tokens[9];

	if (cameras.find(id) == cameras.end())
	{
	    std::cerr << "Could not find intrinsics for image: "
		      << tokens[9] << std::endl;
	    return CalibratedImage::Vector();
	}
	const PinholeIntrinsics& pi = cameras[id];

	// Skip the observations.
	std::getline(images_file, line);

	Eigen::Quaternionf quat(qw, qx, qy, qz);
        Eigen::Matrix3f orientation;
        orientation = /*converter.transpose() **/ quat.toRotationMatrix().transpose() * converter;
	
        Eigen::Vector3f position(tx, ty, tz);	
        position = -(orientation * converter * position);

	const std::string image_path = base_path + "/stereo/images/" + image_name;
	
        calib_cameras.push_back(CalibratedImage(pi.fx, pi.fy, pi.dx, pi.dy,
						Eigen::Vector3f(0.0f, 0.0f, 0.0f),
                                                orientation, position,
						Eigen::Vector2i(pi.width, pi.height),
                                                Texture::Ptr(), image_path));
    } while (std::getline(images_file, line));

    return calib_cameras;
}

}
