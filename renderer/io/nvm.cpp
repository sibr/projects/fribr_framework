#include "io/nvm.h"
#include "3d/types.h"
#include "tools/string_tools.h"
#include "tools/image_tools.h"
#include "tools/file_tools.h"
#include "gl_wrappers/texture.h"

#include <Eigen/Core>

#include <fstream>
#include <sstream>

// OpenMVG point clouds also include the camera locations as bright green points.
// This hack removes any points with the color (0, 255, 0) from the cloud.
#define IGNORE_OPENMVG_CAMERA_POINTS_HACK 1

namespace fribr
{

PointCloud::Ptr NVM::load_point_cloud(const std::string &file_path)
{
    std::ifstream nvm_file(file_path, std::ios::in);

    if (!nvm_file)
    {
        std::cerr << "Loading file " << file_path << " failed." << std::endl;
        return PointCloud::Ptr();
    }

    std::string line;
    std::getline(nvm_file, line);
    if (line.substr(0,6) != "NVM_V3")
    {
        std::cerr << "Wrong file format: " << file_path << std::endl;
        return PointCloud::Ptr();
    }

    getline(nvm_file, line); // Skip one line.
    getline(nvm_file, line);
    size_t nb_cameras = size_t(std::stof(line));

    // Skip loading the cameras.
    for (size_t i = 0; i < nb_cameras; ++i)
        std::getline(nvm_file, line);

    getline(nvm_file, line); // Skip one line.
    getline(nvm_file, line);
    size_t nb_sfm_points = size_t(std::stof(line));

    std::vector<float3> sfm_vertices, sfm_colors;
    sfm_vertices.reserve(nb_sfm_points);
    sfm_colors.reserve(nb_sfm_points);

    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);
    for (size_t i = 0; i < nb_sfm_points; ++i)
    {
        std::getline(nvm_file, line);
        std::vector<std::string> tokens = split_string(line);

        Eigen::Vector3f point = converter * Eigen::Vector3f(std::stof(tokens[0]),
                                                            std::stof(tokens[1]), 
                                                            std::stof(tokens[2]));
        float3 point3f = make_float3(point);
        float3 color3f = make_float3(std::stof(tokens[3 + 0]) / 255.0f,
                                     std::stof(tokens[3 + 1]) / 255.0f,
                                     std::stof(tokens[3 + 2]) / 255.0f);
        #if IGNORE_OPENMVG_CAMERA_POINTS_HACK
        if (color3f.x >= 0.0001f || color3f.y <= 0.9999f || color3f.z >= 0.0001f)
        #endif
        {
            sfm_vertices.push_back(point3f);
            sfm_colors.push_back(color3f);
        }
    }

    return PointCloud::Ptr(new PointCloud(sfm_vertices, std::vector<float3>(), sfm_colors));
}

// NOTE: NVM files store the extrinsic rotation and the camera position
CalibratedImage::Vector NVM::load_calibrated_images(const std::string &file_path)
{
    std::ifstream nvm_file(file_path, std::ios::in);

    if (!nvm_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
	    return CalibratedImage::Vector();
    }

    std::string folder_path = strip_filename(file_path);

    std::string line;
    std::getline(nvm_file, line);

    if (line.substr(0,6) != "NVM_V3")
    {
        std::cerr << "wrong file format: " << file_path << std::endl;
	    return CalibratedImage::Vector();
    }

    getline(nvm_file, line); // Skip one line.
    getline(nvm_file, line);
    size_t nb_cameras = size_t(std::stof(line));

    CalibratedImage::Vector calib_cameras;
    calib_cameras.reserve(nb_cameras);

    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);
    for (size_t i = 0; i < nb_cameras; ++i)
    {
        std::getline(nvm_file, line);
        std::vector<std::string> tokens = split_string(line);

        std::string     image_path = append_path(folder_path, tokens[0]);
        Eigen::Vector2i resolution = image_resolution(image_path);
        if (resolution.x() < 0 || resolution.y() < 0)
        {
            std::cerr << "Could not get resolution for calibrated image: " << image_path << std::endl;
            return CalibratedImage::Vector();
        }

        static const size_t NB_CAM_PARAMS = 9;
        float values[NB_CAM_PARAMS];
        for (unsigned int j = 0; j < NB_CAM_PARAMS; ++j)
            values[j] = std::stof(tokens[j + 1]);

        float dx    = resolution.x() * 0.5f;
        float dy    = resolution.y() * 0.5f;
        float focal = values[0];

        Eigen::Quaternionf quat;
        quat = Eigen::Quaternionf(values[1], values[2], values[3], values[4]);

        Eigen::Vector3f position(values[5], values[6], values[7]);
        position = converter.transpose() * position;

        Eigen::Matrix3f orientation;
        orientation = converter.transpose() * quat.toRotationMatrix().transpose() * converter;

        calib_cameras.push_back(CalibratedImage(focal, focal, dx, dy, Eigen::Vector3f(values[8] / (focal * focal), 0.0f, 0.0f),
                                                orientation, position, resolution,
                                                Texture::Ptr(), image_path));
    }

    return calib_cameras;
}

void NVM::save(const std::string &file_path,
               const CalibratedImage::Vector &images,
               const PointCloud::Ptr point_cloud)
{
    std::ofstream nvm_file(file_path, std::ios_base::trunc);
    nvm_file << "NVM_V3" << std::endl;

    const std::string base_path   = fribr::strip_filename(file_path);
    const std::string images_path = fribr::append_path(base_path, "images");
    if (!fribr::directory_exists(images_path))
        fribr::make_directory(images_path);

    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN).inverse();

    nvm_file << std::endl;
    nvm_file << images.size() << std::endl;
    for (const CalibratedImage& camera : images)
    {
        cv::Mat image = camera.get_image();
        if (image.empty())
            image = fribr::load_undistorted_image(camera.get_image_path(),
                                                  Eigen::Vector2f(camera.get_focal_x(),
                                                                  camera.get_focal_y()),
                                                  Eigen::Vector2f(camera.get_dx(),
                                                                  camera.get_dy()),
                                                  camera.get_k(),
                                                 int(camera.get_max_width()),
                                                  int(cv::IMREAD_COLOR));
        const std::string image_path = fribr::append_path(images_path, camera.get_image_name()) + ".jpg";
        cv::imwrite(image_path, image);

        const std::string relative_path = fribr::append_path("images", camera.get_image_name()) + ".jpg";
        nvm_file << relative_path << " "
		 << camera.get_scale_factor() * camera.get_focal_x() << " ";

        Eigen::Matrix3f r = /*converter.transpose() **/ converter * camera.get_orientation().transpose() /** converter*/;
        Eigen::Quaternion<float> q(r);
        nvm_file << q.w() << " " << q.x() << " " << q.y() << " " << q.z() << " ";

        Eigen::Vector3f p = /*converter.transpose() **/ camera.get_position();
        nvm_file << p.x() << " " << p.y() << " " << p.z() << " ";
        
        nvm_file << "0 0" << std::endl;
    }

    if (!point_cloud)
    {
        nvm_file << std::endl;
        nvm_file << "0" << std::endl;
        nvm_file << std::endl;
        nvm_file << "0" << std::endl;
        return;
    }

    const std::vector<float3> &vertices = point_cloud->get_vertices();
    const std::vector<float3> &colors   = point_cloud->get_colors();
    nvm_file << std::endl;
    nvm_file << vertices.size() << std::endl;
    for (size_t i = 0; i < vertices.size(); ++i)
    {
        float3 v = make_float3(converter * make_vec3f(vertices[i]));
        float3 c = make_float3(255.0f *  make_vec3f(colors[i]));
        nvm_file << v.x << " " << v.y << " " << v.z << " ";
        nvm_file << c.x << " " << c.y << " " << c.z << " ";
        nvm_file << "0 " << std::endl;
    }

    nvm_file << std::endl;
    nvm_file << "0" << std::endl;
}

}

