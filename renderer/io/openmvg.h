#ifndef OPENMVG_H
#define OPENMVG_H

#include "projects/fribr_framework/renderer/3d/point_cloud.h"
#include "projects/fribr_framework/renderer/vision/calibrated_image.h"
#include "projects/fribr_framework/renderer/io/observations.h"

#include "picojson/picojson.hpp"


#include <Eigen/Core>
#include <vector>
#include <string>

namespace fribr
{

namespace OpenMVG
{

struct View
{
    std::string image_name;
    int         extrinsic_id;
    View(const std::string &_image_name, int _extrinsic_id)
        : image_name(_image_name), extrinsic_id(_extrinsic_id)
    {
    }
};
std::vector<View>         load_views(const std::string &file_path);
size_t                    num_extrinsics(const std::string &file_path);

PointCloud::Ptr           load_point_cloud(const std::string &file_path);  
CalibratedImage::Vector   load_calibrated_images(const std::string &file_path);
Observations              load_observations(const std::string &file_path);

}

}

#endif
