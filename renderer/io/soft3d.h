#ifndef SOFT3D_H
#define SOFT3D_H

#include "vision/calibrated_image.h"

#include <vector>
#include <string>

namespace fribr
{

namespace Soft3D
{

CalibratedImage::Vector load_calibrated_images(const std::string &file_path);

}

}

#endif
