#include "io/blacklist.h"

#include <iostream>
#include <fstream>

namespace fribr
{

std::vector<std::string> Blacklist::load_blacklist(const std::string &file_path)
{
    std::ifstream blacklist_file(file_path, std::ios::in);
    if (!blacklist_file.good())
    {
        std::cerr << "Error loading file: " << file_path << std::endl;
        return std::vector<std::string>();
    }

    std::string line;
    std::vector<std::string> blacklist;
    while (std::getline(blacklist_file, line).good())
        blacklist.push_back(line);

    std::cout << "Blacklist file: " << file_path << " loaded. Ignoring " << blacklist.size() << " cameras!" << std::endl;
    return blacklist;
}

void Blacklist::save_blacklist(const std::string &file_path, const std::vector<std::string> &blacklist)
{
    std::ofstream blacklist_file(file_path, std::ios::trunc);
    if (!blacklist_file.good())
    {
        std::cerr << "Error loading file: " << file_path << std::endl;
        return;
    }

    for (std::string entry : blacklist)
        blacklist_file << entry << std::endl;
}

}
