#ifndef COLMAP_H
#define COLMAP_H

#include "3d/point_cloud.h"
#include "vision/calibrated_image.h"

#include <vector>
#include <string>

namespace fribr
{

namespace COLMAP
{

PointCloud::Ptr         load_point_cloud(const std::string &file_path);  
CalibratedImage::Vector load_calibrated_images(const std::string &file_path);

}

}

#endif
