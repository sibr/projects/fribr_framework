#ifndef BLACKLIST_H
#define BLACKLIST_H

#include <vector>
#include <string>

namespace fribr
{

namespace Blacklist
{

std::vector<std::string> load_blacklist(const std::string &file_path);
void                     save_blacklist(const std::string &file_path,
                                        const std::vector<std::string> &blacklist);

}

}

#endif
