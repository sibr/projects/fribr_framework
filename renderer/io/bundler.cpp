#include <Eigen/Core>
#include <Eigen/StdVector>
#include "io/bundler.h"

#include "io/openmvg.h"
#include "tools/string_tools.h"
#include "tools/image_tools.h"
#include "tools/file_tools.h"
#include "3d/point_cloud.h"
#include "gl_wrappers/texture.h"


#include <fstream>
#include <sstream>
#include <iomanip>

namespace fribr
{

#ifdef INRIA_WIN

	PointCloud::Ptr Bundler::load_point_cloud(const std::string &file_path)
	{
		const std::string base_path = strip_filename(file_path);
		const std::string points_path = base_path + "/pmvs/models/pmvs_recon.ply";
		return fribr::load_point_cloud(points_path, fribr::RH_Y_DOWN, false);
	}

	PointCloud::Ptr Bundler::load_sparse_point_cloud(const std::string & file_path) {
		// TODO: unimplemented because RC/our scripts do not preserve sparse SfM points in the bundle.out.
		/*std::string base_path = fribr::strip_filename(file_path);
		std::string visualize_path = fribr::append_path(base_path, "visualize");

		std::ifstream bundler_file(file_path, std::ios::in);
		if (!bundler_file)
		{
		std::cerr << "file loading failed: " << file_path << std::endl;
		return PointCloud::Ptr();
		}

		std::string header;
		std::getline(bundler_file, header);
		if (header.find("# Bundle file v0.3") == std::string::npos)
		{
		std::cerr << "Unexpected file format: " << header << " (" << file_path << ")" << std::endl;
		return PointCloud::Ptr();
		}

		int num_cameras, num_points;
		bundler_file >> num_cameras >> num_points;
		Eigen::Matrix3f to_cv;
		to_cv << 1.0f, 0.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, 0.0f, -1.0f;
		Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);

		CalibratedImage::Vector images;
		for (int i = 0; i < num_cameras; ++i)
		{
		float f, k1, k2;
		bundler_file >> f >> k1 >> k2;

		float r00, r01, r02;
		float r10, r11, r12;
		float r20, r21, r22;
		bundler_file >> r00 >> r01 >> r02
		>> r10 >> r11 >> r12
		>> r20 >> r21 >> r22;
		float tx, ty, tz;
		bundler_file >> tx >> ty >> tz;
		}*/

		return PointCloud::Ptr();
	}

#endif

CalibratedImage::Vector Bundler::load_calibrated_images(const std::string &file_path, const bool ignoreImages)
{

	CalibratedImage::Vector images;
    std::string base_path      = fribr::strip_filename(file_path);
#ifdef INRIA_WIN
    std::string visualize_path = fribr::append_path(base_path, "");
#else
    std::string visualize_path = fribr::append_path(base_path, "visualize");
#endif

    std::ifstream bundler_file(file_path, std::ios::in);
    if (!bundler_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
        return CalibratedImage::Vector();
    }
    
    std::string header;
    std::getline(bundler_file, header);
    if (header.find("# Bundle file v0.3") == std::string::npos)
    {
        std::cerr << "Unexpected file format: " << header << " (" << file_path << ")" << std::endl;
        return CalibratedImage::Vector();
    }

    int num_cameras, num_points;
    bundler_file >> num_cameras >> num_points;
    Eigen::Matrix3f to_cv;
    to_cv << 1.0f, 0.0f, 0.0f,
             0.0f, -1.0f, 0.0f,
             0.0f, 0.0f, -1.0f;    
    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);

    for (int i = 0; i < num_cameras; ++i)
    {
        float f, k1, k2;
        bundler_file >> f >> k1 >> k2;
        
        float r00, r01, r02;
        float r10, r11, r12;
        float r20, r21, r22;
        bundler_file >> r00 >> r01 >> r02
                     >> r10 >> r11 >> r12
                     >> r20 >> r21 >> r22;

        Eigen::Matrix3f rotation;
        rotation(0, 0) = r00;
        rotation(0, 1) = r01;
        rotation(0, 2) = r02;
        rotation(1, 0) = r10;
        rotation(1, 1) = r11;
        rotation(1, 2) = r12;
        rotation(2, 0) = r20;
        rotation(2, 1) = r21;
        rotation(2, 2) = r22;

        Eigen::Matrix3f orientation = (to_cv * rotation).transpose();

        float tx, ty, tz;
        bundler_file >> tx >> ty >> tz;
        Eigen::Vector3f position = -orientation * (to_cv * Eigen::Vector3f(tx, ty, tz));

        std::stringstream pad_stream;
        pad_stream << std::setfill('0') << std::setw(8) << i << ".jpg";
        std::string     image_path = fribr::append_path(visualize_path, pad_stream.str());

		Eigen::Vector2i resolution(2, 2);
		if (!ignoreImages) {
			resolution = image_resolution(image_path);
		}

        if (resolution.x() < 0 || resolution.y() < 0)
        {
            std::cerr << "Could not get resolution for calibrated image: " << image_path << std::endl;
            return CalibratedImage::Vector();
        }

        float dx = resolution.x() * 0.5f;
        float dy = resolution.y() * 0.5f;

        orientation = /*converter.transpose() **/ orientation * converter;
        position = /*converter.transpose() **/ position;

        images.push_back(CalibratedImage(f, f, dx, dy, Eigen::Vector3f(0.0f, 0.0f, 0.0f),
                                         orientation, position, resolution,
                                         Texture::Ptr(), image_path));
    }

    return images;
}

void Bundler::save(const std::string &file_path, const CalibratedImage::Vector &images)
{
    std::string base_path      = fribr::strip_filename(file_path);
    std::string visualize_path = fribr::append_path(base_path, "visualize");
    std::string list_path      = fribr::append_path(base_path, "list.txt");

    std::ofstream bundler_file(file_path, std::ios::out | std::ios::trunc);
    if (!bundler_file)
    {
        std::cerr << "Cannot open file for writing: " << file_path << std::endl;
        return;
    }
    std::ofstream list_file (list_path, std::ios::out | std::ios::trunc);
    if (!list_file)
    {
        std::cerr << "Cannot open file for writing: " << list_path << std::endl;
        return;
    }

    if (!fribr::directory_exists(visualize_path))
        fribr::make_directory(visualize_path);
    
    std::string header;
    bundler_file << "# Bundle file v0.3" << std::endl; 
    bundler_file << images.size() << " " << 0 << std::endl;

    Eigen::Matrix3f from_cv;
    from_cv << 1.0f,  0.0f,  0.0f,
               0.0f, -1.0f,  0.0f,
               0.0f,  0.0f, -1.0f;    
    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);
    
    for (size_t i = 0; i < images.size(); ++i)
    {
        const fribr::CalibratedImage &camera = images[i];
        cv::Mat                       image  = camera.get_image();
        if (image.empty())
            image = fribr::load_undistorted_image(camera.get_image_path(),
                                                  Eigen::Vector2f(camera.get_focal_x(),
                                                                  camera.get_focal_y()),
                                                  Eigen::Vector2f(camera.get_dx(),
                                                                  camera.get_dy()),
                                                  camera.get_k(),
                                                  int(camera.get_max_width()),
                                                 int( cv::IMREAD_COLOR));
        std::stringstream pad_stream;
        pad_stream << std::setfill('0') << std::setw(8) << i << ".jpg";
        std::string image_path = fribr::append_path(visualize_path, pad_stream.str());
        cv::imwrite(image_path, image);

        list_file << "visualize/" << pad_stream.str() << " 0 "
		  << camera.get_scale_factor() * camera.get_focal_x() << std::endl;

        Eigen::Matrix3f orientation = camera.get_orientation();
        Eigen::Vector3f position    = camera.get_position();

        bundler_file << camera.get_scale_factor() * camera.get_focal_x()
		     << " " << 0 << " " << 0 << std::endl;

        Eigen::Matrix3f rotation_cv      = converter.transpose() * orientation.transpose() * converter;
        Eigen::Matrix3f rotation_bundler = from_cv * rotation_cv;

        Eigen::Vector3f position_cv         = converter.transpose() * position;
        Eigen::Vector3f translation_cv      = -(rotation_cv * position_cv);
        Eigen::Vector3f translation_bundler = from_cv * translation_cv;

        bundler_file << rotation_bundler(0, 0) << " " 
                     << rotation_bundler(0, 1) << " " 
                     << rotation_bundler(0, 2) << std::endl
                     << rotation_bundler(1, 0) << " " 
                     << rotation_bundler(1, 1) << " " 
                     << rotation_bundler(1, 2) << std::endl
                     << rotation_bundler(2, 0) << " " 
                     << rotation_bundler(2, 1) << " "
                     << rotation_bundler(2, 2) << std::endl;

        bundler_file << translation_bundler.x() << " "
                     << translation_bundler.y() << " "
                     << translation_bundler.z() << " "
                     << std::endl;
    }
}

}
