#include "io/pmvs.h"
#include "3d/types.h"

#include <fstream>
#include <iostream>
#include <Eigen/Core>

namespace fribr
{

PointCloud::Ptr PMVS::load_point_cloud(const std::string &file_path)
{
    std::ifstream pmvs_file(file_path, std::ios::in);

    if (!pmvs_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
        return PointCloud::Ptr();
    }

    std::string line;
    unsigned long int num_points = 0;

    getline(pmvs_file, line);
    pmvs_file >> num_points;

    std::vector<float3> vertices, normals, colors;
    vertices.reserve(num_points);
    normals.reserve(num_points);

    std::cout << "\rLoading PMVS points: 0%       " << std::flush;
    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);
    for (unsigned long int i = 0; i < num_points; i++) 
    {
        do {
            if (!pmvs_file.good())
            {
                pmvs_file.close();
                std::cout << std::endl
                          << "WARNING: EOF reached before parsing all points "
                          << "(" << i << "/" << num_points << ")" 
                          << std::endl;
                goto end_loop;
            }
            
            getline(pmvs_file, line);
        } while (line.find("PATCHS") == std::string::npos);
	
        Eigen::Vector4f p, n, t;
        pmvs_file >> p[0] >> p[1] >> p[2] >> p[3];
        pmvs_file >> n[0] >> n[1] >> n[2] >> n[3];
        pmvs_file >> t[0] >> t[1] >> t[2];

        vertices.push_back(make_float3(converter.transpose() * Eigen::Vector3f(p[0], p[1], p[2])));
        normals.push_back(make_float3(converter.transpose()  * Eigen::Vector3f(n[0], n[1], n[2])));

        if (i % 1000 == 999)
            std::cout << "\rLoading PMVS points "<< i * 100.0f / num_points << "%      " << std::flush;
    }
    std::cout << std::endl;
end_loop:

    return PointCloud::Ptr(new PointCloud(vertices, std::vector<float3>(), colors));
}

Observations PMVS::load_observations(const std::string &file_path, int num_cameras)
{
    static const float MVS_THRESHOLD = 0.8f;

    Observations observations;
    std::ifstream pmvs_file(file_path, std::ios::in);

    if (!pmvs_file)
    {
        std::cerr << "file loading failed: " << file_path << std::endl;
        return observations;
    }

    std::string line;
    unsigned long int num_points = 0;

    getline(pmvs_file, line);
    pmvs_file >> num_points;

    observations.observations.resize(num_cameras);
    observations.points.reserve(num_points);

    std::cout << "\rLoading PMVS points: 0%       " << std::flush;
    Eigen::Matrix3f converter = to_right_handed_y_up(RH_Y_DOWN);
    for (unsigned long int i = 0; i < num_points; i++) 
    {
        do {
            if (pmvs_file.eof())
            {
                pmvs_file.close();
                std::cout << std::endl
                          << "WARNING: EOF reached before parsing all points "
                          << "(" << i << "/" << num_points << ")" 
                          << std::endl;
                return observations;
            }
            
            getline(pmvs_file, line);
        } while (line.compare("PATCHS"));
	
        Eigen::Vector4f p, n, t;
        pmvs_file >> p[0] >> p[1] >> p[2] >> p[3];
        pmvs_file >> n[0] >> n[1] >> n[2] >> n[3];
        pmvs_file >> t[0] >> t[1] >> t[2];

        observations.points.push_back(converter.transpose() * Eigen::Vector3f(p[0], p[1], p[2]));

        int num_cam = 0;
        pmvs_file >> num_cam;

        if (num_cam < 2 || t[0] < MVS_THRESHOLD)
            continue;

        for (int j = 0; j < num_cam; j++)
        {
            int cam;
            pmvs_file >> cam;
            observations.observations[cam].push_back(i);
        }

        getline(pmvs_file, line); // one integer
        getline(pmvs_file, line); // some image ID's
        getline(pmvs_file, line); // blank line

        if (i % 1000 == 999)
            std::cout << "\rLoading PMVS points "<< i * 100.0f / num_points << "%      " << std::flush;
    }
    std::cout << std::endl;

    return observations;
}

}
