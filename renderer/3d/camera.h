#ifndef CAMERA_H
#define CAMERA_H

#include "imgui/imgui.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <iostream>


namespace fribr
{

class Camera
{
public:
    Camera(float near, float far, float fov, float aspect);

    Eigen::Vector3f get_position()         const throw()        { return m_position; }
    Eigen::Vector3f get_up()               const throw()        { return m_up;       }
    Eigen::Vector3f get_forward()          const throw()        { return m_forward;  }

    float           get_near()             const throw()        { return m_near;     }
    float           get_far()              const throw()        { return m_far;      }
    float           get_fov()              const throw()        { return m_fov;      }
    float           get_aspect()           const throw()        { return m_aspect;   }

    Eigen::Matrix4f get_camera_to_clip();
    Eigen::Affine3f get_world_to_camera();

    float get_speed()                      const throw()        { return m_speed; }
    float get_mouse_sensitivity()          const throw()        { return m_mouse_sensitivity; }
    void  set_speed             (float speed)                   { m_speed             = speed; }
    void  set_mouse_sensitivity (float mouse_sensitivity)       { m_mouse_sensitivity = mouse_sensitivity; }

    void set_position           (const Eigen::Vector3f pos)     { m_dirty |= m_position != pos;     m_position = pos; }
    void set_up                 (const Eigen::Vector3f up)      { m_dirty |= m_up       != up;      m_up       = up; }
    void set_forward            (const Eigen::Vector3f forward) { m_dirty |= m_forward  != forward; m_forward = forward;  }
    void set_aspect             (float aspect)                  { m_dirty |= m_aspect   != aspect;  m_aspect  = aspect; }
    void set_fov                (float fov)                     { m_dirty |= m_fov      != fov;     m_fov     = fov; }
    void set_near               (float near)                    { m_dirty |= m_near     != near;    m_near    = near; }
    void set_far                (float far)                     { m_dirty |= m_far      != far;     m_far     = far; }

    void update_transformation(const ImGuiIO &io);

private:
    bool            m_dirty;

    Eigen::Affine3f m_world_to_camera;
    Eigen::Matrix4f m_camera_to_clip;

    float           m_near;
    float           m_far;
    float           m_fov;
    float           m_aspect;

    float           m_speed;
    float           m_mouse_sensitivity;

    Eigen::Vector3f m_position;
    Eigen::Vector3f m_forward;
    Eigen::Vector3f m_up;
};

}

#endif // CAMERA_H
