#ifndef TSDF_GRID_H
#define TSDF_GRID_H

#include "vision/calibrated_image.h"
#include "io/observations.h"
#include "3d/point_cloud.h"

#include <Eigen/Core>
#include <vector>
#include <memory>

namespace fribr
{

class TSDFGrid
{
public:
    typedef std::shared_ptr<TSDFGrid> Ptr;

    TSDFGrid(Eigen::Vector3i resolution, float truncation_distance_mm);

    void clear();
    void integrate_depth_images(const std::string              &depth_folder, 
                                fribr::CalibratedImage::Vector &cameras_depth,
                                fribr::Observations            &observations_depth,
                                fribr::PointCloud::Ptr          sfm_cloud) throw();

    Eigen::Vector3i get_resolution             () const throw() { return m_resolution; }
    float           get_truncation_distance_mm () const throw() { return m_truncation_distance_mm; }
    float           get_mm_to_sfm              () const throw() { return m_mm_to_sfm; }
    float           get_sfm_to_mm              () const throw() { return m_sfm_to_mm; }


    Eigen::Vector3i compute_voxel_index   (Eigen::Vector3f p) const throw();
    Eigen::Vector3f compute_voxel_position(Eigen::Vector3f p) const throw();
    Eigen::Vector3f compute_world_position(Eigen::Vector3f p) const throw();

    float trilinear_fetch(Eigen::Vector3f p) const throw();
    float nearest_fetch  (Eigen::Vector3f p) const throw();
    float index_fetch    (Eigen::Vector3i i) const throw();

    Eigen::Vector3f ray_cast_first(Eigen::Vector3f orig, Eigen::Vector3f dir, float max_t);
    void            ray_cast_all  (Eigen::Vector3f orig, Eigen::Vector3f dir, float max_t,
                                   std::vector<Eigen::Vector3i> &indices);

private:
    size_t linearize_index(Eigen::Vector3i i) const throw()
    {
        return (i.y() * m_resolution.x() + i.x()) * m_resolution.z() + i.z();
    }
    
    float              m_truncation_distance_mm;
    Eigen::Vector3i    m_resolution;
    std::vector<float> m_tsdf_volume;
    Eigen::Vector3f    m_min;
    Eigen::Vector3f    m_max;
    float              m_mm_to_sfm;
    float              m_sfm_to_mm;
};

}

#endif
