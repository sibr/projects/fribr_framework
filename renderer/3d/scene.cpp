#include "projects/fribr_framework/renderer/3d/scene.h"
#include "projects/fribr_framework/renderer/tools/string_tools.h"

#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <assimp/mesh.h>

#include <iostream>
#include <sstream>

namespace
{

fribr::Shader* compile_scene_shader()
{
    fribr::Shader* shader = new fribr::Shader();
    shader->vertex_shader(
    GLSL(330,
	 layout(location = 0) in vec3 vertex_position;
	 layout(location = 1) in vec3 vertex_normal;
	 layout(location = 2) in vec2 vertex_texcoord;
	 layout(location = 3) in vec3 vertex_color;
	 
	 uniform mat4 camera_to_clip;
	 uniform mat4 world_to_camera;
	 
	 out vec3 position;
	 out vec3 normal;
	 out vec2 texcoord;
     out vec3 color;
	 
	 void main()
	 {
	     position    = (world_to_camera * vec4(vertex_position, 1.0)).xyz;
	     normal      = (world_to_camera * vec4(vertex_normal, 0.0)).xyz;
	     texcoord    = vertex_texcoord;
         color       = vertex_color;
	     gl_Position = camera_to_clip * vec4(position, 1.0);
	 }
    ));

    shader->fragment_shader(
    GLSL(330,
	 in  vec3 position;
	 in  vec3 normal;
	 in  vec2 texcoord;
     in  vec3 color;
	 out vec4 frag_color;

	 uniform Material
	 {
	     vec4  diffuse;
	     vec4  ambient;
	     vec4  specular;
	     vec4  emissive;
	     float shininess;
	     bool  use_texture;
	 };
   
	 uniform sampler2D diffuse_sampler;
     uniform bool      use_vertex_colors;

     uniform bool      use_colors;
     uniform bool      use_shading;

	 void main()
	 {
	     vec3  pixel_normal   = normalize(normal);
	     vec3  view_direction = normalize(-position);
	     float clamped_cosine = max(dot(pixel_normal, view_direction), 0.0);

 	     vec3 pixel_color;
         if (use_texture)
         {
             pixel_color = texture2D(diffuse_sampler, texcoord).rgb;
         }
         else if (use_vertex_colors)
         {
             pixel_color    = color;
         }
         else
         {
             pixel_color = diffuse.rgb;
         }

         if (!use_colors)
             pixel_color = vec3(0.8, 0.8, 0.8);
         if (!use_shading)
             clamped_cosine = 1.0;
         
	     frag_color = vec4(clamped_cosine * pixel_color, 1.0);
	 }
    ));
    shader->link();

    return shader;
}

}

namespace fribr
{

int     Scene::s_num_instances = 0;
Shader* Scene::s_shader        = 0;

Scene::Scene(const std::vector<MaterialMesh> &meshes,
             const std::vector<Material>     &materials,
             const std::vector<cv::Mat3b>    &images)
    : m_meshes(meshes),
      m_materials(materials),
      m_images(images)
{
    // Use a null pointer to indicate that the material has no texture.
    Texture::Descriptor texture_descriptor(GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR,
                                           Texture::GENERATE_MIPMAPS | Texture::ANISOTROPIC_FILTERING);
    for (size_t i = 0; i < m_images.size(); ++i)
    {
        m_textures.push_back(
            m_images[i].empty() ? Texture::Ptr()
                                : Texture::Ptr(new Texture(m_images[i], texture_descriptor))
        );
    }

    // Upload the materials as uniform buffers.
    for (size_t i = 0; i < m_materials.size(); ++i)
    {
        GLuint material_index;
        glGenBuffers(1, &material_index);
        glBindBuffer(GL_UNIFORM_BUFFER, material_index);
        glBufferData(GL_UNIFORM_BUFFER, sizeof(Material), (void*)(&m_materials[i]), GL_STATIC_DRAW);
        glBindVertexArray(0);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        m_material_indices.push_back(material_index);
    }

    // The first instance of Scene is responsible for compiling the shader.
    if (s_num_instances++ == 0)
        s_shader = compile_scene_shader();
}

Scene::Scene(const std::string &path, CoordinateSystem system, bool mergeVerts, bool msgs) noexcept(false) {
    Assimp::Importer importer;
    unsigned flagsOpts = aiProcess_GenNormals;
    if (mergeVerts)
        flagsOpts |= aiProcess_Triangulate | aiProcess_JoinIdenticalVertices;
	
    const aiScene* scene = importer.ReadFile(path.c_str(), flagsOpts);
    //
    if (!scene)
    {
        if( msgs )
            std::cerr << "Loading scene failed: " << path << " ASSIMP ERROR :" << importer.GetErrorString() << std::endl;
        std::stringstream error_stream;
        if( msgs )
            error_stream << "Loading scene failed: " << path << " ASSIMP ERROR :" << importer.GetErrorString();
         
        throw std::invalid_argument(error_stream.str());
    }
    std::string stripped_path = strip_filename(path);

    // Load the meshes, couple them with the index of their material.
    m_meshes.reserve(scene->mNumMeshes);
    for (size_t i = 0; i < scene->mNumMeshes; ++i)
    {
        MaterialMesh material_mesh;
        material_mesh.mesh           = Mesh::Ptr(new Mesh(scene, scene->mMeshes[i], system));
        material_mesh.material_index = scene->mMeshes[i]->mMaterialIndex;
        if (material_mesh.mesh->get_indices().size()  % 3 != 0 ||
            material_mesh.mesh->get_vertices().size() % 3 != 0)
        {
            std::cout << "Warning: Ignoring mesh with " << material_mesh.mesh->get_vertices().size() << " vertices and "
                      << material_mesh.mesh->get_indices().size() << " indices." << std::endl;
            continue;
        }

        m_meshes.push_back(material_mesh);
    }

    // Sort according to material to avoid state changes when drawing.
    std::sort(m_meshes.begin(), m_meshes.end());

    // Pre-allocate space for the textures and the materials.
    m_textures.reserve(scene->mNumMaterials);
    m_materials.reserve(scene->mNumMaterials);

    // Load the materials.
    for (size_t i = 0; i < scene->mNumMaterials; ++i)
    {
        Material material;
        aiMaterial *mtl = scene->mMaterials[i];

        // Load colors first, assume a Phong BRDF.
        Eigen::Vector4f c;
        aiColor4D aiColor;

        // Diffuse
        c = Eigen::Vector4f(0.8f, 0.8f, 0.8f, 1.0f);
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_DIFFUSE, &aiColor))
            c = Eigen::Vector4f(&aiColor[0]);
        memcpy(material.diffuse, c.data(), sizeof(float) * 4);

        // Ambient
        c = Eigen::Vector4f(0.2f, 0.2f, 0.2f, 1.0f);
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_AMBIENT, &aiColor))
             c = Eigen::Vector4f(&aiColor[0]);
        memcpy(material.ambient, c.data(), sizeof(float) * 4);

        // Specular
        c = Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_SPECULAR, &aiColor))
            c = Eigen::Vector4f(&aiColor[0]);
        memcpy(material.specular, c.data(), sizeof(float) * 4);

        // Emissive
        c = Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
        if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_EMISSIVE, &aiColor))
            c = Eigen::Vector4f(&aiColor[0]);
         memcpy(material.emissive, c.data(), sizeof(float) * 4);

        // Shininess
        float shininess = 0.0;
        unsigned int max;
        aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, &max);
        material.shininess = shininess;

        // Load the textures next. For now we only use the diffuse texture.
        aiString texture_path;
        aiReturn texture_found = scene->mMaterials[i]->GetTexture(aiTextureType_DIFFUSE, 0, &texture_path);

        std::string full_texture_path = append_path(stripped_path, texture_path.C_Str());
        material.use_texture          = texture_found == AI_SUCCESS ? 1 : 0;
        m_materials.push_back(material);

        // Use a null pointer to indicate that the material has no texture.
	    Texture::Descriptor texture_descriptor(GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR,
					                           Texture::GENERATE_MIPMAPS | Texture::ANISOTROPIC_FILTERING);
        if (texture_found == AI_SUCCESS)
        {
            cv::Mat3b image = cv::imread(full_texture_path);
            m_images.push_back(image);
            m_textures.push_back(Texture::Ptr(new Texture(image, texture_descriptor)));
        }
        else
        {
            m_images.push_back(cv::Mat3b());
            m_textures.push_back(Texture::Ptr());
        }

        // Upload everything as a uniform buffer.
        GLuint material_index;
        glGenBuffers(1, &material_index);
        glBindBuffer(GL_UNIFORM_BUFFER, material_index);
        glBufferData(GL_UNIFORM_BUFFER, sizeof(material), (void*)(&material), GL_STATIC_DRAW);
        glBindVertexArray(0);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        m_material_indices.push_back(material_index);
    }

    // The first instance of Scene is responsible for compiling the shader.
    if (s_num_instances++ == 0)
        s_shader = compile_scene_shader();
}

Scene::~Scene() noexcept {
    // The last instance of Scene is responsible for deleting the shader.
    if (--s_num_instances == 0)
    {
        delete s_shader;
        s_shader = 0;
    }
}

std::vector<Texture::Ptr> Scene::get_textures()
{
    std::vector<Texture::Ptr> textures;
    for (MaterialMesh material_mesh : m_meshes)
    {
	int mid = material_mesh.material_index;
        textures.push_back(m_textures[mid]);
    }
    return textures;
}

std::vector<Mesh::Ptr> Scene::get_meshes()
{
    std::vector<Mesh::Ptr> meshes;
    for (MaterialMesh material_mesh : m_meshes)
        meshes.push_back(material_mesh.mesh);
    return meshes;
}


void Scene::draw(sibr::IRenderTarget & dst, const Eigen::Affine3f world_to_camera,
	const Eigen::Matrix4f camera_to_clip,
	RenderMode            mode)
{
	glViewport(0, 0, dst.w(), dst.h());
	if (_clearDst) {
		dst.clear();
	}
	dst.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glLineWidth(1.0f);
	draw(world_to_camera, camera_to_clip, mode);
	dst.unbind();
}

void Scene::draw(const Eigen::Affine3f world_to_camera,
                 const Eigen::Matrix4f camera_to_clip,
                 RenderMode            mode)
{
	if(!s_shader) {
		s_shader = compile_scene_shader();
	}
	

    s_shader->use();
    s_shader->set_uniform("world_to_camera", world_to_camera);
    s_shader->set_uniform("camera_to_clip",  camera_to_clip);
    s_shader->set_uniform("diffuse_sampler", 0);

    s_shader->set_uniform("use_colors", static_cast<int>(
        mode == OnlyTextures || mode == TexturesAndShading
    ));
    s_shader->set_uniform("use_shading", static_cast<int>(
        mode == OnlyShading || mode == TexturesAndShading
    ));

    int previous_mid = -1;
    for (MaterialMesh material_mesh : m_meshes)
    {
        int mid = material_mesh.material_index;
        if (mid >= 0 && previous_mid != mid)
        {
            glActiveTexture(GL_TEXTURE0);
            if (m_textures[mid])
                m_textures[mid]->bind();
            else
                Texture::unbind();

            glBindBufferBase(GL_UNIFORM_BUFFER, MATERIAL_LOCATION, m_material_indices[mid]);
        }

        int use_vertex_colors = material_mesh.mesh->get_colors().empty() ? 0 : 1;
        s_shader->set_uniform("use_vertex_colors", use_vertex_colors);

        material_mesh.mesh->draw();
        previous_mid = mid;
    }
    glUseProgram(0);
	
}

Scene::Ptr Scene::deserialize(const char* data) noexcept {
    static const std::string header = "fribr_scene_01";
    const std::string data_header = std::string(data, data + header.size());
    if (data_header != header)
    {
        std::cerr << "Incorrect header when deserializing scene: "
                  << data_header << " expected: " << header
                  << std::endl;
        return Scene::Ptr();
    }
    data += header.size();

    // Materials
    uint32_t material_size = 0;
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        reinterpret_cast<char*>(&material_size)[i] = *(data++); 
    std::vector<Material> materials(material_size);
    for (size_t i = 0; i < sizeof(Material) * material_size; ++i)
        reinterpret_cast<char*>(materials.data())[i] = *(data++);

    // Images
    uint32_t image_size = 0;
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        reinterpret_cast<char*>(&image_size)[i] = *(data++); 

    std::vector<cv::Mat3b> images(image_size);
    for (uint32_t i = 0; i < image_size; ++i)
    {
        uint32_t width = 0;
        for (size_t j = 0; j < sizeof(uint32_t); ++j)
            reinterpret_cast<char*>(&width)[j] = *(data++); 

        uint32_t height = 0;
        for (size_t j = 0; j < sizeof(uint32_t); ++j)
            reinterpret_cast<char*>(&height)[j] = *(data++);
                
        images[i] = cv::Mat3b::zeros(height, width);
        for (uint32_t y = 0; y < height; ++y)
        for (uint32_t x = 0; x < width;  ++x)
        {
            cv::Vec3b color;
            color[0] = *(data++);
            color[1] = *(data++);
            color[2] = *(data++);
            images[i](y, x) = color;
        }
    }

    // Meshes
    uint32_t num_meshes = 0;
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        reinterpret_cast<char*>(&num_meshes)[i] = *(data++); 

    std::vector<MaterialMesh> meshes(num_meshes);
    for (uint32_t i = 0; i < num_meshes; ++i)
    {
        uint32_t material_index = 0;
        for (size_t j = 0; j < sizeof(uint32_t); ++j)
            reinterpret_cast<char*>(&material_index)[j] = *(data++); 

        uint32_t mesh_size = 0;
        for (size_t j = 0; j < sizeof(uint32_t); ++j)
            reinterpret_cast<char*>(&mesh_size)[j] = *(data++); 

        MaterialMesh mesh;
        meshes[i].material_index = material_index;
        meshes[i].mesh           = Mesh::deserialize(data);

        data += mesh_size;
    }

    return Scene::Ptr(new Scene(meshes, materials, images));
}

std::vector<char> Scene::serialize() const noexcept {
    static const std::string header = "fribr_scene_01";
    std::vector<char> data;
    data.insert(data.end(), header.begin(), header.end());

    // Materials
    uint32_t material_size = uint32_t(m_materials.size());
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        data.push_back(*(reinterpret_cast<const char*>(&material_size) + i));
    for (size_t i = 0; i < sizeof(Material) * material_size; ++i)
        data.push_back(reinterpret_cast<const char*>(m_materials.data())[i]);

    // Images
    uint32_t image_size = uint32_t(m_images.size());
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        data.push_back(*(reinterpret_cast<const char*>(&image_size) + i));

    for (uint32_t i = 0; i < image_size; ++i)
    {
        uint32_t width = uint32_t(m_images[i].cols);
        for (size_t j = 0; j < sizeof(uint32_t); ++j)
            data.push_back(*(reinterpret_cast<const char*>(&width) + j));

        uint32_t height = uint32_t(m_images[i].rows);
        for (size_t j = 0; j < sizeof(uint32_t); ++j)
            data.push_back(*(reinterpret_cast<const char*>(&height) + j));
        
        for (uint32_t y = 0; y < height; ++y)
        for (uint32_t x = 0; x < width;  ++x)
        {
            cv::Vec3b color = m_images[i](y, x);
            data.push_back(color[0]);
            data.push_back(color[1]);
            data.push_back(color[2]);
        }
    }

    // Meshes
    uint32_t num_meshes = uint32_t(m_meshes.size());
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        data.push_back(*(reinterpret_cast<const char*>(&num_meshes) + i));

    for (uint32_t i = 0; i < num_meshes; ++i)
    {
        uint32_t material_index = uint32_t(m_meshes[i].material_index);
        for (size_t j = 0; j < sizeof(uint32_t); ++j)
            data.push_back(*(reinterpret_cast<const char*>(&material_index) + j));

        std::vector<char> mesh_data = m_meshes[i].mesh->serialize();
        uint32_t mesh_size = uint32_t(mesh_data.size());
        for (size_t j = 0; j < sizeof(uint32_t); ++j)
            data.push_back(*(reinterpret_cast<const char*>(&mesh_size) + j));
        data.insert(data.end(), mesh_data.begin(), mesh_data.end());
    }

    return data;
}

}
