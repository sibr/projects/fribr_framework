#ifndef MESH_H
#define MESH_H

#include "3dmath.h"

#include <assimp/scene.h>
#include <assimp/mesh.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <vector>
#include <memory>
#include <cstdint>

namespace fribr
{

class Mesh
{
public:
    typedef std::shared_ptr<Mesh> Ptr;

    // Vertex shader attribute locations
    static const int VERTEX_LOCATION  = 0;
    static const int NORMAL_LOCATION  = 1;
    static const int TEXTURE_LOCATION = 2;
    static const int COLOR_LOCATION   = 3;

    enum ResourceMode { CPUOnly, CPUAndOpenGL };
    Mesh(const std::vector<uint32_t> &indices,
         const std::vector<float>    &vertices,
         const std::vector<float>    &normals,
         const std::vector<float>    &texture_coordinates,
         const std::vector<float>    &colors,
	 ResourceMode mode = CPUAndOpenGL);
    Mesh(const aiScene *scene, const aiMesh *mesh,
	 CoordinateSystem system = RH_Y_UP,
	 ResourceMode mode = CPUAndOpenGL);
    void upload();
    ~Mesh();

    // Serialization functions for fast file IO.
    static Ptr deserialize(const char* data) throw();
    std::vector<char> serialize() const throw();

    inline void draw() const;

    size_t get_num_vertices()  const throw() { return m_vertices.size() / 3; }
    size_t get_num_triangles() const throw() { return m_num_faces; }

    const std::vector<uint32_t>& get_indices()             const throw() { return m_indices; }
    const std::vector<float>&    get_vertices()            const throw() { return m_vertices; }
    const std::vector<float>&    get_normals()             const throw() { return m_normals; }
    const std::vector<float>&    get_texture_coordinates() const throw() { return m_texture_coordinates; }
    const std::vector<float>&    get_colors()              const throw() { return m_colors; }

private:
    std::vector<uint32_t> m_indices;
    std::vector<float>    m_vertices;
    std::vector<float>    m_normals;
    std::vector<float>    m_texture_coordinates;
    std::vector<float>    m_colors;

    GLuint                m_vao;
    GLuint                m_index_buffer;
    GLuint                m_vertex_buffer;
    GLuint                m_normal_buffer;
    GLuint                m_texture_buffer;
    GLuint                m_color_buffer;
		          
    size_t                m_num_faces;

    // Deliberately left unimplemented, mesh is non-copyable.
    Mesh(const Mesh& rhs);
    const Mesh& operator=(const Mesh& rhs);
};

std::vector<Mesh::Ptr> load_meshes(const std::string &path,
				   CoordinateSystem coordinate_system = RH_Y_UP,
				   Mesh::ResourceMode mode = Mesh::CPUAndOpenGL);

inline void Mesh::draw() const
{
    glBindVertexArray(m_vao);
    glDrawElements(GL_TRIANGLES, GLsizei(3 * m_num_faces), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

}

#endif // MESH_H
