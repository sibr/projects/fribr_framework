#ifndef TYPES_H
#define TYPES_H

#include <Eigen/Core>
#include <cstdint>

namespace fribr
{

struct int2 { int32_t x; int32_t y;
      int32_t& operator[](int i)       { return ((int32_t*)this)[i]; }
const int32_t& operator[](int i) const { return ((int32_t*)this)[i]; }
};
struct int3 { int32_t x; int32_t y; int32_t z;
      int32_t& operator[](int i)       { return ((int32_t*)this)[i]; }
const int32_t& operator[](int i) const { return ((int32_t*)this)[i]; }
};
struct int4 { int32_t x; int32_t y; int32_t z; int32_t w;
      int32_t& operator[](int i)       { return ((int32_t*)this)[i]; }
const int32_t& operator[](int i) const { return ((int32_t*)this)[i]; }
};

inline int2 make_int2(int32_t x, int32_t y)                       
{
    int2 i;
    i.x = x;
    i.y = y;
    return i;
}
inline int2            make_int2(Eigen::Vector2i i) { return make_int2(i.x(), i.y());   }
inline Eigen::Vector2i make_vec2i(int2 i)           { return Eigen::Vector2i(i.x, i.y); }

inline int3 make_int3(int32_t x, int32_t y, int32_t z)
{
    int3 i;
    i.x = x;
    i.y = y;
    i.z = z;
    return i;
}
inline int3            make_int3(Eigen::Vector3i i) { return make_int3(i.x(), i.y(), i.z()); }
inline Eigen::Vector3i make_vec3i(int3 i)           { return Eigen::Vector3i(i.x, i.y, i.z); }


inline int4 make_int4(int32_t x, int32_t y, int32_t z, int32_t w)
{
    int4 i;
    i.x = x;
    i.y = y;
    i.z = z;
    i.w = w;
    return i;
}
inline int4            make_int4(Eigen::Vector4i i) { return make_int4(i.x(), i.y(), i.z(), i.w()); }
inline Eigen::Vector4i make_vec4i(int4 i)           { return Eigen::Vector4i(i.x, i.y, i.z, i.w);   }

struct float2 { float x; float y;
      float& operator[](int i)       { return ((float*)this)[i]; }
const float& operator[](int i) const { return ((float*)this)[i]; }
};
struct float3 { float x; float y; float z;
      float& operator[](int i)       { return ((float*)this)[i]; }
const float& operator[](int i) const { return ((float*)this)[i]; }
};
struct float4 { float x; float y; float z; float w;
      float& operator[](int i)       { return ((float*)this)[i]; }
const float& operator[](int i) const { return ((float*)this)[i]; }
};

inline float2 make_float2(float x, float y)                       
{
    float2 f;
    f.x = x;
    f.y = y;
    return f;
}
inline float2          make_float2(Eigen::Vector2f f) { return make_float2(f.x(), f.y()); }
inline Eigen::Vector2f make_vec2f(float2 f)           { return Eigen::Vector2f(f.x, f.y); }

inline float3 make_float3(float x, float y, float z)
{
    float3 f;
    f.x = x;
    f.y = y;
    f.z = z;
    return f;
}
inline float3          make_float3(Eigen::Vector3f f) { return make_float3(f.x(), f.y(), f.z()); }
inline Eigen::Vector3f make_vec3f(float3 f)           { return Eigen::Vector3f(f.x, f.y, f.z); }

inline float4 make_float4(float x, float y, float z, float w)
{
    float4 f;
    f.x = x;
    f.y = y;
    f.z = z;
    f.w = w;
    return f;
}
inline float4          make_float4(Eigen::Vector4f f) { return make_float4(f.x(), f.y(), f.z(), f.w()); }
inline Eigen::Vector4f make_vec4f(float4 f)           { return Eigen::Vector4f(f.x, f.y, f.z, f.w); }

}

#endif
