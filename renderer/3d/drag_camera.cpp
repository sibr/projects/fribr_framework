#include "3d/drag_camera.h"
#include <cmath>

namespace fribr
{

void DragCamera::init(Eigen::Vector3f center, Eigen::Vector3f normal, Eigen::Vector3f lookat)
{
    m_lookat_point   = lookat;
    m_plane_center   = center;
    m_plane_normal   = normal;
    m_plane_up       = -normal.cross(Eigen::Vector3f(1.0f, 0.0f, 0.0f)).normalized();
    m_plane_right    = normal.cross(m_plane_up);
    m_plane_position = Eigen::Vector2f(0.0f, 0.0f);
    m_snap_speed     = 0.25f;
    m_is_dragging    = false;
}

DragCamera::DragCamera(float near, float far, float fov, float aspect,
		       Eigen::Vector3f center, Eigen::Vector3f normal, Eigen::Vector3f lookat)
    : Camera(near, far, fov, aspect)
{
    init(center, normal, lookat);
}

DragCamera::DragCamera(float near, float far, float fov, float aspect,
		       const CalibratedImage::Vector& cameras)
    : Camera(near, far, fov, aspect)
{
    Eigen::Vector3f normal = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
    Eigen::Vector3f center = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
    for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
    {
	normal += cameras[i].get_forward();
	center += cameras[i].get_position();
    }
    normal.normalize();
    center /= static_cast<float>(cameras.size());

    // Find a point in 3D close to the image center in all cameras.
    // We use a least squares formulation for this: Find the point which
    // minimizes the distance to all lines formed by the cameras and their
    // corresponding forward vectors.
    Eigen::Matrix3f A = Eigen::Matrix3f::Zero();
    Eigen::Vector3f b = Eigen::Vector3f::Zero();
    for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
    {
	Eigen::Vector3f ai = cameras[i].get_position();
	Eigen::Vector3f ni = cameras[i].get_forward();
	A += ni * ni.transpose() - Eigen::Matrix3f::Identity();
	b += (ni * ni.transpose() - Eigen::Matrix3f::Identity()) * ai;
    }
    Eigen::Vector3f lookat = Eigen::Vector3f((A.transpose() * A).ldlt().solve(A.transpose() * b));

    init(center, normal, lookat);
}

void DragCamera::update_transformation(const ImGuiIO &io)
{
    if (!m_is_dragging)
    {
	Camera::update_transformation(io);
	return;
    }
    
    if (io.MouseDown[0] && !ImGui::IsMouseHoveringAnyWindow())
    {
	ImVec2 mouse_delta = io.MouseDelta;
	float  right_delta = -mouse_delta.x * get_mouse_sensitivity();
	float  up_delta    =  mouse_delta.y * get_mouse_sensitivity();

	m_plane_position.x() += right_delta;
	m_plane_position.y() += up_delta;
    }

    set_position(m_plane_center                       +
		 m_plane_up    * m_plane_position.y() +
		 m_plane_right * m_plane_position.x());

    Eigen::Vector3f forward = m_lookat_point - get_position();
    if (forward.dot(m_plane_normal) <= 0.0f)
	forward = m_plane_normal;
    set_forward(forward.normalized());
}

void DragCamera::update_transformation(const ImGuiIO &io, const CalibratedImage::Vector& cameras)
{
    if (!m_is_dragging)
    {
	Camera::update_transformation(io);
	return;
    }

    update_transformation(io);
    bool user_is_dragging = io.MouseDown[0] && !ImGui::IsMouseHoveringAnyWindow();
    if (user_is_dragging)
	return;

    float nearest_up       = m_plane_position.y();
    float nearest_right    = m_plane_position.x();
    float nearest_distance = 1e21f;

    for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
    {
	Eigen::Vector3f offset = cameras[i].get_position() - m_plane_center;
	float           up     = offset.dot(m_plane_up);
	float           right  = offset.dot(m_plane_right);

	float distance = (Eigen::Vector2f(right, up) - m_plane_position).squaredNorm();
	if (distance < nearest_distance)
	{
	    nearest_up       = up;
	    nearest_right    = right;
	    nearest_distance = distance;
	}
    }
    m_plane_position = (1.0f - m_snap_speed) * m_plane_position +
                       m_snap_speed * Eigen::Vector2f(nearest_right, nearest_up);

    set_position(m_plane_center                       +
		 m_plane_up    * m_plane_position.y() +
		 m_plane_right * m_plane_position.x());

    Eigen::Vector3f forward = m_lookat_point - get_position();
    if (forward.dot(m_plane_normal) <= 0.0f)
	forward = m_plane_normal;
    set_forward(forward.normalized());
}

}
