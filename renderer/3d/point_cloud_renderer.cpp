#include "3d/point_cloud_renderer.h"
#include "tools/string_tools.h"
#include "io/nvm.h"

#include <iostream>
#include <sstream>

namespace
{

fribr::Shader* compile_cloud_shader()
{
    fribr::Shader* shader = new fribr::Shader();
    shader->vertex_shader(
	GLSL(330,
	     layout(location = 0) in vec3 vertex_position;
	     layout(location = 1) in vec3 vertex_normal;
	     layout(location = 2) in vec3 vertex_color;

	     uniform mat4 camera_to_clip;
	     uniform mat4 world_to_camera;

	     uniform bool use_normals;
	     uniform bool use_colors;

	     out vec3 position;
	     out vec3 normal;
	     out vec3 color;

	     void main()
	     {
             position    = (world_to_camera * vec4(vertex_position, 1.0)).xyz;
             if (use_normals)
                 normal  = (world_to_camera * vec4(vertex_normal, 0.0)).xyz;
             color       = use_colors ? vertex_color : vec3(1.0, 0.0, 1.0);
             gl_Position = camera_to_clip * vec4(position, 1.0);
	     }
	    ));

    shader->fragment_shader(
	GLSL(330,
	     in vec3 position;
	     in vec3 normal;
	     in vec3 color;
	     out vec4 frag_color;

	     uniform bool use_normals;

	     void main()
	     {
             float clamped_cosine = 1.0f;
             if (use_normals)
             {
                 vec3 pixel_normal   = normalize(normal);
                 vec3 view_direction = normalize(-position);
                 clamped_cosine      = max(dot(pixel_normal, view_direction), 0.0);
             }
             frag_color = vec4(clamped_cosine * color, 1.0);
	     }
	    ));
    shader->link();

    return shader;
}

}

namespace fribr
{
    
int            PointCloudRenderer::s_num_instances = 0;
fribr::Shader* PointCloudRenderer::s_shader        = 0;

PointCloudRenderer::PointCloudRenderer(fribr::PointCloud::Ptr cloud) noexcept
	: m_point_cloud(cloud)
{
    // The first instance of PointCloudRenderer is responsible for compiling the shader.
    if (s_num_instances++ == 0)
        s_shader = compile_cloud_shader();
}

PointCloudRenderer::~PointCloudRenderer() noexcept {
    // The last instance of PointCloudRenderer is responsible for deleting the shader.
    if (--s_num_instances == 0)
    {
        delete s_shader;
        s_shader = 0;
    }
}

void PointCloudRenderer::draw(const Eigen::Affine3f world_to_camera,
                              const Eigen::Matrix4f camera_to_clip,
                              float point_size)
{
    if (!m_point_cloud)
        return;

    s_shader->use();
    s_shader->set_uniform("world_to_camera", world_to_camera);
    s_shader->set_uniform("camera_to_clip",  camera_to_clip);
    s_shader->set_uniform("use_normals",     (int)m_point_cloud->has_normals());
    s_shader->set_uniform("use_colors",      (int)m_point_cloud->has_colors());
    m_point_cloud->draw(point_size);
    glUseProgram(0);
}

}
