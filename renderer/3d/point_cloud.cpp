//#include "3d/point_cloud.h"
#include "projects/fribr_framework/renderer/3d/point_cloud.h"
//#include "io.h"
#include "projects/fribr_framework/renderer/io.h"
//#include "tools.h"
#include "projects/fribr_framework/renderer/tools.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/mesh.h>

#include <Eigen/Core>

#include <iostream>
#include <fstream>

namespace
{

float read_float_b(std::ifstream &file)
{
    float buff[1];
    file.read((char*) buff, 1 * sizeof(float) / sizeof(char));
    return buff[0];
}

float read_uchar_b(std::ifstream &file)
{
    unsigned char buff[1];
    file.read((char*) buff, 1);
    return buff[0] / 255.0f;
}

fribr::float2 read_float2_b(std::ifstream &file)
{
    float buff[2];
    file.read((char*) buff, 2 * sizeof(float) / sizeof(char));
    return fribr::make_float2(buff[0], buff[1]);
}

fribr::float2 read_uchar2_b(std::ifstream &file)
{
    unsigned char buff[2];
    file.read((char*) buff, 2);
    return fribr::make_float2(buff[0] / 255.0f, buff[1] / 255.0f);
}

fribr::float3 read_float3_b(std::ifstream &file)
{
    float buff[3];
    file.read((char*) buff, 3 * sizeof(float) / sizeof(char));
    return fribr::make_float3(buff[0], buff[1], buff[2]);
}

fribr::float3 read_uchar3_b(std::ifstream &file)
{
    unsigned char buff[3];
    file.read((char*) buff, 3);
    return fribr::make_float3(buff[0] / 255.0f, buff[1] / 255.0f, buff[2] / 255.0f);
}

}

namespace fribr
{

PointCloud::Ptr merge_point_clouds(const std::vector<PointCloud::Ptr> clouds)
{
    bool all_has_normals = true;
    bool all_has_colors  = true;
    for (PointCloud::Ptr cloud : clouds)
    {
        all_has_normals = cloud->has_normals() && all_has_normals;
        all_has_colors  = cloud->has_colors()  && all_has_colors;
    }

    std::vector<float3> vertices, normals, colors;
    for (PointCloud::Ptr cloud : clouds)
    {
        vertices.insert(vertices.end(),
                        cloud->get_vertices().begin(),
                        cloud->get_vertices().end());

        if (all_has_normals)
            normals.insert(normals.end(),
                           cloud->get_normals().begin(),
                           cloud->get_normals().end());

        if (all_has_colors)
            colors.insert(colors.end(),
                          cloud->get_colors().begin(),
                          cloud->get_colors().end());
    }

    return PointCloud::Ptr(new PointCloud(vertices, normals, colors));
}

PointCloud::Ptr load_ply_point_cloud(const std::string &file_path, CoordinateSystem cs, bool with_normals)
{
    enum Read_mode {ASCII, BINARY, ELSE};
    enum Property  {VERTEX, NORMAL, COLOR_A, COLOR_F, ALPHA_A, ALPHA_F};

    std::vector<float3> vertices;
    std::vector<float3> normals;
    std::vector<float3> colors;

    std::ifstream file(file_path, std::ifstream::in | std::ifstream::binary);

    std::string line;
    std::vector<std::string> tokens;

    Read_mode read_mode;

    Eigen::Matrix3f converter = to_right_handed_y_up(cs);

    std::getline(file, line);
    if (line.substr(0,3) != "ply")
    {
        std::cout << "File is not ply: " << file_path << std::endl;
        file.close();
        return PointCloud::Ptr();
    }

    std::getline(file, line);
    tokens = split_string(line);
    if (tokens[1] == "ascii")
    {
        read_mode = ASCII;
    }
    else if (tokens[1] == "binary_little_endian")
    {
        read_mode = BINARY;
    }
    else
    {
        read_mode = ELSE;
    }

    if (read_mode == ASCII)
    {
        file.close();
        file.open(file_path, std::ifstream::in);
        std::getline(file, line);
        std::getline(file, line);
    }
    else if (read_mode == ELSE)
    {
        std::cout << "ply mode not supported yet" << std::endl;
        return PointCloud::Ptr();
    }

    int nb_verts = 0;
    int nb_faces = 0;
    std::vector<Property> properties;

    while (std::getline(file, line))
    {
        tokens = split_string(line);

        if (tokens[0] == "end_header")
            break;

        if (tokens[0] == "element")
        {
            if (tokens[1] == "vertex")
            {
                nb_verts = std::stoi(tokens[2]);
            }
            else if (tokens[1] == "face")
            {
                nb_faces = std::stoi(tokens[2]);
            }
        }

        if (tokens[0] == "property")
        {
            if (tokens[1] == "float" && tokens[2] == "x")
                properties.push_back(VERTEX);
            if (tokens[1] == "float" && tokens[2] == "nx")
                properties.push_back(NORMAL);
            if (tokens[1] == "uchar" && (tokens[2] == "red" || tokens[2] == "diffuse_red"))
                properties.push_back(COLOR_A);
            if (tokens[1] == "float" && (tokens[2] == "red" || tokens[2] == "diffuse_red"))
                properties.push_back(COLOR_F);
            if (tokens[1] == "uchar" && (tokens[2] == "alpha" || tokens[2] == "diffuse_alpha"))
                properties.push_back(ALPHA_A);
            if (tokens[1] == "float" && (tokens[2] == "alpha" || tokens[2] == "diffuse_alpha"))
                properties.push_back(ALPHA_F);
        }
    }

    //read vertices
    vertices.reserve(nb_verts);
    normals.reserve(nb_verts);
    colors.reserve(nb_verts);

    bool read_binary = read_mode == BINARY;

    for (int i = 0; i < nb_verts; ++i)
    {
        if (!read_binary)
        {
            std::getline(file, line);
            tokens = split_string(line);
        }

        for (size_t j = 0; j < properties.size(); ++j)
        {
            Property property = properties[j];
            switch (property) {
            case VERTEX:
                vertices.push_back(read_binary ? read_float3_b(file)
                                               : make_float3(float(std::atof(tokens[3 * j + 0].c_str())),
                                                             float(std::atof(tokens[3 * j + 1].c_str())),
                                                             float(std::atof(tokens[3 * j + 2].c_str()))));
                break;
            case NORMAL:
                normals.push_back(read_binary ? read_float3_b(file)
                                              : make_float3(float(std::atof(tokens[3 * j + 0].c_str())),
                                                            float(std::atof(tokens[3 * j + 1].c_str())),
                                                            float(std::atof(tokens[3 * j + 2].c_str()))));
                break;
            case COLOR_F:
                colors.push_back(read_binary ? read_float3_b(file)
                                             : make_float3(float(std::atof(tokens[3 * j + 0].c_str())),
                                                           float(std::atof(tokens[3 * j + 1].c_str())),
                                                           float(std::atof(tokens[3 * j + 2].c_str()))));
                break;
            case COLOR_A:
                colors.push_back(read_binary ? read_uchar3_b(file) 
                                             : make_float3(float(std::atof(tokens[3 * j + 0].c_str())) / 255.0f,
                                                           float(std::atof(tokens[3 * j + 1].c_str())) / 255.0f,
                                                           float(std::atof(tokens[3 * j + 2].c_str())) / 255.0f));
                break;
            case ALPHA_F:
                if (read_binary)
                    read_float_b(file);
                break;
            case ALPHA_A:
                if (read_binary)
                    read_uchar_b(file);
                break;
            default:
                break;
            }
        }
    }

    file.close();

    std::cout << "PLY file loaded: " << nb_verts << " vertices and " << nb_faces << " faces." << std::endl;

    if (!with_normals)
        normals.clear();

    for (auto& v : vertices)
    {
        v = make_float3(converter * make_vec3f(v));
    }

    for (auto& n : normals)
    {
        n = make_float3(converter * make_vec3f(n));
    }

    return fribr::PointCloud::Ptr(new fribr::PointCloud(vertices, normals, colors));
}

void store_point_cloud(const std::string &file_path, PointCloud::Ptr point_cloud)
{
    std::ofstream out_file(file_path, std::ios_base::out   |
                                      std::ios_base::trunc | 
                                      std::ios_base::binary);
    if (!out_file.good())
    {
        std::cerr << "Could not open output file: " << file_path << std::endl;
        return;
    }

    size_t vertex_size = 3;
    out_file << "ply" << std::endl;
    out_file << "format binary_little_endian 1.0" << std::endl;
    out_file << "element vertex " << point_cloud->get_vertices().size() << std::endl;

    out_file << "property float x" << std::endl;
    out_file << "property float y" << std::endl;
    out_file << "property float z" << std::endl;

    if (point_cloud->has_normals())
    {
        out_file << "property float nx" << std::endl;
        out_file << "property float ny" << std::endl;
        out_file << "property float nz" << std::endl;
        vertex_size += 3;
    }

    if (point_cloud->has_colors())
    {
        out_file << "property float red" << std::endl;
        out_file << "property float green" << std::endl;
        out_file << "property float blue" << std::endl;
        vertex_size += 3;

    }
    out_file << "end_header" << std::endl;

    std::vector<float> vertex(vertex_size, 0.0f);
    for (size_t i = 0; i < point_cloud->get_vertices().size(); i++)
    {
        int index = 0;
        vertex[index + 0] = point_cloud->get_vertices()[i].x;
        vertex[index + 1] = point_cloud->get_vertices()[i].y;
        vertex[index + 2] = point_cloud->get_vertices()[i].z;
        index += 3;

        if (point_cloud->has_normals())
        {
            vertex[index + 0] = point_cloud->get_normals()[i].x;
            vertex[index + 1] = point_cloud->get_normals()[i].y;
            vertex[index + 2] = point_cloud->get_normals()[i].z;
            index += 3;
        }

        if (point_cloud->has_colors())
        {
            vertex[index + 0] = point_cloud->get_colors()[i].x;
            vertex[index + 1] = point_cloud->get_colors()[i].y;
            vertex[index + 2] = point_cloud->get_colors()[i].z;
            index += 3;
        }

        out_file.write((char*)vertex.data(), vertex.size() * sizeof(float) / sizeof(char));
    }

    out_file.close();
}

}

namespace fribr
{

PointCloud::Ptr load_point_cloud(const std::string &file_path, CoordinateSystem cs, bool with_normals)
{
    if (get_extension(file_path) == "nvm")
        return NVM::load_point_cloud(file_path);
    else if (get_extension(file_path) == "json")
        return OpenMVG::load_point_cloud(file_path);
    else if (get_extension(file_path) == "ply")
        return load_ply_point_cloud(file_path, cs, with_normals);
    else if (get_extension(file_path) == "patch")
        return PMVS::load_point_cloud(file_path);

    std::cerr << "Unrecognized file extension for path: " << file_path << std::endl;
    return PointCloud::Ptr();
}

PointCloud::PointCloud(const std::vector<float3> &vertices, const std::vector<float3> &normals, const std::vector<float3> &colors)
    : m_vao(0),
      m_vertex_buffer(0),
      m_normal_buffer(0),
      m_color_buffer(0),
      m_vertices(vertices),
      m_normals(normals),
      m_colors(colors)
{
}

PointCloud::~PointCloud()
{
    if (m_vao)
        glDeleteVertexArrays(1, &m_vao);
    if (m_vertex_buffer)
        glDeleteBuffers(1, &m_vertex_buffer);
    if (m_normal_buffer)
        glDeleteBuffers(1, &m_normal_buffer);
    if (m_color_buffer)
        glDeleteBuffers(1, &m_color_buffer);
}

void PointCloud::append(fribr::PointCloud::Ptr cloud)
{
    if (has_colors()  != cloud->has_colors() ||
        has_normals() != cloud->has_normals())
    {
        std::cout << "ERROR: Point clouds have different attributes, not merging" << std::endl;
        return;
    }

    if (m_vao)
        glDeleteVertexArrays(1, &m_vao);
    if (m_vertex_buffer)
        glDeleteBuffers(1, &m_vertex_buffer);
    if (m_normal_buffer)
        glDeleteBuffers(1, &m_normal_buffer);
    if (m_color_buffer)
        glDeleteBuffers(1, &m_color_buffer);

    m_vao           = 0;
    m_vertex_buffer = 0;
    m_normal_buffer = 0;
    m_color_buffer  = 0;

    m_vertices.insert(m_vertices.end(),
                      cloud->get_vertices().begin(),
                      cloud->get_vertices().end());

    if (has_normals())
        m_normals.insert(m_normals.end(),
                         cloud->get_normals().begin(),
                         cloud->get_normals().end());

    if (has_colors())
        m_colors.insert(m_colors.end(),
                        cloud->get_colors().begin(),
                        cloud->get_colors().end());
}

void PointCloud::upload_if_needed() const
{
    if (m_vao)
        return;

    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    if (!m_vertices.empty())
    {
        glGenBuffers(1, &m_vertex_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float3) * m_vertices.size(), m_vertices.data(), GL_STATIC_DRAW);

        glEnableVertexAttribArray(VERTEX_LOCATION);
        glVertexAttribPointer(VERTEX_LOCATION, 3, GL_FLOAT, 0, 0, 0);
    }

    if (!m_normals.empty())
    {
        glGenBuffers(1, &m_normal_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_normal_buffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float3) * m_normals.size(), m_normals.data(), GL_STATIC_DRAW);

        glEnableVertexAttribArray(NORMAL_LOCATION);
        glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, 0, 0, 0);
    }

    if (!m_colors.empty())
    {
        glGenBuffers(1, &m_color_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_color_buffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float3) * m_colors.size(), m_colors.data(), GL_STATIC_DRAW);

        glEnableVertexAttribArray(COLOR_LOCATION);
        glVertexAttribPointer(COLOR_LOCATION, 3, GL_FLOAT, 0, 0, 0);
    }

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
}

}
