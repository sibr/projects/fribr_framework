#ifndef POINT_CLOUD_RENDERER_H
#define POINT_CLOUD_RENDERER_H

#include "vision/calibrated_image.h"
#include "3d/point_cloud.h"
#include "gl_wrappers/shader.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <vector>
#include <string>
#include <memory>
#include <exception>

namespace fribr
{

class PointCloudRenderer
{
public:
    typedef std::shared_ptr<PointCloudRenderer> Ptr;

    PointCloudRenderer(fribr::PointCloud::Ptr cloud) throw();
    ~PointCloudRenderer() throw();

    void draw(const Eigen::Affine3f world_to_camera,
              const Eigen::Matrix4f camera_to_clip,
              float point_size = 1.0f);

    fribr::PointCloud::Ptr get_point_cloud() throw() { return m_point_cloud; }

private:
    static int                          s_num_instances;
    static fribr::Shader*               s_shader;

    fribr::PointCloud::Ptr              m_point_cloud;

    // Deliberately left unimplemented, PointCloudRenderer is non-copyable.
    PointCloudRenderer(const PointCloudRenderer &rhs);
    PointCloudRenderer& operator=(const PointCloudRenderer &rhs);
};

}

#endif
