#ifndef POINTCLOUD_H
#define POINTCLOUD_H

//#include "3d/types.h"
#include "projects/fribr_framework/renderer/3d/types.h"
//#include "3dmath.h"
#include "projects/fribr_framework/renderer/3dmath.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <vector>
#include <memory>

namespace fribr
{

class PointCloud
{
public:
    typedef std::shared_ptr<PointCloud> Ptr;

    // Vertex shader attribute locations
    static const int VERTEX_LOCATION  = 0;
    static const int NORMAL_LOCATION  = 1;
    static const int COLOR_LOCATION   = 2;

    PointCloud(const std::vector<float3> &vertices,
               const std::vector<float3> &normals,
               const std::vector<float3> &colors);
    ~PointCloud();

    void append(PointCloud::Ptr cloud);

    inline void draw(float point_size) const;

    const std::vector<float3>& get_vertices() const throw() { return m_vertices; }
    const std::vector<float3>& get_normals()  const throw() { return m_normals;  }
    const std::vector<float3>& get_colors()   const throw() { return m_colors;   }

    bool has_colors()  const                     { return !m_colors.empty();   }
    bool has_normals() const                     { return !m_normals.empty();  }

private:
    void upload_if_needed() const;

    mutable GLuint      m_vao;
    mutable GLuint      m_vertex_buffer;
    mutable GLuint      m_normal_buffer;
    mutable GLuint      m_color_buffer;

    std::vector<float3> m_vertices;
    std::vector<float3> m_normals;
    std::vector<float3> m_colors;


    // Deliberately left unimplemented, mesh is non-copyable.
    PointCloud(const PointCloud& rhs);
    const PointCloud& operator=(const PointCloud& rhs);
};

PointCloud::Ptr merge_point_clouds (const std::vector<PointCloud::Ptr> clouds);

PointCloud::Ptr load_point_cloud   (const std::string &file_path,
                                    CoordinateSystem cs = RH_Y_DOWN,
                                    bool with_normals = true);
void            store_point_cloud  (const std::string &file_path,
                                    PointCloud::Ptr point_cloud);

inline void PointCloud::draw(float point_size) const
{
    upload_if_needed();
    glPointSize(point_size);
    glBindVertexArray(m_vao);
    glDrawArrays(GL_POINTS, 0, GLsizei(m_vertices.size()));
    glBindVertexArray(0);
    glPointSize(1.0f);
}

}

#endif // POINTCLOUD_H
