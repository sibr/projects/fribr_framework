#ifndef SCENE_H
#define SCENE_H

#include "projects/fribr_framework/renderer/3dmath.h"
#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/3d/mesh.h"

#include <assimp/scene.h>
#include <assimp/mesh.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <vector>
#include <string>
#include <memory>
#include <exception>

#include <core/graphics/Texture.hpp>
namespace fribr
{

class Scene
{
public:
    typedef std::shared_ptr<Scene> Ptr;

    // Fragment shader uniform locations
    static const int MATERIAL_LOCATION = 0;

    Scene(const std::string &path, CoordinateSystem system = RH_Y_UP, bool mergeVerts = false, bool msgs=true) noexcept(false);
    ~Scene() throw();

    // Serialization functions for fast file IO.
    static Ptr deserialize(const char* data) noexcept;
    std::vector<char> serialize() const noexcept;

    enum RenderMode { OnlyTextures, OnlyShading, TexturesAndShading };
    void draw(sibr::IRenderTarget & dst,
		const Eigen::Affine3f world_to_camera,
              const Eigen::Matrix4f camera_to_clip,
              RenderMode            mode);

	void draw(
		const Eigen::Affine3f world_to_camera,
		const Eigen::Matrix4f camera_to_clip,
		RenderMode            mode);

    // For rendering with external shaders, ensures that
    // get_textures()[i] is the correct texture for get_meshes()[i].
    std::vector<Texture::Ptr> get_textures();
    std::vector<Mesh::Ptr> get_meshes();

protected:
    struct Material
    {
        float diffuse[4];
        float ambient[4];
        float specular[4];
        float emissive[4];
        float shininess;
        int   use_texture;
    };

    struct MaterialMesh {
        int       material_index;
        Mesh::Ptr mesh;
        bool operator<(const MaterialMesh& rhs) const { return material_index < rhs.material_index; }
    };

    Scene(const std::vector<MaterialMesh> &meshes,
          const std::vector<Material>     &materials,
          const std::vector<cv::Mat3b>    &images);

    std::vector<MaterialMesh> m_meshes;
    std::vector<Material>     m_materials;
    std::vector<GLuint>       m_material_indices;
    std::vector<Texture::Ptr> m_textures;
    std::vector<cv::Mat3b>    m_images;

	bool						_clearDst = true;
private:
    static int                s_num_instances;
    static Shader*            s_shader;

    // Deliberately left unimplemented, Scene is non-copyable.
    Scene(const Scene &rhs);
    Scene& operator=(const Scene &rhs);
};

}

#endif
