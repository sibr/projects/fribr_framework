#ifndef DRAG_CAMERA_H
#define DRAG_CAMERA_H

#include "3d/camera.h"
#include "vision/calibrated_image.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <iostream>


namespace fribr
{

class DragCamera : public Camera
{
public:
    DragCamera(float near, float far, float fov, float aspect,
	       Eigen::Vector3f center, Eigen::Vector3f normal, Eigen::Vector3f lookat);
    DragCamera(float near, float far, float fov, float aspect,
	       const CalibratedImage::Vector& cameras);

    void update_transformation(const ImGuiIO &io);
    void update_transformation(const ImGuiIO &io, const CalibratedImage::Vector& cameras);

    Eigen::Vector2f plane_position() const throw() { return m_plane_position; }
    void            plane_position(Eigen::Vector2f p) throw() { m_plane_position = p; }

    bool is_dragging() const throw() { return m_is_dragging; }
    void is_dragging(bool b) throw() { m_is_dragging = b; }

    float snap_speed() const throw() { return m_snap_speed; }
    void  snap_speed(float f) throw() { m_snap_speed = f; }

    Eigen::Vector3f get_center() const { return m_plane_center; }
    Eigen::Vector3f get_normal() const { return m_plane_normal; }
    Eigen::Vector3f get_lookat() const { return m_lookat_point; }

private:

    void init(Eigen::Vector3f center, Eigen::Vector3f normal, Eigen::Vector3f lookat);
    
    bool  m_is_dragging;
    float m_snap_speed;

    Eigen::Vector3f m_lookat_point;
    Eigen::Vector3f m_plane_center;
    Eigen::Vector3f m_plane_normal;
    Eigen::Vector3f m_plane_up;
    Eigen::Vector3f m_plane_right;
    Eigen::Vector2f m_plane_position;
};

}

#endif // CAMERA_H
