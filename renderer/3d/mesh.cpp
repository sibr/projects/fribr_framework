#include "3d/mesh.h"

#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <assimp/mesh.h>
#include <Eigen/Core>

#include <vector>
#include <iostream>

namespace fribr
{

std::vector<Mesh::Ptr> load_meshes(const std::string &path, CoordinateSystem system, Mesh::ResourceMode mode)
{
    std::vector<Mesh::Ptr> meshes;
    Assimp::Importer importer;

    const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices);
    if (!scene)
    {
        std::cout << "Loading failed." << std::endl;
        return meshes;
    }

    meshes.reserve(scene->mNumMeshes);
    for (size_t i = 0; i < scene->mNumMeshes; ++i)
    {
        Mesh::Ptr mesh(new Mesh(scene, scene->mMeshes[i], system, mode));
        if (mesh->get_indices().size()  % 3 != 0 || mesh->get_vertices().size() % 3 != 0)
        {
            std::cout << "Warning: Ignoring mesh with " << mesh->get_vertices().size() << " vertices and "
                      << mesh->get_indices().size() << " indices." << std::endl;
            continue;
        }
        meshes.push_back(mesh);
    }

    return meshes;
}

Mesh::Mesh(const std::vector<uint32_t> &indices,
           const std::vector<float>    &vertices,
           const std::vector<float>    &normals,
           const std::vector<float>    &texture_coordinates,
           const std::vector<float>    &colors,
	   ResourceMode                 mode)
    : m_vao(0),
      m_index_buffer(0),
      m_vertex_buffer(0),
      m_normal_buffer(0),
      m_texture_buffer(0),
      m_color_buffer(0),
      m_num_faces(0)
{
    m_indices   = indices;
    m_num_faces = indices.size() / 3;

    m_vertices = vertices;
    m_normals = normals;
    m_texture_coordinates = texture_coordinates;
    m_colors = colors;

    if (mode == CPUAndOpenGL)
	upload();
}

Mesh::Mesh(const aiScene *scene, const aiMesh *mesh, CoordinateSystem system, ResourceMode mode)
    : m_vao(0),
      m_index_buffer(0),
      m_vertex_buffer(0),
      m_normal_buffer(0),
      m_texture_buffer(0),
      m_color_buffer(0),
      m_num_faces(0)
{
    m_indices.reserve(mesh->mNumFaces * 3);
    for (size_t i = 0; i < mesh->mNumFaces; ++i)
    {
        const aiFace* face = &mesh->mFaces[i];
        for (size_t j = 0; j < face->mNumIndices; ++j)
            m_indices.push_back(face->mIndices[j]);
    }
    m_num_faces = mesh->mNumFaces;

    Eigen::Matrix3f converter = to_right_handed_y_up(system);
    if (mesh->HasPositions())
    {
        m_vertices.reserve(3 * mesh->mNumVertices);
        for (size_t i = 0; i < mesh->mNumVertices; ++i)
        {
            Eigen::Vector3f point = converter * Eigen::Vector3f(mesh->mVertices[i].x,
								mesh->mVertices[i].y,
								mesh->mVertices[i].z);
            m_vertices.push_back(point[0]);
            m_vertices.push_back(point[1]);
            m_vertices.push_back(point[2]);
        }
    }

    if (mesh->HasNormals())
    {
        m_normals.reserve(3 * mesh->mNumVertices);
        for (size_t i = 0; i < mesh->mNumVertices; ++i)
        {
            Eigen::Vector3f normal = converter * Eigen::Vector3f(mesh->mNormals[i].x,
                                                                 mesh->mNormals[i].y,
                                                                 mesh->mNormals[i].z);
            m_normals.push_back(normal[0]);
            m_normals.push_back(normal[1]);
            m_normals.push_back(normal[2]);
        }
    }

    if (mesh->HasTextureCoords(0))
    {
        m_texture_coordinates.reserve(mesh->mNumVertices * 2);
        for (unsigned int k = 0; k < mesh->mNumVertices; ++k)
        {
            m_texture_coordinates.push_back(mesh->mTextureCoords[0][k].x);
            m_texture_coordinates.push_back(mesh->mTextureCoords[0][k].y);

        }
    }

    if (mesh->HasVertexColors(0))
    {
        m_colors.reserve(3 * mesh->mNumVertices);
        for (size_t i = 0; i < mesh->mNumVertices; ++i)
        {
            Eigen::Vector3f color = Eigen::Vector3f(mesh->mColors[0][i].r,
                                                    mesh->mColors[0][i].g,
                                                    mesh->mColors[0][i].b);
            m_colors.push_back(color[0]);
            m_colors.push_back(color[1]);
            m_colors.push_back(color[2]);
        }
    }

    if (mode == CPUAndOpenGL)
	upload();
}

void Mesh::upload()
{
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_index_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 sizeof(unsigned int) * m_indices.size(),
                 m_indices.data(), GL_STATIC_DRAW);

    if (!m_vertices.empty())
    {
        glGenBuffers(1, &m_vertex_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer);
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(float) * m_vertices.size(),
                     m_vertices.data(), GL_STATIC_DRAW);

        glEnableVertexAttribArray(VERTEX_LOCATION);
        glVertexAttribPointer(VERTEX_LOCATION, 3, GL_FLOAT, 0, 0, 0);
    }

    if (!m_normals.empty())
    {
        glGenBuffers(1, &m_normal_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_normal_buffer);
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(float) * m_normals.size(),
                     m_normals.data(), GL_STATIC_DRAW);

        glEnableVertexAttribArray(NORMAL_LOCATION);
        glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, 0, 0, 0);
    }

    if (!m_texture_coordinates.empty())
    {
        glGenBuffers(1, &m_texture_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_texture_buffer);
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(float) * m_texture_coordinates.size(),
                     m_texture_coordinates.data(), GL_STATIC_DRAW);

        glEnableVertexAttribArray(TEXTURE_LOCATION);
        glVertexAttribPointer(TEXTURE_LOCATION, 2, GL_FLOAT, 0, 0, 0);
    }

    if (!m_colors.empty())
    {
        glGenBuffers(1, &m_color_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_color_buffer);
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(float) * m_colors.size(),
                     m_colors.data(), GL_STATIC_DRAW);

        glEnableVertexAttribArray(COLOR_LOCATION);
        glVertexAttribPointer(COLOR_LOCATION, 3, GL_FLOAT, 0, 0, 0);
    }

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

Mesh::~Mesh()
{
    if (m_vao)
        glDeleteVertexArrays(1, &m_vao);
    if (m_index_buffer)
        glDeleteBuffers(1, &m_index_buffer);
    if (m_vertex_buffer)
        glDeleteBuffers(1, &m_vertex_buffer);
    if (m_normal_buffer)
        glDeleteBuffers(1, &m_normal_buffer);
    if (m_texture_buffer)
        glDeleteBuffers(1, &m_texture_buffer);
    if (m_color_buffer)
        glDeleteBuffers(1, &m_color_buffer);
}

Mesh::Ptr Mesh::deserialize(const char* data) noexcept {
    static const std::string header = "fribr_mesh_01";
    const std::string data_header = std::string(data, data + header.size());
    if (data_header != header)
    {
        std::cerr << "Incorrect header when deserializing mesh: "
                  << data_header << " expected: " << header
                  << std::endl;
        return Mesh::Ptr();
    }
    data += header.size();

    // Indices
    uint32_t index_size = 0;
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        reinterpret_cast<char*>(&index_size)[i] = *(data++); 
    std::vector<uint32_t> indices(index_size);
    for (size_t i = 0; i < sizeof(uint32_t) * index_size; ++i)
        reinterpret_cast<char*>(indices.data())[i] = *(data++);

    // Vertices
    uint32_t vertex_size = 0;
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        reinterpret_cast<char*>(&vertex_size)[i] = *(data++); 
    std::vector<float> vertices(vertex_size);
    for (size_t i = 0; i < sizeof(float) * vertex_size; ++i)
        reinterpret_cast<char*>(vertices.data())[i] = *(data++);

    // Normals
    uint32_t normal_size = 0;
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        reinterpret_cast<char*>(&normal_size)[i] = *(data++); 
    std::vector<float> normals(normal_size);
    for (size_t i = 0; i < sizeof(float) * normal_size; ++i)
        reinterpret_cast<char*>(normals.data())[i] = *(data++);

    // Texture coordinates
    uint32_t texture_coordinate_size = 0;
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        reinterpret_cast<char*>(&texture_coordinate_size)[i] = *(data++); 
    std::vector<float> texture_coordinates(texture_coordinate_size);
    for (size_t i = 0; i < sizeof(float) * texture_coordinate_size; ++i)
        reinterpret_cast<char*>(texture_coordinates.data())[i] = *(data++);

    // Colors
    uint32_t color_size = 0;
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        reinterpret_cast<char*>(&color_size)[i] = *(data++); 
    std::vector<float> colors(color_size);
    for (size_t i = 0; i < sizeof(float) * color_size; ++i)
        reinterpret_cast<char*>(colors.data())[i] = *(data++);

    return Mesh::Ptr(new Mesh(indices, vertices, normals, texture_coordinates, colors));
}

std::vector<char> Mesh::serialize() const noexcept {
    static const std::string header = "fribr_mesh_01";
    std::vector<char> data;
    data.insert(data.end(), header.begin(), header.end());
    
    // Indices
    uint32_t index_size = uint32_t(m_indices.size());
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        data.push_back(*(reinterpret_cast<const char*>(&index_size) + i));
    for (size_t i = 0; i < sizeof(uint32_t) * index_size; ++i)
        data.push_back(reinterpret_cast<const char*>(m_indices.data())[i]);

    // Vertices
    uint32_t vertex_size = uint32_t(m_vertices.size());
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        data.push_back(*(reinterpret_cast<const char*>(&vertex_size) + i));
    for (size_t i = 0; i < sizeof(float) * vertex_size; ++i)
        data.push_back(reinterpret_cast<const char*>(m_vertices.data())[i]);

    // Normals
    uint32_t normal_size = uint32_t(m_normals.size());
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        data.push_back(*(reinterpret_cast<const char*>(&normal_size) + i));
    for (size_t i = 0; i < sizeof(float) * normal_size; ++i)
        data.push_back(reinterpret_cast<const char*>(m_normals.data())[i]);

    // Texture coordinates
    uint32_t texture_coordinate_size = uint32_t(m_texture_coordinates.size());
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        data.push_back(*(reinterpret_cast<const char*>(&texture_coordinate_size) + i));
    for (size_t i = 0; i < sizeof(float) * texture_coordinate_size; ++i)
        data.push_back(reinterpret_cast<const char*>(m_texture_coordinates.data())[i]);

    // Colors
    uint32_t color_size = uint32_t(m_colors.size());
    for (size_t i = 0; i < sizeof(uint32_t); ++i)
        data.push_back(*(reinterpret_cast<const char*>(&color_size) + i));
    for (size_t i = 0; i < sizeof(float) * color_size; ++i)
        data.push_back(reinterpret_cast<const char*>(m_colors.data())[i]);

    return data;
}

}
