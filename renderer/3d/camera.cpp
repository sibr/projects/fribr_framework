#include "3d/camera.h"
#include "3dmath.h"
#include <cmath>

namespace
{

Eigen::Affine3f compute_view_matrix(const Eigen::Vector3f forward, const Eigen::Vector3f up, const Eigen::Vector3f position)
{
    Eigen::Vector3f right;
    right = forward.cross(up).normalized();

    Eigen::Vector3f orthogonal_up;
    orthogonal_up = right.cross(forward);

    Eigen::Matrix3f rotation;
    rotation.col(0) = right;
    rotation.col(1) = orthogonal_up;
    rotation.col(2) = -forward;

    return Eigen::Translation3f(-rotation.transpose() * position) * rotation.transpose();
}

}

namespace fribr
{

Camera::Camera(float near, float far, float fov, float aspect)
    : m_dirty(true), m_near(near), m_far(far), m_fov(fov), m_aspect(aspect), m_speed(1.0f), m_mouse_sensitivity(1.0f / 1000.0f),
      m_position(0.0f, 0.0f, 0.0f), m_forward(0.0f, 0.0f, -1.0f), m_up(0.0f, 1.0f, 0.0f)
{
}

Eigen::Matrix4f Camera::get_camera_to_clip()
{
    if (m_dirty)
    {
        m_camera_to_clip  = compute_projection_matrix(m_near, m_far, m_fov, m_aspect);
        m_world_to_camera = compute_view_matrix(m_forward, m_up, m_position);
        m_dirty = false;
    }

    return m_camera_to_clip;
}

Eigen::Affine3f Camera::get_world_to_camera()
{
    if (m_dirty)
    {
        m_camera_to_clip  = compute_projection_matrix(m_near, m_far, m_fov, m_aspect);
        m_world_to_camera = compute_view_matrix(m_forward, m_up, m_position);
        m_dirty = false;
    }

    return m_world_to_camera;
}

void Camera::update_transformation(const ImGuiIO& io)
{
    Eigen::Vector3f position_delta(0.0f, 0.0f, 0.0f);
    Eigen::Vector3f right;
    right = m_forward.cross(m_up).normalized();

    if (io.KeysDown[io.KeyMap[ImGuiKey_W]])
        position_delta += m_forward * m_speed;
    if (io.KeysDown[io.KeyMap[ImGuiKey_S]])
        position_delta -= m_forward * m_speed;
    if (io.KeysDown[io.KeyMap[ImGuiKey_D]])
        position_delta += right * m_speed;
    if (io.KeysDown[io.KeyMap[ImGuiKey_A]])
        position_delta -= right * m_speed;
    if (io.KeysDown[io.KeyMap[ImGuiKey_R]])
        position_delta += m_up * m_speed;
    if (io.KeysDown[io.KeyMap[ImGuiKey_F]])
        position_delta -= m_up * m_speed;

    if (position_delta.norm() > 0.0f)
        m_dirty = true;

    m_position += position_delta * io.DeltaTime;

    if (!io.MouseDown[0] || ImGui::IsMouseHoveringAnyWindow())
        return;

    ImVec2 mouse_delta = io.MouseDelta;
    float  yaw_angle   = -mouse_delta.x * m_mouse_sensitivity;
    float  pitch_angle = -mouse_delta.y * m_mouse_sensitivity;

    m_forward = Eigen::AngleAxisf(pitch_angle, right) *
                Eigen::AngleAxisf(yaw_angle,   m_up)  *
                m_forward;

    m_dirty = true;
}

}
