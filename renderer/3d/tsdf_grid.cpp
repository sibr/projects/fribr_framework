#include "tsdf_grid.h"

#include "tools/string_tools.h"
#include "tools/sized_priority_queue.h"
#include "tools/image_tools.h"

#include <opencv2/opencv.hpp>
#include <random>


#ifdef INRIA_WIN
size_t doAtomicOp(size_t* progress, int val);
#endif

namespace
{

std::string get_frame_number(const std::string &str)
{
    std::string color_name = fribr::strip_extension(fribr::get_filename(str));
    size_t position = color_name.find_last_of("-");
    return color_name.substr(position + 1);
}

Eigen::Vector3f unproject(int x, int y, float depth,
                          const Eigen::Matrix3f &inv_intrinsics,
                          const Eigen::Matrix4f &cam_to_world)
{
    Eigen::Vector3f img_pos   = Eigen::Vector3f(float(x), float(y), 1.0f);
    Eigen::Vector3f cam_pos   = depth * (inv_intrinsics * img_pos);
    Eigen::Vector4f world_pos = cam_to_world * Eigen::Vector4f(cam_pos.x(),
                                                               cam_pos.y(),
                                                               cam_pos.z(),
                                                               1.0f);
    return Eigen::Vector3f(world_pos.x(), world_pos.y(), world_pos.z());
}

float estimate_sfm_scale(const fribr::CalibratedImage::Vector &rgbd_images,
                         const std::vector<cv::Mat>           &depth_images,
                         const fribr::Observations            &observations,
                         float scale_factor)
{
    std::vector<float> sfm_depths;
	std::vector<float> depth_depths;
    for (size_t i = 0; i < rgbd_images.size(); ++i)
    {
        Eigen::Matrix4f world_to_camera = rgbd_images[i].get_world_to_camera().matrix();
	    for (size_t j = 0; j < observations.observations[i].size(); ++j)
        {
            int ii = observations.observations[i][j];
            Eigen::Vector4f point(observations.points[ii].x(),
                                  observations.points[ii].y(),
                                  observations.points[ii].z(),
                                  1.0f);

			Eigen::Vector4f point_camera_space = world_to_camera * point;
            Eigen::Vector3f point_image_space  = rgbd_images[i].get_intrinsics() * 
                                                 Eigen::Vector3f(point_camera_space.x(),
                                                                 point_camera_space.y(),
                                                                 point_camera_space.z());
            point_image_space *= scale_factor / point_image_space.z();

            if (point_image_space.x() < 0.0f || point_image_space.x() > depth_images[i].cols ||
				point_image_space.y() < 0.0f || point_image_space.y() > depth_images[i].rows)
				continue;

            uint16_t depth_uint = depth_images[i].at<uint16_t>(
                static_cast<int>(depth_images[i].rows - point_image_space.y()),
                static_cast<int>(point_image_space.x())
            );
            float depth_image_z = static_cast<float>(depth_uint);

			if (depth_image_z < 500.0f || depth_image_z > 3500.0f)
				continue;			

            sfm_depths.push_back(-point_camera_space.z());
			depth_depths.push_back(depth_image_z);
		}
    }

    static const int   RANSAC_ITERATIONS = 5000;
    static const float INLINER_THRESHOLD = 50.0f;
    static const int   RANSAC_SAMPLES    = 1;
    std::mt19937 generator(1337);
    std::uniform_int_distribution<int> dis(0, int(sfm_depths.size()) - 1);

    int   max_inliers = -1;
    float best_scale  = 0.0f;
    for (size_t i = 0; i < RANSAC_ITERATIONS; ++i)
    {
        int indices[RANSAC_SAMPLES];
        indices[0] = dis(generator);
        for (size_t j = 1; j < RANSAC_SAMPLES; ++j)
        {
            bool index_taken = true;
            while (index_taken)
            {
                index_taken = false;
                indices[j] = dis(generator);
                for (size_t k = 0; k < j; ++k)
                    index_taken |= indices[j] == indices[k];
            }
        }

        float numerator = 0.0f, denominator = 0.0f;
        for (size_t j = 0; j < RANSAC_SAMPLES; ++j)
        {
            numerator   += depth_depths[indices[j]] * sfm_depths[indices[j]];
            denominator += sfm_depths[indices[j]]   * sfm_depths[indices[j]];
        }
        float scale = numerator / denominator;
        
        int num_inliers = 0;
        for (size_t j = 0; j < sfm_depths.size(); ++j)
            if (fabs(sfm_depths[j] * scale - depth_depths[j]) < INLINER_THRESHOLD)
                num_inliers++;

        if (num_inliers > max_inliers)
        {
            max_inliers = num_inliers;
            best_scale  = scale;
        }
    }

    std::vector<float> sfm_inliers, depth_inliers;
    sfm_inliers.reserve(max_inliers);
    depth_inliers.reserve(max_inliers);
    for (size_t j = 0; j < sfm_depths.size(); ++j)
    {
        if (fabs(best_scale * sfm_depths[j] - depth_depths[j]) >= INLINER_THRESHOLD)
            continue;
        sfm_inliers.push_back(sfm_depths[j]);
        depth_inliers.push_back(depth_depths[j]);
    }

	Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, 1> > sfm_depths_eigen(sfm_inliers.data(), sfm_inliers.size(), 1);
	Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, 1> > depth_depths_eigen(depth_inliers.data(), depth_inliers.size(), 1);

    Eigen::Matrix<float, 1, 1> scaling = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>(sfm_depths_eigen.transpose() * sfm_depths_eigen).ldlt().solve(sfm_depths_eigen.transpose() * depth_depths_eigen);

    return scaling(0, 0);
}

}

namespace fribr
{

TSDFGrid::TSDFGrid(Eigen::Vector3i resolution, float truncation_distance_mm)
    : m_truncation_distance_mm(truncation_distance_mm), 
      m_resolution(resolution), 
      m_tsdf_volume(resolution.x() * resolution.y() * resolution.z(), 0.0f),
      m_min(0.0f, 0.0f, 0.0f),
      m_max(1.0f, 1.0f, 1.0f),
      m_mm_to_sfm(1.0f),
      m_sfm_to_mm(1.0f)
{
}

void TSDFGrid::clear()
{
    m_max = Eigen::Vector3f(1.0f, 1.0f, 1.0f);
    m_min = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
    m_mm_to_sfm = 1.0f;
    m_sfm_to_mm = 1.0f;
    m_tsdf_volume.assign(m_resolution.x() * m_resolution.y() * m_resolution.z(), 0.0f);
}

void TSDFGrid::integrate_depth_images(const std::string              &depth_folder, 
                                      fribr::CalibratedImage::Vector &cameras_depth,
                                      fribr::Observations            &observations_depth,
                                      fribr::PointCloud::Ptr          sfm_cloud) noexcept {
    std::cout << "Loading depth images..." << std::flush;
    std::vector<cv::Mat> color_images(cameras_depth.size(), cv::Mat());
    std::vector<cv::Mat> depth_images(cameras_depth.size(), cv::Mat());
    #ifdef NDEBUG
    #pragma omp parallel for
    #endif
#ifdef INRIA_WIN
    for (int i = 0; i < depth_images.size(); ++i)
#else
    for (size_t i = 0; i < depth_images.size(); ++i)
#endif
    {
        const fribr::CalibratedImage &camera = cameras_depth[i];

        std::string image_path   = camera.get_image_path();
        std::string frame_number = get_frame_number(image_path);
        std::string depth_path   = append_path(depth_folder, std::string("depth-") + frame_number + ".png");

        color_images[i] = load_image(image_path, -1, cv::IMREAD_COLOR);
        depth_images[i] = load_image(depth_path, -1, cv::IMREAD_ANYDEPTH);
    }
    std::cout << " Done!" << std::endl;

    std::cout << "Computing global scale..." << std::flush;
    float scale_factor = static_cast<float>(depth_images[0].rows) / color_images[0].rows;
    m_sfm_to_mm        = estimate_sfm_scale(cameras_depth, depth_images, observations_depth, scale_factor);
    m_mm_to_sfm        = 1.0f / m_sfm_to_mm;
    std::cout << " Done!" << std::endl;

    struct DepthSample {
        Eigen::Vector3f point;
        Eigen::Vector3f direction;
        float           confidence;
        DepthSample(Eigen::Vector3f &_point,
                    Eigen::Vector3f &_direction,
                    float            _confidence)
            : point(_point), direction(_direction), confidence(_confidence)
        {
        }
    };

#define FILTER_MISREGISTERED_DEPTH_MAPS 0
#ifdef  FILTER_MISREGISTERED_DEPTH_MAPS
    std::cout << "Finding misregistered depth maps..." << std::flush;
    std::vector<float> difference_percentile_mm(depth_images.size(), 500.0f);
    #ifdef NDEBUG
    #pragma omp parallel for
    #endif
#ifdef INRIA_WIN
    for (int i = 0; i < cameras_depth.size(); ++i)
#else
    for (size_t i = 0; i < cameras_depth.size(); ++i)
#endif
    {
        Eigen::Matrix4f world_to_camera = cameras_depth[i].get_world_to_camera().matrix();
        std::vector<float> depth_differences_mm;
	    for (size_t j = 0; j < observations_depth.observations[i].size(); ++j)
        {
            int ii = observations_depth.observations[i][j];
            Eigen::Vector4f point(observations_depth.points[ii].x(),
                                  observations_depth.points[ii].y(),
                                  observations_depth.points[ii].z(),
                                  1.0f);

			Eigen::Vector4f point_camera_space = world_to_camera * point;
            Eigen::Vector3f point_image_space  = cameras_depth[i].get_intrinsics() * 
                                                 Eigen::Vector3f(point_camera_space.x(),
                                                                 point_camera_space.y(),
                                                                 point_camera_space.z());
            point_image_space *= scale_factor / point_image_space.z();

            if (point_image_space.x() < 0.0f || point_image_space.x() > depth_images[i].cols ||
				point_image_space.y() < 0.0f || point_image_space.y() > depth_images[i].rows)
				continue;

            uint16_t depth_uint = depth_images[i].at<uint16_t>(
                static_cast<int>(depth_images[i].rows - point_image_space.y()),
                static_cast<int>(point_image_space.x())
            );
            float depth_image_z = static_cast<float>(depth_uint);

			if (depth_image_z < 500.0f || depth_image_z > 2500.0f)
				continue;		

            depth_differences_mm.push_back(fabs(-point_camera_space.z() * m_sfm_to_mm - depth_image_z));
		}

        if (depth_differences_mm.empty())
            continue;

        std::sort(depth_differences_mm.begin(), depth_differences_mm.end());
        difference_percentile_mm[i] = depth_differences_mm[(9 * depth_differences_mm.size()) / 10];
    }
    
    static const float DISTANCE_THRESHOLD_MM = 330.0f;
    int total_misregistered = 0;
    for (size_t i = 0; i < cameras_depth.size(); ++i)
    {
        if (difference_percentile_mm[i] > DISTANCE_THRESHOLD_MM)
            total_misregistered++;
    }
    std::cout << " Done! " << total_misregistered << " misregistered depth maps" << std::endl;
#endif

    std::cout << "Adding sfm points to depth maps..." << std::flush;
    #ifdef NDEBUG
    #pragma omp parallel for
    #endif
#ifdef INRIA_WIN
    for (int i = 0; i < depth_images.size(); ++i)
#else
    for (size_t i = 0; i < depth_images.size(); ++i)
#endif
    {
        #if FILTER_MISREGISTERED_DEPTH_MAPS
        if (difference_percentile_mm[i] > DISTANCE_THRESHOLD_MM)
            continue;
        #endif

        const fribr::CalibratedImage &camera = cameras_depth[i];
        cv::Mat                      &image  = depth_images[i];

        Eigen::Matrix4f world_to_camera = camera.get_world_to_camera().matrix();
        Eigen::Matrix3f intrinsics      = camera.get_intrinsics();

	    for (size_t j = 0; j < observations_depth.observations[i].size(); ++j)
        {
            int ii = observations_depth.observations[i][j];
            Eigen::Vector4f point(observations_depth.points[ii].x(),
                                  observations_depth.points[ii].y(),
                                  observations_depth.points[ii].z(),
                                  1.0f);

			Eigen::Vector4f point_camera_space = world_to_camera * point;
            Eigen::Vector3f point_image_space  = intrinsics * Eigen::Vector3f(point_camera_space.x(),
                                                                              point_camera_space.y(),
                                                                              point_camera_space.z());
            point_image_space *= scale_factor / point_image_space.z();

            if (point_image_space.x() < 0.0f || point_image_space.x() > depth_images[i].cols ||
				point_image_space.y() < 0.0f || point_image_space.y() > depth_images[i].rows)
				continue;

            uint16_t depth_uint = image.at<uint16_t>(
                static_cast<int>(depth_images[i].rows - point_image_space.y()),
                static_cast<int>(point_image_space.x())
            );
            float depth_image_z = static_cast<float>(depth_uint);
			if (depth_image_z > 0.0f)
				continue;

            float sfm_depth_mm = -point_camera_space.z() * m_sfm_to_mm;
            image.at<uint16_t>(
               static_cast<int>(depth_images[i].rows - point_image_space.y()),
               static_cast<int>(point_image_space.x())
            ) = static_cast<uint16_t>(sfm_depth_mm);
        }
    }
    std::cout << " Done!" << std::endl;

#define BILATERAL_FILLING 0
#if BILATERAL_FILLING
    std::cout << "Filling holes with a bilateral filter..." << std::flush;
    #ifdef NDEBUG
    #pragma omp parallel for
    #endif
    for (size_t i = 0; i < depth_images.size(); ++i)
    {
        cv::Mat &depth_image = depth_images[i];
        cv::Mat &color_image = color_images[i];
        static const float stdd_c = 0.1f;
        static const float stdd_s = 6.0f;
        for (int y = 0; y < depth_image.rows; ++y)
        for (int x = 0; x < depth_image.cols; ++x)
        {
            if (depth_image.at<uint16_t>(y, x) > 0)
                continue;

            cv::Vec3b c = color_image.at<cv::Vec3b>(y, x);
            Eigen::Vector3f c_ref(c[0] / 255.0f, c[1] / 255.0f, c[2] / 255.0f);
            
            float w_total = 0.0f;
            float d_total = 0.0f;
            for (int dy = -12; dy <= 12; ++dy)
            for (int dx = -12; dx <= 12; ++dx)
            {
                int nx = x + dx;
                int ny = y + dy;
                if (nx < 0 || nx >= depth_image.cols ||
                    ny < 0 || ny >= depth_image.rows)
                    continue;

                uint16_t d = depth_image.at<uint16_t>(ny, nx);
                if (d == 0)
                    continue;

                c = color_image.at<cv::Vec3b>(ny, nx);
                Eigen::Vector3f c_other(c[0] / 255.0f, c[1] / 255.0f, c[2] / 255.0f);
                float distance_squared = (c_ref - c_other).squaredNorm();
                
                float w = expf(-distance_squared / (2.0f * stdd_c * stdd_c))
                        * expf((-dy * dy - dx * dx) / (2.0f * stdd_s * stdd_s));
                
                w_total += w;
                d_total += d * w;
            }

            if (w_total > 4.0f * expf(-stdd_c / (2.0f * stdd_c * stdd_c) +
                                      -stdd_s / (2.0f * stdd_s * stdd_s)))
                depth_image.at<uint16_t>(y, x) = static_cast<uint16_t>(d_total / w_total);
        }
    }
    std::cout << " Done!" << std::endl;
#endif

    std::cout << "Computing point clouds..." << std::flush;
    static const float MIN_MM = 250.0f;
    static const float MAX_MM = 3500.0f;
    std::vector<std::vector<DepthSample> > point_clouds(cameras_depth.size());
    #ifdef NDEBUG
    #pragma omp parallel for
    #endif
#ifdef INRIA_WIN
    for (int i = 0; i < depth_images.size(); ++i)
#else
    for (size_t i = 0; i < depth_images.size(); ++i)
#endif
    {
        #if FILTER_MISREGISTERED_DEPTH_MAPS
        if (difference_percentile_mm[i] > DISTANCE_THRESHOLD_MM)
            continue;
        #endif

        const CalibratedImage &camera  = cameras_depth[i];
        cv::Mat               &image   = depth_images[i];

        Eigen::Matrix4f world_to_cam   = camera.get_world_to_camera().matrix();
        Eigen::Matrix4f cam_to_world   = world_to_cam.inverse();
        Eigen::Matrix3f intrinsics     = camera.get_intrinsics();
        Eigen::Matrix3f inv_intrinsics = intrinsics.inverse();
        Eigen::Vector4f camera_pos     = cam_to_world * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f);

        for (int y = 0; y < image.rows; ++y)
        for (int x = 0; x < image.cols; ++x)
        {
            uint16_t depth_uint = depth_images[i].at<uint16_t>(static_cast<int>(depth_images[i].rows - y), x);
            float    depth_mm   = static_cast<float>(depth_uint);
            if (depth_mm < MIN_MM || depth_mm > MAX_MM)
                continue;

            float depth_sfm = depth_mm * m_mm_to_sfm;
            Eigen::Vector3f point     = unproject(int(x / scale_factor), int(y / scale_factor), depth_sfm,
                                                  inv_intrinsics, cam_to_world);
            Eigen::Vector3f direction = (point - Eigen::Vector3f(camera_pos.x(),
                                                                 camera_pos.y(),
                                                                 camera_pos.z())).normalized();

            // from Nguyen et al. [3DIMPVT 2012.]
            float depth_m     = depth_mm / 1000.0f;
            float noise_std_m = 0.0012f + 0.0019f * (depth_m - 0.4f) * (depth_m - 0.4f);
            float confidence  = 1.0f / (1000.0f * noise_std_m);
            point_clouds[i].push_back(DepthSample(point, direction, confidence));
        }
    }
    std::cout << " Done!" << std::endl;

    std::cout << "Computing scene extents..." << std::flush;
    Eigen::Array3f aabb_min( 1e21f,  1e21f,  1e21f);
    Eigen::Array3f aabb_max(-1e21f, -1e21f, -1e21f);
    for (size_t i = 0; i < point_clouds.size(); ++i)
    {
        #if FILTER_MISREGISTERED_DEPTH_MAPS
        if (difference_percentile_mm[i] > DISTANCE_THRESHOLD_MM)
            continue;
        #endif

        std::vector<DepthSample> &cloud = point_clouds[i];
        for (DepthSample &p : cloud)
        {
            Eigen::Array3f array_v = p.point.array();
            aabb_min = aabb_min.min(array_v);
            aabb_max = aabb_max.max(array_v);
        }
    }
    m_max = aabb_max;
    m_min = aabb_min;
    std::cout << " Done!" << std::endl;

    std::cout << "Allocating memory for volume prior..." << std::flush;
    std::vector<float> confidence_grid(m_resolution.x() *
                                       m_resolution.y() *
                                       m_resolution.z(), 0.0f);
    std::cout << " Done!" << std::endl;

    Eigen::Array3f     grid_size_mm               = (aabb_max - aabb_min) * m_sfm_to_mm;
    Eigen::Array3f     truncation_distance_voxels = m_truncation_distance_mm *
                                                    Eigen::Array3f(float(m_resolution.x()),
                                                                   float(m_resolution.y()),
                                                                   float(m_resolution.z())) /
                                                    grid_size_mm;

    std::cout << std::endl
              << "Truncation distance in voxels: ["
              << truncation_distance_voxels.x() << ";"
              << truncation_distance_voxels.y() << ";"
              << truncation_distance_voxels.z() << "]"
              << std::endl;

    std::cout << "Computing TSDF volume: 0%               " << std::flush;
    size_t progress = 0;
    #ifdef NDEBUG
    #pragma omp parallel for
    #endif
#ifdef INRIA_WIN
    for (int i = 0; i < point_clouds.size(); ++i)
#else
    for (size_t i = 0; i < point_clouds.size(); ++i)
#endif
    {
        #if FILTER_MISREGISTERED_DEPTH_MAPS
        if (difference_percentile_mm[i] > DISTANCE_THRESHOLD_MM)
            continue;
        #endif
        
        const CalibratedImage &camera = cameras_depth[i];

        Eigen::Matrix4f world_to_cam  = camera.get_world_to_camera().matrix();
        Eigen::Matrix4f cam_to_world  = world_to_cam.inverse();
        Eigen::Vector4f camera_pos    = cam_to_world * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
        std::vector<Eigen::Vector3i> indices;
        indices.reserve(m_resolution.x() + m_resolution.y() + m_resolution.z());
        for (DepthSample &p : point_clouds[i])
        {
            indices.clear();
            indices.push_back(compute_voxel_index(p.point));
            ray_cast_all(p.point,  p.direction, m_truncation_distance_mm* m_mm_to_sfm, indices);

            for (Eigen::Vector3i &index : indices)
            {
                #ifdef NDEBUG
                #pragma omp atomic
                #endif
                confidence_grid[linearize_index(index)] += p.confidence;

                Eigen::Vector3f cur_voxel_pos = Eigen::Vector3f(float(index.x()), float(index.y()), float(index.z())) +
                                                Eigen::Vector3f(0.5f, 0.5f, 0.5f);
                Eigen::Vector3f cur_world_pos = compute_world_position(cur_voxel_pos);
                float           distance      = std::min(static_cast<float>(m_truncation_distance_mm),
                                                         (cur_world_pos - p.point).norm() * m_sfm_to_mm);
                #ifdef NDEBUG
                #pragma omp atomic
                #endif
                m_tsdf_volume[linearize_index(index)] += p.confidence * distance;
            }
            indices.clear();
            Eigen::Vector3f direction = p.point - camera_pos.head<3>();
            float           max_t     = direction.norm();
            direction /= max_t;
            ray_cast_all(camera_pos.head<3>(), direction, max_t, indices);
            for (Eigen::Vector3i &index : indices)
            {
                #ifdef NDEBUG
                #pragma omp atomic
                #endif
                confidence_grid[linearize_index(index)] += p.confidence;

                Eigen::Vector3f cur_voxel_pos = Eigen::Vector3f(float(index.x()), float(index.y()), float(index.z())) +
                                                Eigen::Vector3f(0.5f, 0.5f, 0.5f);
                Eigen::Vector3f cur_world_pos = compute_world_position(cur_voxel_pos);
                float           distance      = std::min(static_cast<float>(m_truncation_distance_mm),
                                                         (cur_world_pos - p.point).norm() * m_sfm_to_mm);
                #ifdef NDEBUG
                #pragma omp atomic
                #endif
                m_tsdf_volume[linearize_index(index)] -= p.confidence * distance;
            }
        }

#ifdef INRIA_WIN
		size_t prev_progress = doAtomicOp(&progress, 1);
#else
			size_t prev_progress = __sync_fetch_and_add(&progress, 1);
#endif
        if (prev_progress % 3 == 2)
            std::cout << "\rComputing TSDF volume: "
                      << (prev_progress + 1.0f) * 100.0f / point_clouds.size()
                      << "%                 " << std::flush;
    }

    #ifdef NDEBUG
    #pragma omp parallel for
    #endif
    for (int y = 0; y < m_resolution.y(); ++y)
    for (int x = 0; x < m_resolution.x(); ++x)
    for (int z = 0; z < m_resolution.z(); ++z)
    {
        size_t index          = linearize_index(Eigen::Vector3i(x, y, z));
        float  total_weight   = confidence_grid[index];
        float  total_distance = m_tsdf_volume[index];

        float final_dist = -m_truncation_distance_mm;
        if (total_weight > 0.0f)
            final_dist = total_distance / total_weight;

        m_tsdf_volume[index] = final_dist;
    }
    std::cout << " Done!" << std::endl;
}

Eigen::Vector3i TSDFGrid::compute_voxel_index(Eigen::Vector3f p) const noexcept {
    return compute_voxel_position(p).cast<int>();
}

Eigen::Vector3f TSDFGrid::compute_voxel_position(Eigen::Vector3f p) const noexcept {
    Eigen::Array3f delta    = (m_max - m_min).array();
    Eigen::Array3f p_local  = (p - m_min).array() / delta;
    Eigen::Array3f p_index  = p_local * Eigen::Array3f(float(m_resolution.x()),
                                                       float(m_resolution.y()),
                                                       float(m_resolution.z()));
    return p_index.matrix();
}

Eigen::Vector3f TSDFGrid::compute_world_position(Eigen::Vector3f p) const noexcept {
    Eigen::Array3f delta   = (m_max - m_min).array();
    Eigen::Array3f p_local = p.array() / Eigen::Array3f(float(m_resolution.x()),
                                                        float(m_resolution.y()),
                                                        float(m_resolution.z()));
    return (p_local * delta).matrix() + m_min;
}

float TSDFGrid::trilinear_fetch(Eigen::Vector3f p) const noexcept {
    Eigen::Vector3f vp = compute_voxel_position(p);
    if (vp.x() < 0.0f || vp.x() >= float(m_resolution.x()) ||
        vp.y() < 0.0f || vp.y() >= float(m_resolution.y()) ||
        vp.z() < 0.0f || vp.z() >= float(m_resolution.z()))
        return m_truncation_distance_mm;

    Eigen::Vector3i li(std::max(0, (int)floorf(vp.x())),
                       std::max(0, (int)floorf(vp.y())),
                       std::max(0, (int)floorf(vp.z())));
    Eigen::Vector3i ui(std::min(m_resolution.x() - 1, (int)ceilf(vp.x())),
                       std::min(m_resolution.y() - 1, (int)ceilf(vp.y())),
                       std::min(m_resolution.z() - 1, (int)ceilf(vp.z())));

    float x0y0z0 = m_tsdf_volume[linearize_index(Eigen::Vector3i(li.x(), li.y(), li.z()))];
    float x0y0z1 = m_tsdf_volume[linearize_index(Eigen::Vector3i(li.x(), li.y(), ui.z()))];
    float x0y1z0 = m_tsdf_volume[linearize_index(Eigen::Vector3i(li.x(), ui.y(), li.z()))];
    float x0y1z1 = m_tsdf_volume[linearize_index(Eigen::Vector3i(li.x(), ui.y(), ui.z()))];
    float x1y0z0 = m_tsdf_volume[linearize_index(Eigen::Vector3i(ui.x(), li.y(), li.z()))];
    float x1y0z1 = m_tsdf_volume[linearize_index(Eigen::Vector3i(ui.x(), li.y(), ui.z()))];
    float x1y1z0 = m_tsdf_volume[linearize_index(Eigen::Vector3i(ui.x(), ui.y(), li.z()))];
    float x1y1z1 = m_tsdf_volume[linearize_index(Eigen::Vector3i(ui.x(), ui.y(), ui.z()))];

    Eigen::Vector3f a = vp - li.cast<float>();
    float x0y0 = (1.0f - a.z()) * x0y0z0 + a.z() * x0y0z1;
    float x0y1 = (1.0f - a.z()) * x0y1z0 + a.z() * x0y1z1;
    float x1y0 = (1.0f - a.z()) * x1y0z0 + a.z() * x1y0z1;
    float x1y1 = (1.0f - a.z()) * x1y1z0 + a.z() * x1y1z1;

    float x0 = (1.0f - a.y()) * x0y0 + a.y() * x0y1;
    float x1 = (1.0f - a.y()) * x1y0 + a.y() * x1y1;

    return (1.0f - a.x()) * x0 + a.x() * x1;
}


float TSDFGrid::nearest_fetch(Eigen::Vector3f p) const noexcept {
    Eigen::Vector3i vi = compute_voxel_index(p);
    if (vi.x() < 0 || vi.x() >= m_resolution.x() ||
        vi.y() < 0 || vi.y() >= m_resolution.y() ||
        vi.z() < 0 || vi.z() >= m_resolution.z())
        return m_truncation_distance_mm;

    return m_tsdf_volume[linearize_index(vi)];
}

float TSDFGrid::index_fetch(Eigen::Vector3i vi) const noexcept {
    if (vi.x() < 0 || vi.x() >= m_resolution.x() ||
        vi.y() < 0 || vi.y() >= m_resolution.y() ||
        vi.z() < 0 || vi.z() >= m_resolution.z())
        return m_truncation_distance_mm;

    return m_tsdf_volume[linearize_index(vi)];
}

Eigen::Vector3f TSDFGrid::ray_cast_first(Eigen::Vector3f orig, Eigen::Vector3f dir, float max_t)
{
    Eigen::Vector3f a_voxel   = compute_voxel_position(orig);
    Eigen::Vector3f b_voxel   = compute_voxel_position(orig + dir);
    Eigen::Vector3f dir_voxel = b_voxel - a_voxel;

    Eigen::Vector3f inv_dir(1e21f, 1e21f, 1e21f);
    if (dir_voxel.x() != 0.0f) inv_dir.x() = 1.0f / dir_voxel.x();
    if (dir_voxel.y() != 0.0f) inv_dir.y() = 1.0f / dir_voxel.y();
    if (dir_voxel.z() != 0.0f) inv_dir.z() = 1.0f / dir_voxel.z();

    Eigen::Vector3i index = compute_voxel_index(orig);
    float ds_x      = float((0 < dir_voxel.x()) - (dir_voxel.x() < 0)); // sign(dir_voxel.x())
    float ds_y      = float((0 < dir_voxel.y()) - (dir_voxel.y() < 0)); // sign(dir_voxel.y())
    float ds_z      = float((0 < dir_voxel.z()) - (dir_voxel.z() < 0)); // sign(dir_voxel.z())

    float v_max_x   = (ds_x > 0.0f ? ceilf(a_voxel.x()) : floorf(a_voxel.x())) - a_voxel.x();
    float v_max_y   = (ds_y > 0.0f ? ceilf(a_voxel.y()) : floorf(a_voxel.y())) - a_voxel.y();
    float v_max_z   = (ds_z > 0.0f ? ceilf(a_voxel.z()) : floorf(a_voxel.z())) - a_voxel.z();

    float t_max_x   = v_max_x * inv_dir.x();
    float t_max_y   = v_max_y * inv_dir.y();
    float t_max_z   = v_max_z * inv_dir.z();

    float t_delta_x = fabs(inv_dir.x());
    float t_delta_y = fabs(inv_dir.y());
    float t_delta_z = fabs(inv_dir.z());

    float near_t    = 0.0f;
    float far_t     = 0.0f;

    do
    {
        if (trilinear_fetch(orig + dir * far_t) >= 0.0f)
            break;

        near_t = far_t;
        far_t  = std::min(t_max_x,
                 std::min(t_max_y,
                          t_max_z));

        if (t_max_x < t_max_y)
        {
            if (t_max_x < t_max_z)
            {
                index.x() += static_cast<int>(ds_x);
                t_max_x   += t_delta_x;
            }
            else
            {
                index.z() += static_cast<int>(ds_z);
                t_max_z   += t_delta_z;
            }
        }
        else
        {
            if (t_max_y < t_max_z)
            {
                index.y() += static_cast<int>(ds_y);
                t_max_y   += t_delta_y;
            }
            else
            {
                index.z() += static_cast<int>(ds_z);
                t_max_z   += t_delta_z;
            }
        }

        if (index.x() < 0 || index.x() >= m_resolution.x() ||
            index.y() < 0 || index.y() >= m_resolution.y() ||
            index.z() < 0 || index.z() >= m_resolution.z())
            break;

    } while(t_max_x < max_t || t_max_y < max_t || t_max_z < max_t);

    float t = far_t;
    if (trilinear_fetch(orig + dir * t) > 0.0f)
    while (fabs(far_t - near_t) / far_t > 0.0001f)
    {
        t = (far_t + near_t) / 2.0f;
        float tsdf = trilinear_fetch(orig + dir * t);
        if (tsdf > 0.0f)
            far_t  = t;
        else if (tsdf < 0.0f)
            near_t = t;
        else 
            break;
    }

    return orig + dir * t;
}

void TSDFGrid::ray_cast_all(Eigen::Vector3f orig, Eigen::Vector3f dir, float max_t,
                            std::vector<Eigen::Vector3i> &indices)
{
    Eigen::Vector3f a_voxel   = compute_voxel_position(orig);
    Eigen::Vector3f b_voxel   = compute_voxel_position(orig + dir);
    Eigen::Vector3f dir_voxel = b_voxel - a_voxel;

    Eigen::Vector3f inv_dir(1e21f, 1e21f, 1e21f);
    if (dir_voxel.x() != 0.0f) inv_dir.x() = 1.0f / dir_voxel.x();
    if (dir_voxel.y() != 0.0f) inv_dir.y() = 1.0f / dir_voxel.y();
    if (dir_voxel.z() != 0.0f) inv_dir.z() = 1.0f / dir_voxel.z();

    Eigen::Vector3i index = compute_voxel_index(orig);
    float ds_x      = float((0 < dir_voxel.x()) - (dir_voxel.x() < 0)); // sign(dir_voxel.x())
    float ds_y      = float((0 < dir_voxel.y()) - (dir_voxel.y() < 0)); // sign(dir_voxel.y())
    float ds_z      = float((0 < dir_voxel.z()) - (dir_voxel.z() < 0)); // sign(dir_voxel.z())

    float v_max_x   = (ds_x > 0.0f ? ceilf(a_voxel.x()) : floorf(a_voxel.x())) - a_voxel.x();
    float v_max_y   = (ds_y > 0.0f ? ceilf(a_voxel.y()) : floorf(a_voxel.y())) - a_voxel.y();
    float v_max_z   = (ds_z > 0.0f ? ceilf(a_voxel.z()) : floorf(a_voxel.z())) - a_voxel.z();

    float t_max_x   = v_max_x * inv_dir.x();
    float t_max_y   = v_max_y * inv_dir.y();
    float t_max_z   = v_max_z * inv_dir.z();

    float t_delta_x = fabs(inv_dir.x());
    float t_delta_y = fabs(inv_dir.y());
    float t_delta_z = fabs(inv_dir.z());

    do
    {
        if (t_max_x < t_max_y)
        {
            if (t_max_x < t_max_z)
            {
                index.x() += static_cast<int>(ds_x);
                t_max_x   += t_delta_x;
            }
            else
            {
                index.z() += static_cast<int>(ds_z);
                t_max_z   += t_delta_z;
            }
        }
        else
        {
            if (t_max_y < t_max_z)
            {
                index.y() += static_cast<int>(ds_y);
                t_max_y   += t_delta_y;
            }
            else
            {
                index.z() += static_cast<int>(ds_z);
                t_max_z   += t_delta_z;
            }
        }

        if (index.x() < 0 || index.x() >= m_resolution.x() ||
            index.y() < 0 || index.y() >= m_resolution.y() ||
            index.z() < 0 || index.z() >= m_resolution.z())
            break;

        indices.push_back(index);
    } while(t_max_x < max_t || t_max_y < max_t || t_max_z < max_t);
}

}

#ifdef INRIA_WIN
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <Windows.h>
#include <WinBase.h>

// https://msdn.microsoft.com/en-us/library/windows/desktop/ms683597%28v=vs.85%29.aspx

size_t doAtomicOp(size_t* progress, int val) {
//	*progress =  (*progress + val);
//	return *progress;
			return _InterlockedExchangeAdd(progress, val);
}
#endif

