set(PROJECT_PAGE "fribrFrameworkPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/fribr_framework")
set(PROJECT_DESCRIPTION "Core framework of FRIBR")
set(PROJECT_TYPE "TOOLBOX")
